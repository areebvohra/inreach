import React from 'react';
import { Container } from 'native-base';

import AppNavigator from './src/navigation/AppNavigator';

const App = () => {
  return (
    <Container>
      <AppNavigator />
    </Container>
  );
}

export default App;
