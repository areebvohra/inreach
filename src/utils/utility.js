import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen';
import { Dimensions, Alert, Platform, StatusBar } from 'react-native';

const X_WIDTH = 375;
const X_HEIGHT = 812;

const XSMAX_WIDTH = 414;
const XSMAX_HEIGHT = 896;

const SCREEN_WIDTH = Dimensions.get('window').width
const SCREEN_HEIGHT = Dimensions.get('window').height

const { height, width } = Dimensions.get('window');

const isIPhoneX = () => Platform.OS === 'ios' && !Platform.isPad && !Platform.isTVOS
    ? width === X_WIDTH && height === X_HEIGHT || width === XSMAX_WIDTH && height === XSMAX_HEIGHT
    : false;

const StatusBarHeight = Platform.select({
    ios: isIPhoneX() ? 44 : 20,
    android: StatusBar.currentHeight,
    default: 0
})

const deviceHeight = isIphoneX()
    ? SCREEN_HEIGHT - 78 // iPhone X style SafeAreaView size in portrait
    : Platform.OS === "android"
        ? SCREEN_HEIGHT - StatusBar.currentHeight
        : SCREEN_HEIGHT;

const wp = (number) => widthPercentageToDP(number);

const hp = (number) => heightPercentageToDP(number);

function converToRatio(fontSize) {
    // guideline height for standard 5" device screen
    const standardScreenHeight = 680;
    const heightPercent = (fontSize * deviceHeight) / standardScreenHeight;
    if (Platform.OS === 'ios') {
        return Math.round(heightPercent);
    } else {
        return Math.round(heightPercent) - 2
    }
}

function isIphoneX() {
    const dimen = Dimensions.get('window');
    return (
        Platform.OS === 'ios' &&
        !Platform.isPad &&
        !Platform.isTVOS &&
        ((dimen.height === 812 || dimen.width === 812) || (dimen.height === 896 || dimen.width === 896))
    );
}
function getStatusBarHeight(safe) {
    return Platform.select({
        ios: ifIphoneX(safe ? 44 : 30, 20),
        android: StatusBar.currentHeight,
        default: 0
    });
}

export { wp, hp, converToRatio, isIphoneX, getStatusBarHeight, StatusBarHeight };