const Fonts = {
    MulishBlack: 'Mulish-Black',
    MulishBlackItalic: 'Mulish-BlackItalic',
    MulishBold: 'Mulish-Bold',
    MulishBoldItalic: 'Mulish-BoldItalic',
    MulishExtraBold: 'Mulish-ExtraBold',
    MulishExtraBoldItalic: 'Mulish-ExtraBoldItalic',
    MulishExtraLight: 'Mulish-ExtraLight',
    MulishItalic: 'Mulish-Italic',
    MulishLight: 'Mulish-Light',
    MulishLightItalic: 'Mulish-LightItalic',
    MulishMedium: 'Mulish-Medium',
    MulishMediumItalic: 'Mulish-MediumItalic',
    MulishRegular: 'Mulish-Regular',
    MulishSemiBold: 'Mulish-SemiBold',
    MulishSemiBoldItalic: 'Mulish-SemiBoldItalic',
};

export { Fonts };