import React from 'react';
import { View, StyleSheet } from 'react-native';
import { hp, wp } from '../utils/utility';

const DotSlide = ({ active1, active2, active3 }) => (
    <View style={{ flexDirection: 'row', width: wp(100), paddingHorizontal: wp(20), justifyContent: 'space-between', alignItems: 'center', }}>
        <View style={[styles.dots, active1 ? null : { backgroundColor: 'transparent' }]} />
        <View style={styles.line} />
        <View style={[styles.dots, active2 ? null : { backgroundColor: 'transparent' }]} />
        <View style={styles.line} />
        <View style={[styles.dots, active3 ? null : { backgroundColor: 'transparent' }]} />
    </View>
);

const styles = StyleSheet.create({
    dots: {
        backgroundColor: '#005bd7',
        width: wp(5), height: hp(2.5),
        borderRadius: 30,
        borderColor: '#005bd7', borderWidth: 1,
    },
    line: {
        borderBottomColor: '#005bd7',
        borderBottomWidth: 0.5,
        width: wp(22)
    }
})

export default DotSlide;