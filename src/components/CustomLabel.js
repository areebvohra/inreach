import React from 'react';
import { Image, StyleSheet, Text, View } from "react-native";
import { Badge } from "react-native-elements";
import { Fonts } from "../utils/Fonts";
import FontSize from "../utils/FontSize";
import { hp, wp } from "../utils/utility";

const CustomLabel = ({ text, image, badgeValue, display = 'flex' }) => (
    <View style={styles.labelContainer}>
        <View style={{ alignItems: 'flex-end', flexDirection: 'row', justifyContent: 'space-between', }}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Image style={styles.iconStyle} resizeMode="contain" source={image} />
                <Text style={styles.labelStyle}>{text}</Text>
            </View>
        </View>
        <Badge value={badgeValue} badgeStyle={[styles.badgeStyle, { display }]} />
    </View>
);

export default CustomLabel;

const styles = StyleSheet.create({
    iconStyle: { tintColor: '#000', width: wp(6), height: hp(3) },
    labelContainer: {
        backgroundColor: '#fff',
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        width: wp(62),
    },
    labelStyle: { color: '#333333', fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, paddingLeft: wp(2) },
    badgeStyle: { width: wp(6), height: hp(3), backgroundColor: '#0c51af', borderRadius: 15 }
})