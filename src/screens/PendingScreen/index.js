import React, { useState } from 'react';
import { StyleSheet, Text, View, useWindowDimensions, Image } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { Button } from 'native-base'
import { Badge } from "react-native-elements";

import { hp, wp } from '../../utils/utility';
import FontSize from '../../utils/FontSize';
import { Fonts } from '../../utils/Fonts';

const FirstRoute = () => {
    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <View style={{ width: '100%', paddingVertical: hp(2), paddingLeft: wp(4), backgroundColor: '#f3f3f3' }}>
                <Text style={{ fontFamily: Fonts.MulishExtraBold, fontSize: 13, color: '#333333' }}>All Connection Requests</Text>
            </View>
            <View style={{ paddingHorizontal: wp(4), paddingVertical: hp(1), flexDirection: 'row', justifyContent: 'space-between', }}>
                <View style={styles.itemContainer}>
                    <Image style={styles.itemImageStyle} resizeMode="cover" source={require('../../assets/avatar-pending-1.png')} />
                    <View style={{ paddingVertical: hp(1.5), width: wp(43), }}>
                        <Text style={{ textAlign: 'center', fontSize: FontSize('medium'), fontFamily: Fonts.MulishMedium, color: '#333333' }}>David Smith</Text>
                        <Text style={{ textAlign: 'center', fontSize: FontSize('small'), fontFamily: Fonts.MulishBold, color: '#333333' }}>Dentist</Text>
                        <Text style={{ textAlign: 'center', fontSize: FontSize('small'), fontFamily: Fonts.MulishLight, color: '#333333' }}>Los Angeles, CA</Text>
                    </View>
                    <View>
                        <Button style={[styles.btnStyle, { backgroundColor: '#1156b4' }]}>
                            <Text style={{ color: '#ffffff', fontSize: FontSize('small'), fontFamily: Fonts.MulishLight }}>Approve</Text>
                        </Button>
                        <Button style={[styles.btnStyle, { elevation: 0, backgroundColor: '#ffffff' }]}>
                            <Text style={{ color: '#1156b4', fontSize: FontSize('small'), fontFamily: Fonts.MulishLight }}>Ignore</Text>
                        </Button>
                    </View>
                    <View style={styles.referredStyle}>
                        <Image style={{ width: wp(4), height: hp(2), tintColor: '#fff' }} resizeMode="contain" source={require('../../assets/icons/reach-out.png')} />
                        <Text style={{ fontSize: FontSize('xMini'), fontFamily: Fonts.MulishLight, color: '#fff', paddingLeft: wp(1) }}>Referred: <Text style={{ fontFamily: Fonts.MulishBold, }}>108x</Text></Text>
                    </View>
                </View>
                <View style={styles.itemContainer}>
                    <Image style={styles.itemImageStyle} resizeMode="cover" source={require('../../assets/avatar-pending-2.png')} />
                    <View style={{ paddingVertical: hp(1.5), width: wp(43), }}>
                        <Text style={{ textAlign: 'center', fontSize: FontSize('medium'), fontFamily: Fonts.MulishMedium, color: '#333333' }}>Susan Chang</Text>
                        <Text style={{ textAlign: 'center', fontSize: FontSize('small'), fontFamily: Fonts.MulishBold, color: '#333333' }}>Realtor</Text>
                        <Text style={{ textAlign: 'center', fontSize: FontSize('small'), fontFamily: Fonts.MulishLight, color: '#333333' }}>Los Angeles, CA</Text>
                    </View>
                    <View>
                        <Button style={[styles.btnStyle, { backgroundColor: '#1156b4' }]}>
                            <Text style={{ color: '#ffffff', fontSize: FontSize('small'), fontFamily: Fonts.MulishLight }}>Approve</Text>
                        </Button>
                        <Button style={[styles.btnStyle, { elevation: 0, backgroundColor: '#ffffff' }]}>
                            <Text style={{ color: '#1156b4', fontSize: FontSize('small'), fontFamily: Fonts.MulishLight }}>Ignore</Text>
                        </Button>
                    </View>
                    <View style={styles.referredStyle}>
                        <Image style={{ width: wp(4), height: hp(2), tintColor: '#fff' }} resizeMode="contain" source={require('../../assets/icons/reach-out.png')} />
                        <Text style={{ fontSize: FontSize('xMini'), fontFamily: Fonts.MulishLight, color: '#fff', paddingLeft: wp(1) }}>Referred: <Text style={{ fontFamily: Fonts.MulishBold, }}>108x</Text></Text>
                    </View>
                </View>
            </View>
        </View>
    );
}

const SecondRoute = () => {
    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <View style={{ width: '100%', paddingVertical: hp(2), paddingLeft: wp(4), backgroundColor: '#f3f3f3' }}>
                <Text style={{ fontFamily: Fonts.MulishExtraBold, fontSize: 13, color: '#333333' }}>There are the referrals sent by your network of contacts</Text>
            </View>
            <View style={{ paddingHorizontal: wp(4), paddingTop: hp(3), flexDirection: 'row', justifyContent: 'space-between', }}>
                <View style={styles.itemContainer}>
                    <Image style={styles.itemImageStyle} resizeMode="cover" source={require('../../assets/avatar-pending-1.png')} />
                    <View style={{ paddingVertical: hp(1.5), width: wp(43), }}>
                        <Text style={{ textAlign: 'center', fontSize: FontSize('medium'), fontFamily: Fonts.MulishMedium, color: '#333333' }}>David Smith</Text>
                        <Text style={{ textAlign: 'center', fontSize: FontSize('small'), fontFamily: Fonts.MulishBold, color: '#333333' }}>Dentist</Text>
                        <Text style={{ textAlign: 'center', fontSize: FontSize('small'), fontFamily: Fonts.MulishLight, color: '#333333' }}>Los Angeles, CA</Text>
                    </View>
                    <View>
                        <Button style={[styles.btnStyle, { backgroundColor: '#1156b4' }]}>
                            <Text style={{ color: '#ffffff', fontSize: FontSize('small'), fontFamily: Fonts.MulishLight }}>Approve</Text>
                        </Button>
                        <Button style={[styles.btnStyle, { elevation: 0, backgroundColor: '#ffffff' }]}>
                            <Text style={{ color: '#1156b4', fontSize: FontSize('small'), fontFamily: Fonts.MulishLight }}>Ignore</Text>
                        </Button>
                    </View>
                    <View style={styles.referredStyle}>
                        <Image style={{ width: wp(4), height: hp(2), tintColor: '#fff' }} resizeMode="contain" source={require('../../assets/icons/reach-out.png')} />
                        <Text style={{ fontSize: FontSize('xMini'), fontFamily: Fonts.MulishLight, color: '#fff', paddingLeft: wp(1) }}>Referred: <Text style={{ fontFamily: Fonts.MulishBold, }}>108x</Text></Text>
                    </View>
                    <View style={{ borderWidth: 1, borderRadius: wp(20), borderColor: '#fff', position: 'absolute', top: -10, right: -7 }}>
                        <Image style={{ width: wp(11), height: hp(5.5), borderRadius: wp(20) }} resizeMode="contain" source={require('../../assets/avatar-pending-2.png')} />
                    </View>
                </View>
                <View style={styles.itemContainer}>
                    <Image style={styles.itemImageStyle} resizeMode="cover" source={require('../../assets/avatar-pending-2.png')} />
                    <View style={{ paddingVertical: hp(1.5), width: wp(43), }}>
                        <Text style={{ textAlign: 'center', fontSize: FontSize('medium'), fontFamily: Fonts.MulishMedium, color: '#333333' }}>Susan Chang</Text>
                        <Text style={{ textAlign: 'center', fontSize: FontSize('small'), fontFamily: Fonts.MulishBold, color: '#333333' }}>Realtor</Text>
                        <Text style={{ textAlign: 'center', fontSize: FontSize('small'), fontFamily: Fonts.MulishLight, color: '#333333' }}>Los Angeles, CA</Text>
                    </View>
                    <View>
                        <Button style={[styles.btnStyle, { backgroundColor: '#1156b4' }]}>
                            <Text style={{ color: '#ffffff', fontSize: FontSize('small'), fontFamily: Fonts.MulishLight }}>Approve</Text>
                        </Button>
                        <Button style={[styles.btnStyle, { elevation: 0, backgroundColor: '#ffffff' }]}>
                            <Text style={{ color: '#1156b4', fontSize: FontSize('small'), fontFamily: Fonts.MulishLight }}>Ignore</Text>
                        </Button>
                    </View>
                    <View style={styles.referredStyle}>
                        <Image style={{ width: wp(4), height: hp(2), tintColor: '#fff' }} resizeMode="contain" source={require('../../assets/icons/reach-out.png')} />
                        <Text style={{ fontSize: FontSize('xMini'), fontFamily: Fonts.MulishLight, color: '#fff', paddingLeft: wp(1) }}>Referred: <Text style={{ fontFamily: Fonts.MulishBold, }}>108x</Text></Text>
                    </View>
                    <View style={{ borderWidth: 1, borderRadius: wp(20), borderColor: '#fff', position: 'absolute', top: -10, right: -7 }}>
                        <Image style={{ width: wp(11), height: hp(5.5), borderRadius: wp(20) }} resizeMode="contain" source={require('../../assets/avatar-pending-1.png')} />
                    </View>
                </View>
            </View>
        </View>
    )
};

const PendingScreen = ({ navigation }) => {
    const layout = useWindowDimensions();

    const [index, setIndex] = useState(0);
    const [routes] = useState([
        { key: 'first', title: 'Requests', badge: 1 },
        { key: 'second', title: 'Referrals', badge: 2 },
    ]);

    const renderScene = SceneMap({
        first: FirstRoute,
        second: SecondRoute,
    });

    const renderTabBar = props => (
        <TabBar
            {...props}
            indicatorContainerStyle={{ marginLeft: wp(6) }}
            indicatorStyle={{ backgroundColor: '#074caa', height: 6, marginBottom: -6 }}
            style={{ backgroundColor: '#fff', elevation: 0, alignContent: 'center', borderBottomWidth: hp(0.1), borderBottomColor: '#e6e6e6' }}
            tabStyle={{ width: wp(30), height: hp(6.5), }}
            labelStyle={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishLight, textTransform: 'capitalize', marginTop: -1, marginLeft: wp(10), width: wp(26), textAlign: 'center' }}
            renderLabel={({ route, focused, color }) => (
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={{ color, fontSize: FontSize('medium'), fontFamily: Fonts.MulishLight, textTransform: 'capitalize', marginTop: -1, marginLeft: wp(10), width: wp(26), textAlign: 'center' }}>
                        {route.title}
                    </Text>
                    <Badge value={route.badge} badgeStyle={[styles.badgeStyle, { display: route.badge == 0 ? 'none' : 'flex' }]} />
                </View>
            )}
            activeColor={'#333333'}
            inactiveColor={'#074caa'}
        />
    );

    return (
        <TabView
            navigationState={{ index, routes }}
            style={{ justifyContent: 'center' }}
            sceneContainerStyle={{ flexDirection: 'row' }}
            renderTabBar={renderTabBar}
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={{ width: layout.width }}
        />
    );
}

export default PendingScreen;

const styles = StyleSheet.create({
    container: { flex: 1, alignItems: 'center', backgroundColor: '#fff' },
    itemContainer: { width: wp(44), alignItems: 'center', borderRadius: 5, borderWidth: 0, backgroundColor: '#fff', elevation: 8 },
    itemImageStyle: { width: wp(44), height: hp(20), borderTopLeftRadius: 5, borderTopRightRadius: 5 },
    btnStyle: { width: wp(34), height: hp(4.5), justifyContent: 'center', alignItems: 'center', borderRadius: 4, marginBottom: hp(1) },
    badgeStyle: { width: wp(6), height: hp(3.1), backgroundColor: '#0c51af', borderRadius: 15 },
    referredStyle: { backgroundColor: '#000', flexDirection: 'row', alignItems: 'center', paddingHorizontal: wp(1.5), paddingVertical: hp(0.8), borderRadius: 5, opacity: 0.8, position: 'absolute', left: 3, top: 4 }
})