import React, { useState } from 'react';
import { Text, View, Image, StyleSheet, TextInput, TouchableOpacity, TouchableWithoutFeedback, useWindowDimensions, ScrollView, FlatList, LayoutAnimation, Platform, UIManager, Modal, Pressable, } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import AntDesign from 'react-native-vector-icons/AntDesign'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Button } from 'native-base'
import Animated from 'react-native-reanimated';
import SwipeableItem, { UnderlayParams } from 'react-native-swipeable-item';
import { CodeField, Cursor, useBlurOnFulfill, useClearByFocusCell } from 'react-native-confirmation-code-field';

import FontSize from '../../utils/FontSize';
import { Fonts } from '../../utils/Fonts';
import { hp, wp } from '../../utils/utility';

if (Platform.OS === 'android') {
    UIManager.setLayoutAnimationEnabledExperimental &&
        UIManager.setLayoutAnimationEnabledExperimental(true);
}

type Item = {
    key: string;
    text: string;
    imagePath: string;
    messageName: string;
    lock: boolean;
    time: string;
    sent: boolean;
    received: boolean;
    read: boolean;
};

const FirstRoute = () => {
    return (
        <ScrollView style={{ flex: 1, backgroundColor: '#fff' }} contentContainerStyle={{ paddingBottom: hp(4) }}>
            <View style={{ width: '100%', paddingVertical: hp(2), paddingHorizontal: wp(4), backgroundColor: '#f3f3f3', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                <Text style={{ fontFamily: Fonts.MulishExtraBold, fontSize: 13, color: '#333333' }}>Discover Professionals that are In Reach</Text>
                <Image style={{ width: wp(6), height: hp(3) }} resizeMode="contain" source={require('../../assets/icons/photo.png')} />
            </View>
            <View style={{ paddingHorizontal: wp(4), paddingTop: hp(2), paddingBottom: hp(1), }}>
                <Text style={{ fontSize: FontSize('large'), fontFamily: Fonts.MulishMedium, color: '#333333' }}>Most Referred</Text>
            </View>
            <View style={{ paddingHorizontal: wp(4), paddingVertical: hp(1), flexDirection: 'row', justifyContent: 'space-between', }}>
                <View style={styles.itemContainer}>
                    <Image style={styles.itemImageStyle} resizeMode="cover" source={require('../../assets/avatar-pending-1.png')} />
                    <View style={{ paddingVertical: hp(1.5), width: wp(43), }}>
                        <Text style={{ textAlign: 'center', fontSize: FontSize('medium'), fontFamily: Fonts.MulishMedium, color: '#333333' }}>David Smith</Text>
                        <Text style={{ textAlign: 'center', fontSize: FontSize('small'), fontFamily: Fonts.MulishBold, color: '#333333' }}>Dentist</Text>
                        <Text style={{ textAlign: 'center', fontSize: FontSize('small'), fontFamily: Fonts.MulishRegular, color: '#333333' }}>Los Angeles, CA</Text>
                    </View>
                    <View style={{ paddingTop: hp(1), paddingBottom: hp(2) }}>
                        <Button style={[styles.btnStyle, { backgroundColor: '#1156b4' }]}>
                            <Text style={{ color: '#ffffff', fontSize: FontSize('small'), fontFamily: Fonts.MulishLight }}>Exchange Contacts</Text>
                        </Button>
                    </View>
                    <View style={styles.referredStyle}>
                        <Image style={{ width: wp(4), height: hp(2), tintColor: '#fff' }} resizeMode="contain" source={require('../../assets/icons/reach-out.png')} />
                        <Text style={{ fontSize: FontSize('xMini'), fontFamily: Fonts.MulishLight, color: '#fff', paddingLeft: wp(1) }}>Referred: <Text style={{ fontFamily: Fonts.MulishBold, }}>108x</Text></Text>
                    </View>
                </View>
                <View style={styles.itemContainer}>
                    <Image style={styles.itemImageStyle} resizeMode="cover" source={require('../../assets/avatar-pending-2.png')} />
                    <View style={{ paddingVertical: hp(1.5), width: wp(43), }}>
                        <Text style={{ textAlign: 'center', fontSize: FontSize('medium'), fontFamily: Fonts.MulishMedium, color: '#333333' }}>Susan Chang</Text>
                        <Text style={{ textAlign: 'center', fontSize: FontSize('small'), fontFamily: Fonts.MulishBold, color: '#333333' }}>Realtor</Text>
                        <Text style={{ textAlign: 'center', fontSize: FontSize('small'), fontFamily: Fonts.MulishRegular, color: '#333333' }}>Los Angeles, CA</Text>
                    </View>
                    <View style={{ paddingTop: hp(1), paddingBottom: hp(2) }}>
                        <Button style={[styles.btnStyle, { backgroundColor: '#1156b4' }]}>
                            <Text style={{ color: '#ffffff', fontSize: FontSize('small'), fontFamily: Fonts.MulishLight }}>Exchange Contacts</Text>
                        </Button>
                    </View>
                    <View style={styles.referredStyle}>
                        <Image style={{ width: wp(4), height: hp(2), tintColor: '#fff' }} resizeMode="contain" source={require('../../assets/icons/reach-out.png')} />
                        <Text style={{ fontSize: FontSize('xMini'), fontFamily: Fonts.MulishLight, color: '#fff', paddingLeft: wp(1) }}>Referred: <Text style={{ fontFamily: Fonts.MulishBold, }}>108x</Text></Text>
                    </View>
                </View>
            </View>
            <View style={{ paddingHorizontal: wp(4), paddingTop: hp(2), paddingBottom: hp(1), }}>
                <Text style={{ fontSize: FontSize('large'), fontFamily: Fonts.MulishMedium, color: '#333333' }}>Closest in Reach</Text>
            </View>
            <View style={{ paddingHorizontal: wp(4), paddingVertical: hp(1), flexDirection: 'row', justifyContent: 'space-between', }}>
                <View style={styles.itemContainer}>
                    <Image style={styles.itemImageStyle} resizeMode="cover" source={require('../../assets/avatar-pending-3.png')} />
                    <View style={{ paddingVertical: hp(1.5), width: wp(43), }}>
                        <Text style={{ textAlign: 'center', fontSize: FontSize('medium'), fontFamily: Fonts.MulishMedium, color: '#333333' }}>David Smith</Text>
                        <Text style={{ textAlign: 'center', fontSize: FontSize('small'), fontFamily: Fonts.MulishBold, color: '#333333' }}>Dentist</Text>
                        <Text style={{ textAlign: 'center', fontSize: FontSize('small'), fontFamily: Fonts.MulishRegular, color: '#333333' }}>Los Angeles, CA</Text>
                    </View>
                    <View style={{ paddingTop: hp(1), paddingBottom: hp(2) }}>
                        <Button style={[styles.btnStyle, { backgroundColor: '#1156b4' }]}>
                            <Text style={{ color: '#ffffff', fontSize: FontSize('small'), fontFamily: Fonts.MulishLight }}>Exchange Contacts</Text>
                        </Button>
                    </View>
                    <View style={styles.referredStyle}>
                        <Image style={{ width: wp(4), height: hp(2), tintColor: '#fff' }} resizeMode="contain" source={require('../../assets/icons/reach-out.png')} />
                        <Text style={{ fontSize: FontSize('xMini'), fontFamily: Fonts.MulishLight, color: '#fff', paddingLeft: wp(1) }}>Referred: <Text style={{ fontFamily: Fonts.MulishBold, }}>108x</Text></Text>
                    </View>
                </View>
                <View style={styles.itemContainer}>
                    <Image style={styles.itemImageStyle} resizeMode="cover" source={require('../../assets/avatar-pending-4.png')} />
                    <View style={{ paddingVertical: hp(1.5), width: wp(43), }}>
                        <Text style={{ textAlign: 'center', fontSize: FontSize('medium'), fontFamily: Fonts.MulishMedium, color: '#333333' }}>Susan Chang</Text>
                        <Text style={{ textAlign: 'center', fontSize: FontSize('small'), fontFamily: Fonts.MulishBold, color: '#333333' }}>Realtor</Text>
                        <Text style={{ textAlign: 'center', fontSize: FontSize('small'), fontFamily: Fonts.MulishRegular, color: '#333333' }}>Los Angeles, CA</Text>
                    </View>
                    <View style={{ paddingTop: hp(1), paddingBottom: hp(2) }}>
                        <Button style={[styles.btnStyle, { backgroundColor: '#1156b4' }]}>
                            <Text style={{ color: '#ffffff', fontSize: FontSize('small'), fontFamily: Fonts.MulishLight }}>Exchange Contacts</Text>
                        </Button>
                    </View>
                    <View style={styles.referredStyle}>
                        <Image style={{ width: wp(4), height: hp(2), tintColor: '#fff' }} resizeMode="contain" source={require('../../assets/icons/reach-out.png')} />
                        <Text style={{ fontSize: FontSize('xMini'), fontFamily: Fonts.MulishLight, color: '#fff', paddingLeft: wp(1) }}>Referred: <Text style={{ fontFamily: Fonts.MulishBold, }}>108x</Text></Text>
                    </View>
                </View>
            </View>
        </ScrollView>
    );
}

const SecondRoute = () => {
    const [oldNew, setOldNew] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);
    const [value, setValue] = useState('');
    const ref = useBlurOnFulfill({ value, cellCount: 4 });
    const [props, getCellOnLayoutHandler] = useClearByFocusCell({ value, setValue });
    const [pinCode, setPinCode] = useState({ key: '0', lock: false })
    const [data, setData] = useState(
        [
            { key: "0", text: "Row 0", imagePath: '../../assets/avatar-circle.png', messageName: 'John Jmith', time: 'Today 5:30PM', lock: true, sent: true, received: false, read: false },
            { key: "1", text: "Row 1", imagePath: '../../assets/avatar-circle.png', messageName: 'John Jmith', time: 'Today 5:30PM', lock: false, sent: true, received: true, read: false },
            { key: "2", text: "Row 2", imagePath: '../../assets/avatar-circle.png', messageName: 'John Jmith', time: 'Today 5:30PM', lock: false, sent: true, received: true, read: true },
            { key: "3", text: "Row 3", imagePath: '../../assets/avatar-circle.png', messageName: 'John Jmith', time: 'Today 5:30PM', lock: false, sent: true, received: false, read: false },
            { key: "4", text: "Row 4", imagePath: '../../assets/avatar-circle.png', messageName: 'John Jmith', time: 'Today 5:30PM', lock: false, sent: true, received: false, read: false },
            { key: "5", text: "Row 4", imagePath: '../../assets/avatar-circle.png', messageName: 'John Jmith', time: 'Today 5:30PM', lock: false, sent: true, received: false, read: false }
        ]
    )

    const itemRefs = new Map();

    const deleteItem = (item: Item) => {
        const updatedData = data.filter((d) => d !== item);
        // Animate list to close gap when item is deleted
        LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
        setData(updatedData)
    };

    const renderUnderlayLeft = ({ item, percentOpen }: UnderlayParams<Item>) => (
        // Fade in on open
        <Animated.View style={[styles.row, styles.underlayLeft, { opacity: percentOpen }]}>
            <TouchableOpacity style={{ alignItems: 'center' }}>
                <Image style={{ width: wp(6), height: hp(3), }} resizeMode="contain" source={require('../../assets/icons/favorities.png')} />
                <Text style={{ fontFamily: Fonts.MulishLight, fontSize: FontSize('xMini'), color: '#434343' }}>Remove Favorties</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => setModalVisible(true)} style={{ alignItems: 'center' }}>
                <Image style={{ width: wp(7), height: hp(3), }} resizeMode="contain" source={require('../../assets/icons/unlock-2.png')} />
                <Text style={{ fontFamily: Fonts.MulishLight, fontSize: FontSize('xMini'), color: '#434343' }}>{item.lock ? 'Unlock' : 'Lock'}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPressOut={() => deleteItem(item)} style={{ alignItems: 'center' }}>
                <Image style={{ width: wp(6), height: hp(3), }} resizeMode="contain" source={require('../../assets/icons/delete.png')} />
                <Text style={{ fontFamily: Fonts.MulishLight, fontSize: FontSize('xMini'), color: '#bd0606' }}>Delete</Text>
            </TouchableOpacity>
        </Animated.View>
    );

    const renderItem = ({ item }) => {
        return (
            <SwipeableItem
                key={item.key}
                item={item}
                ref={(ref) => { if (ref && !itemRefs.get(item.key)) itemRefs.set(item.key, ref) }}
                onChange={({ open }) => {
                    if (open) {
                        // Close all other open items
                        [...itemRefs.entries()].forEach(([key, ref]) => {
                            if (key !== item.key && ref) ref.close();
                        });
                    }
                }}
                overSwipe={20}
                renderUnderlayLeft={renderUnderlayLeft}
                snapPointsLeft={[270]}
            >
                <View style={styles.row}>
                    <Image style={{ width: wp(18), height: hp(9) }} resizeMode="contain" source={require('../../assets/avatar-circle.png')} />
                    <View>
                        <Text style={{ fontSize: FontSize('large'), fontFamily: Fonts.MulishRegular, color: '#333333' }}>John Smith</Text>
                        <Text style={{ fontSize: 13, fontFamily: Fonts.MulishRegular, color: '#333333' }}>Lorem ipsum dolor sit amet...</Text>
                    </View>
                    <View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                            <Text style={{ fontSize: FontSize('small'), fontFamily: Fonts.MulishRegular, color: '#848484' }}>Today 5:30PM </Text>
                            <Image style={{ width: wp(4), height: hp(2), tintColor: '#b1b1b1' }} resizeMode="contain" source={require('../../assets/icons/arrow-right.png')} />
                        </View>
                        <Text></Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end', position: 'absolute', bottom: hp(1), right: wp(5) }}>
                        {
                            item.received
                                ? <Image style={{ width: wp(4), height: hp(2), tintColor: item.read ? '#074caa' : '#817f7f' }} resizeMode="contain" source={require('../../assets/icons/tick.png')} />
                                : null
                        }
                        {
                            item.sent
                                ? <Image style={{ width: wp(4), height: hp(2), tintColor: item.read ? '#074caa' : '#817f7f' }} resizeMode="contain" source={require('../../assets/icons/tick.png')} />
                                : null
                        }
                    </View>
                </View>
            </SwipeableItem>
        );
    };

    return (
        <View style={{ flex: 1, backgroundColor: '#fafafa' }}>
            <View style={{ width: '100%', paddingVertical: hp(2), paddingLeft: wp(4), paddingRight: wp(2), backgroundColor: '#f3f3f3', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <Text style={{ fontFamily: Fonts.MulishExtraBold, fontSize: 13, color: '#333333' }}>
                    {oldNew ? 'All Connections' : 'Favorites'}
                </Text>
                {
                    oldNew
                        ? <TouchableWithoutFeedback onPress={() => setOldNew(!oldNew)}>
                            <Image style={{ width: hp(4.5), height: hp(2.5) }} resizeMode="contain" source={require('../../assets/icons/carot-down.png')} />
                        </TouchableWithoutFeedback>
                        : <TouchableWithoutFeedback onPress={() => setOldNew(!oldNew)}>
                            <Image style={{ width: hp(4.5), height: hp(2.5) }} resizeMode="contain" source={require('../../assets/icons/carot-up.png')} />
                        </TouchableWithoutFeedback>
                }
            </View>
            <View>
                <FlatList
                    keyExtractor={(item) => item.key}
                    contentContainerStyle={{ paddingBottom: hp(20) }}
                    ItemSeparatorComponent={() => (<View style={{ width: wp(90), height: hp(0.1), marginHorizontal: wp(5), borderBottomColor: '#aaaaaa', borderBottomWidth: hp(0.1) }} />)}
                    data={data}
                    renderItem={renderItem}
                />
            </View>
            <View style={{ position: 'absolute', bottom: hp(2), right: wp(5) }}>
                <TouchableWithoutFeedback>
                    <Image style={{ width: wp(24), height: hp(12) }} resizeMode="contain" source={require('../../assets/icons/edit-blue.png')} />
                </TouchableWithoutFeedback>
            </View>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => setModalVisible(!modalVisible)}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <View style={{ alignItems: "center", }}>
                            <Text style={styles.modalText}>{pinCode.lock ? 'Unlock Conversation' : 'This Chat is Locked'}</Text>
                            <Text style={{ color: "#333333", fontSize: FontSize('small'), fontFamily: Fonts.MulishLight, paddingTop: hp(1) }}>Type in your 4 digit pin</Text>
                            <CodeField
                                ref={ref}
                                {...props}
                                value={value}
                                onChangeText={setValue}
                                cellCount={4}
                                rootStyle={styles.codeFieldRoot}
                                keyboardType="number-pad"
                                textContentType="oneTimeCode"
                                renderCell={({ index, symbol, isFocused }) => (
                                    <View
                                        // Make sure that you pass onLayout={getCellOnLayoutHandler(index)} prop to root component of "Cell"
                                        onLayout={getCellOnLayoutHandler(index)}
                                        key={index}
                                        style={[styles.cellRoot, isFocused && styles.focusCell]}
                                    >
                                        <Text style={styles.cellText}>
                                            {symbol || (isFocused ? <Cursor /> : null)}
                                        </Text>
                                    </View>
                                )}
                            />
                        </View>
                        <View style={{ paddingTop: hp(2) }}>
                            <Button style={[styles.btnStyle, { backgroundColor: '#1156b4', marginBottom: hp(0), width: wp(70), height: hp(7), }]}>
                                <Text style={{ color: '#ffffff', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishLight }}>{pinCode.lock ? 'Unlock' : 'Submit'}</Text>
                            </Button>
                        </View>
                    </View>
                </View>
            </Modal>
        </View>
    )
};

const DiscoverScreen = ({ navigation }) => {
    const [searchNeadby, setNearby] = useState(undefined)
    const [searchMessage, setMessage] = useState(undefined)
    const layout = useWindowDimensions();

    const [index, setIndex] = useState(0);
    const [routes] = useState([
        { key: 'first', title: 'Discover' },
        { key: 'second', title: 'Messages' },
    ]);

    const renderScene = SceneMap({
        first: FirstRoute,
        second: SecondRoute,
    });

    const renderTabBar = props => (
        <TabBar
            {...props}
            indicatorContainerStyle={{ marginLeft: wp(4) }}
            indicatorStyle={{ backgroundColor: '#074caa', height: 6, marginBottom: -6 }}
            style={{ backgroundColor: '#fff', elevation: 0, alignContent: 'center', borderBottomWidth: hp(0.1), borderBottomColor: '#e6e6e6' }}
            tabStyle={{ width: wp(23), height: hp(5.5), }}
            renderLabel={({ route, focused, color }) => (
                <Text style={{ color, fontSize: FontSize('medium'), fontFamily: Fonts.MulishLight, textTransform: 'capitalize', marginTop: -1, marginLeft: wp(6), width: wp(23), paddingLeft: wp(2) }}>
                    {route.title}
                </Text>
            )}
            activeColor={'#333333'}
            inactiveColor={'#074caa'}
        />
    );

    return (
        <View style={{ flex: 1 }}>
            <View style={{ paddingTop: hp(2), paddingBottom: hp(1), backgroundColor: '#fff' }}>
                <View style={{ flexDirection: 'row', width: wp(100), justifyContent: 'space-between', alignItems: 'center', height: hp(6), backgroundColor: '#fff' }}>
                    <TouchableOpacity onPress={() => navigation.toggleDrawer()} style={{ width: wp(12), alignItems: 'center', justifyContent: 'center' }}>
                        <Ionicons name="menu-sharp" size={35} />
                    </TouchableOpacity>
                    <View style={{ backgroundColor: '#fff', borderRadius: 5, elevation: 15, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: wp(2) }}>
                        <TextInput
                            value={index == 0 ? searchNeadby : searchMessage}
                            onChangeText={index == 0 ? setNearby : setMessage}
                            placeholder={index == 0 ? "Search Nearby" : "Search Messages"} placeholderTextColor="#333333"
                            style={styles.searchTextInput}
                        />
                        <AntDesign name="search1" size={24} />
                    </View>
                    <View style={{ width: wp(1) }} />
                </View>
            </View>
            <TabView
                navigationState={{ index, routes }}
                style={{ justifyContent: 'center' }}
                sceneContainerStyle={{ flexDirection: 'row' }}
                renderTabBar={renderTabBar}
                renderScene={renderScene}
                onIndexChange={setIndex}
                initialLayout={{ width: layout.width }}
            />
        </View>
    );
}

export default DiscoverScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    itemContainer: {
        width: wp(44),
        alignItems: 'center',
        borderRadius: 5,
        borderWidth: 0,
        backgroundColor: '#fff',
        elevation: 8
    },
    itemImageStyle: {
        width: wp(44),
        height: hp(20),
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5
    },
    btnStyle: {
        width: wp(36),
        height: hp(4.5),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        marginBottom: hp(1)
    },
    badgeStyle: {
        width: wp(6),
        height: hp(3.1),
        backgroundColor: '#0c51af',
        borderRadius: 15
    },
    referredStyle: {
        backgroundColor: '#000',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: wp(1.5),
        paddingVertical: hp(0.8),
        borderRadius: 5, opacity: 0.8,
        position: 'absolute',
        left: 3,
        top: 4
    },
    searchTextInput: {
        fontSize: FontSize('xMedium'),
        fontFamily: Fonts.MulishRegular,
        width: wp(70),
        color: '#333333'
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: hp(14),
        backgroundColor: '#fff',
        padding: 15,
    },
    text: {
        fontWeight: 'bold',
        color: 'black',
        fontSize: 32,
    },
    underlayLeft: {
        flex: 1,
        backgroundColor: '#f0f0f0',
        alignSelf: 'flex-end',
        width: wp(65),
    },
    button: { padding: 10, },
    centeredView: {
        flex: 1,
        justifyContent: "flex-end",
        alignItems: "center",
        backgroundColor: 'rgba(0,0,0,0.5)',
    },
    modalView: {
        backgroundColor: "white",
        width: wp(100),
        height: hp(40),
        paddingVertical: hp(4),
        justifyContent: 'space-between',
        alignItems: "center",
    },
    modalText: {
        color: "#333333",
        fontFamily: Fonts.MulishBold,
        fontSize: FontSize('large'),
    },
    textStyle: {
        color: "#333333",
        fontFamily: Fonts.MulishRegular,
        fontSize: 18,
        textAlign: "center"
    },
    codeFieldRoot: {
        marginTop: hp(1),
        width: wp(70),
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    cellRoot: {
        width: wp(14),
        height: hp(8),
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: '#ccc',
        borderBottomWidth: 1,
    },
    cellText: {
        color: '#000',
        fontSize: FontSize('x4Large'),
        textAlign: 'center',
    },
    focusCell: {
        borderBottomColor: '#007AFF',
        borderBottomWidth: 2,
    },
})