import WSAboutScreen from './WSAboutScreen';
import WSContactScreen from './WSContactScreen';

export { WSAboutScreen, WSContactScreen };