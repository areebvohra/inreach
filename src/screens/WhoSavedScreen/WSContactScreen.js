import React, { useState, useLayoutEffect } from 'react';
import { StyleSheet, Text, View, Image, ScrollView, TouchableWithoutFeedback, TouchableOpacity } from 'react-native';
import { Button } from 'native-base'

import { hp, wp } from '../../utils/utility';
import FontSize from '../../utils/FontSize';
import { Fonts } from '../../utils/Fonts';

const WSContactScreen = ({ navigation }) => {
    const [oldNew, setOldNew] = useState(false);

    useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: () => (
                <TouchableOpacity style={{ marginRight: wp(4) }}>
                    <Text style={{ fontSize: FontSize('small'), color: '#176fdb', fontFamily: Fonts.MulishRegular }}>Clear All</Text>
                </TouchableOpacity>
            )
        });
    }, [navigation]);


    return (
        <ScrollView contentContainerStyle={{ paddingBottom: hp(4) }} style={{ flex: 1, backgroundColor: '#fff' }}>
            <View style={{ width: '100%', paddingVertical: hp(2), paddingLeft: wp(4), paddingRight: wp(2), backgroundColor: '#f3f3f3', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <Text style={{ fontFamily: Fonts.MulishExtraBold, fontSize: 13, color: '#333333' }}>
                    {oldNew ? 'Most Recent' : 'Oldest to newest'}
                </Text>
                {
                    oldNew
                        ? <TouchableWithoutFeedback onPress={() => setOldNew(!oldNew)}>
                            <Image style={{ width: hp(4.5), height: hp(2.5) }} resizeMode="contain" source={require('../../assets/icons/carot-down.png')} />
                        </TouchableWithoutFeedback>
                        : <TouchableWithoutFeedback onPress={() => setOldNew(!oldNew)}>
                            <Image style={{ width: hp(4.5), height: hp(2.5) }} resizeMode="contain" source={require('../../assets/icons/carot-up.png')} />
                        </TouchableWithoutFeedback>
                }
            </View>
            <View style={{ paddingHorizontal: wp(4), paddingTop: hp(1.5), paddingBottom: hp(1.5), flexDirection: 'row', justifyContent: 'space-between', }}>
                <View>
                    <Text style={{ textAlign: 'center', fontFamily: Fonts.MulishRegular, fontSize: FontSize('xMini'), paddingBottom: hp(1.5), color: '#333333' }}>Saved on 10/25/21</Text>
                    <View style={styles.itemContainer}>
                        <Image style={styles.itemImageStyle} resizeMode="cover" source={require('../../assets/avatar-pending-1.png')} />
                        <View style={{ paddingVertical: hp(1.5), width: wp(43), }}>
                            <Text style={{ textAlign: 'center', fontSize: FontSize('medium'), fontFamily: Fonts.MulishMedium, color: '#333333' }}>David Smith</Text>
                            <Text style={{ textAlign: 'center', fontSize: FontSize('small'), fontFamily: Fonts.MulishBold, color: '#333333' }}>Dentist</Text>
                            <Text style={{ textAlign: 'center', fontSize: FontSize('small'), fontFamily: Fonts.MulishLight, color: '#333333' }}>Los Angeles, CA</Text>
                        </View>
                        <View style={styles.referredStyle}>
                            <Image style={{ width: wp(4), height: hp(2), tintColor: '#fff' }} resizeMode="contain" source={require('../../assets/icons/reach-out.png')} />
                            <Text style={{ fontSize: FontSize('xMini'), fontFamily: Fonts.MulishLight, color: '#fff', paddingLeft: wp(1) }}>Referred: <Text style={{ fontFamily: Fonts.MulishExtraBold, }}>108x</Text></Text>
                        </View>
                    </View>
                </View>
                <View>
                    <Text style={{ textAlign: 'center', fontFamily: Fonts.MulishRegular, fontSize: FontSize('xMini'), paddingBottom: hp(1.5), color: '#333333' }}>Saved on 10/25/21</Text>
                    <View style={styles.itemContainer}>
                        <Image style={styles.itemImageStyle} resizeMode="cover" source={require('../../assets/avatar-pending-2.png')} />
                        <View style={{ paddingVertical: hp(1.5), width: wp(43), }}>
                            <Text style={{ textAlign: 'center', fontSize: FontSize('medium'), fontFamily: Fonts.MulishMedium, color: '#333333' }}>Susan Chang</Text>
                            <Text style={{ textAlign: 'center', fontSize: FontSize('small'), fontFamily: Fonts.MulishBold, color: '#333333' }}>Realtor</Text>
                            <Text style={{ textAlign: 'center', fontSize: FontSize('small'), fontFamily: Fonts.MulishLight, color: '#333333' }}>Los Angeles, CA</Text>
                        </View>
                        <View style={styles.referredStyle}>
                            <Image style={{ width: wp(4), height: hp(2), tintColor: '#fff' }} resizeMode="contain" source={require('../../assets/icons/reach-out.png')} />
                            <Text style={{ fontSize: FontSize('xMini'), fontFamily: Fonts.MulishLight, color: '#fff', paddingLeft: wp(1) }}>Referred: <Text style={{ fontFamily: Fonts.MulishExtraBold, }}>108x</Text></Text>
                        </View>
                    </View>
                </View>
            </View>
            <View style={{ paddingHorizontal: wp(4), paddingTop: hp(1.5), flexDirection: 'row', justifyContent: 'space-between', }}>
                <View>
                    <Text style={{ textAlign: 'center', fontFamily: Fonts.MulishRegular, fontSize: FontSize('xMini'), paddingBottom: hp(1.5), color: '#333333' }}>Saved on 10/25/21</Text>
                    <View style={styles.itemContainer}>
                        <Image style={styles.itemImageStyle} resizeMode="cover" source={require('../../assets/avatar-pending-1.png')} />
                        <View style={{ paddingVertical: hp(1.5), width: wp(43), }}>
                            <Text style={{ textAlign: 'center', fontSize: FontSize('medium'), fontFamily: Fonts.MulishMedium, color: '#333333' }}>David Smith</Text>
                            <Text style={{ textAlign: 'center', fontSize: FontSize('small'), fontFamily: Fonts.MulishBold, color: '#333333' }}>Dentist</Text>
                            <Text style={{ textAlign: 'center', fontSize: FontSize('small'), fontFamily: Fonts.MulishLight, color: '#333333' }}>Los Angeles, CA</Text>
                        </View>
                        <View style={styles.referredStyle}>
                            <Image style={{ width: wp(4), height: hp(2), tintColor: '#fff' }} resizeMode="contain" source={require('../../assets/icons/reach-out.png')} />
                            <Text style={{ fontSize: FontSize('xMini'), fontFamily: Fonts.MulishLight, color: '#fff', paddingLeft: wp(1) }}>Referred: <Text style={{ fontFamily: Fonts.MulishExtraBold, }}>108x</Text></Text>
                        </View>
                    </View>
                </View>
                <View>
                    <Text style={{ textAlign: 'center', fontFamily: Fonts.MulishRegular, fontSize: FontSize('xMini'), paddingBottom: hp(1.5), color: '#333333' }}>Saved on 10/25/21</Text>
                    <View style={styles.itemContainer}>
                        <Image style={styles.itemImageStyle} resizeMode="cover" source={require('../../assets/avatar-pending-2.png')} />
                        <View style={{ paddingVertical: hp(1.5), width: wp(43), }}>
                            <Text style={{ textAlign: 'center', fontSize: FontSize('medium'), fontFamily: Fonts.MulishMedium, color: '#333333' }}>Susan Chang</Text>
                            <Text style={{ textAlign: 'center', fontSize: FontSize('small'), fontFamily: Fonts.MulishBold, color: '#333333' }}>Realtor</Text>
                            <Text style={{ textAlign: 'center', fontSize: FontSize('small'), fontFamily: Fonts.MulishLight, color: '#333333' }}>Los Angeles, CA</Text>
                        </View>
                        <View style={styles.referredStyle}>
                            <Image style={{ width: wp(4), height: hp(2), tintColor: '#fff' }} resizeMode="contain" source={require('../../assets/icons/reach-out.png')} />
                            <Text style={{ fontSize: FontSize('xMini'), fontFamily: Fonts.MulishLight, color: '#fff', paddingLeft: wp(1) }}>Referred: <Text style={{ fontFamily: Fonts.MulishExtraBold, }}>108x</Text></Text>
                        </View>
                    </View>
                </View>
            </View>
        </ScrollView>
    );
}

export default WSContactScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    itemContainer: { width: wp(44), alignItems: 'center', borderRadius: 5, borderWidth: 0, backgroundColor: '#fff', elevation: 8 },
    itemImageStyle: { width: wp(44), height: hp(20), borderTopLeftRadius: 5, borderTopRightRadius: 5 },
    btnStyle: { width: wp(34), height: hp(4.5), justifyContent: 'center', alignItems: 'center', borderRadius: 4, marginBottom: hp(1) },
    referredStyle: { backgroundColor: '#000', flexDirection: 'row', alignItems: 'center', paddingHorizontal: wp(1.5), paddingVertical: hp(0.8), borderRadius: 5, opacity: 0.8, position: 'absolute', left: 3, top: 4 }
})