import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { Button } from 'native-base'

import FontSize from '../../utils/FontSize';
import { Fonts } from '../../utils/Fonts';
import { hp, wp } from '../../utils/utility';

const WSAboutScreen = ({ navigation }) => (
    <View style={styles.container}>
        <View style={{ alignItems: 'center', paddingHorizontal: wp(8), paddingTop: hp(4) }}>
            <Image style={{ width: wp(24), height: hp(12) }} resizeMode="contain" source={require('../../assets/icons/search.png')} />
            <Text style={{ fontSize: 16, fontFamily: Fonts.MulishExtraBold, paddingTop: hp(3), color: '#333333' }}>See who saved your profile information</Text>
            <Text style={{ fontSize: 16, fontFamily: Fonts.MulishRegular, lineHeight: 25, textAlign: 'left', paddingTop: hp(3), color: '#333333' }}>Public profiles are ideal for events such as Trade Shows, Open Houses, Conventions, and networking events. <Text style={{ fontSize: 14 }}>(Privacy Settings)</Text>{'\n\n'}If you set your profile to public other people can save your contact info with a click of a button without you having to approve it.{'\n\n'}You won’t get their contact information but you will get a list of everyone that saved your contact. This comes in handy when creating Offers & Promtions.{'\n\n'}You can select “Who saved my contacts” as an option to send marketing and deals to.</Text>
        </View>
        <View>
            <Button onPress={() => navigation.navigate('WSContact')} style={[styles.doneBtn, { backgroundColor: '#1156b4' }]}>
                <Text style={{ color: '#ffffff', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishLight }}>Got It</Text>
            </Button>
        </View>
    </View>
);

export default WSAboutScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff',
        justifyContent: 'space-between',
        paddingBottom: hp(2)
    },
    doneBtn: {
        width: wp(80),
        height: hp(6),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        marginBottom: hp(1),
        backgroundColor: '#074caa'
    },
    doneBtnText: {
        color: '#ffffff',
        fontSize: 18,
        fontFamily: Fonts.MulishLight
    },
})