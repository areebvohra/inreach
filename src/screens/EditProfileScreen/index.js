import React, { useState } from 'react';
import { StyleSheet, Text, View, useWindowDimensions, Image, ScrollView, TextInput, TouchableOpacity, TouchableWithoutFeedback, ImageBackground } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { Button } from 'native-base'
import { useNavigation } from '@react-navigation/native';

import { hp, wp } from '../../utils/utility';
import FontSize from '../../utils/FontSize';
import { Fonts } from '../../utils/Fonts';

const FirstRoute = () => {
    const navigation = useNavigation();

    const [name, setName] = useState('Nojan Mahjour')
    const [email, setEmail] = useState('nmahjour@gmail.com')
    const [conpany, setCompany] = useState('')
    const [website, setWebsite] = useState('')
    const [address, setAddress] = useState('')
    const [zipcode, setZipcode] = useState('')
    const [video, setVideo] = useState('')

    return (
        <ScrollView style={{ flex: 1, backgroundColor: '#fafafa' }} contentContainerStyle={styles.container}>
            <View style={[styles.profileImageContainer, styles.borderTopStyle, styles.borderBottomStyle]}>
                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
                    <Image style={{ width: wp(20), height: hp(10), backgroundColor: '#fff', borderRadius: 40 }} resizeMode="cover" source={require('../../assets/avatar-circle.png')} />
                    <Image style={{ width: wp(4), height: hp(2), left: -10, bottom: -4 }} resizeMode="cover" source={require('../../assets/icons/edit.png')} />
                </TouchableOpacity>
                <Text style={{ fontSize: FontSize('xLarge'), fontFamily: Fonts.MulishExtraBold, paddingVertical: hp(1), color: '#333333' }}>Nojan Mahjour</Text>
            </View>
            <View style={[styles.inputContainer, styles.borderTopStyle, styles.borderBottomStyle]}>
                <TextInput
                    value={name}
                    placeholder="Username:"
                    placeholderTextColor="#000"
                    onChangeText={(value) => setName(value)}
                    style={styles.inputStyle}
                />
            </View>
            <View style={[styles.inputContainer, styles.borderBottomStyle]}>
                <TextInput
                    value={email}
                    placeholder="Email:"
                    placeholderTextColor="#000"
                    onChangeText={(value) => setEmail(value)}
                    style={styles.inputStyle}
                />
            </View>
            <TouchableOpacity style={[styles.openListStyle, styles.borderBottomStyle]}>
                <Text style={{ fontFamily: Fonts.MulishRegular, fontSize: FontSize('xMedium'), color: '#333333' }}>Occupation: </Text>
                <Image style={{ width: wp(4), height: hp(2) }} resizeMode="contain" source={require('../../assets/icons/arrow-right.png')} />
            </TouchableOpacity>
            <View style={[styles.inputContainer, styles.borderBottomStyle]}>
                <TextInput
                    value={conpany}
                    placeholder="Company Name:"
                    placeholderTextColor="#000"
                    onChangeText={(value) => setCompany(value)}
                    style={styles.inputStyle}
                />
            </View>
            <View style={[styles.inputContainer, styles.borderBottomStyle, { flexDirection: 'row', alignItems: 'center', }]}>
                <Image style={{ width: wp(6), height: hp(3) }} resizeMode="contain" source={require('../../assets/icons/web.png')} />
                <TextInput
                    value={website}
                    placeholder="Website:"
                    placeholderTextColor="#000"
                    onChangeText={(value) => setWebsite(value)}
                    style={[styles.inputStyle, { paddingLeft: wp(2) }]}
                />
            </View>
            <View style={{ width: wp(100), height: hp(1) }} />
            <View style={[styles.inputContainer, styles.borderBottomStyle, styles.borderTopStyle]}>
                <TextInput
                    value={address}
                    placeholder="Address:"
                    placeholderTextColor="#000"
                    onChangeText={(value) => setAddress(value)}
                    style={styles.inputStyle}
                />
            </View>
            <TouchableOpacity style={[styles.openListStyle, styles.borderBottomStyle]}>
                <Text style={{ fontFamily: Fonts.MulishRegular, fontSize: FontSize('xMedium'), color: '#333333' }}>City: </Text>
                <Image style={{ width: wp(4), height: hp(2) }} resizeMode="contain" source={require('../../assets/icons/arrow-right.png')} />
            </TouchableOpacity>
            <View style={[styles.inputContainer, styles.borderBottomStyle]}>
                <TextInput
                    value={zipcode}
                    placeholder="Zipcode:"
                    placeholderTextColor="#000"
                    onChangeText={(value) => setZipcode(value)}
                    style={styles.inputStyle}
                />
            </View>
            <View style={{ width: wp(100), height: hp(1) }} />
            <TouchableOpacity onPress={() => navigation.navigate('socialConnect')} style={[styles.openListStyle, styles.borderBottomStyle, styles.borderTopStyle]}>
                <Text style={{ fontFamily: Fonts.MulishRegular, fontSize: FontSize('xMedium'), color: '#333333' }}>Social Media Connects: </Text>
                <Image style={{ width: wp(4), height: hp(2) }} resizeMode="contain" source={require('../../assets/icons/arrow-right.png')} />
            </TouchableOpacity>
            <View style={{ width: wp(100), height: hp(1) }} />
            <View style={[styles.inputContainer, styles.borderBottomStyle, styles.borderTopStyle, { flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }]}>
                <TextInput
                    value={video}
                    placeholder="Video Trailer URL:  "
                    placeholderTextColor="#000"
                    onChangeText={(value) => setVideo(value)}
                    style={styles.inputStyle}
                />
                <Image style={{ width: wp(6), height: hp(3) }} resizeMode="contain" source={require('../../assets/icons/no-url.png')} />
            </View>
            <View style={{ width: wp(100), height: hp(1) }} />
            <TouchableOpacity style={[styles.openListStyle, styles.borderBottomStyle, styles.borderTopStyle]}>
                <Text style={{ fontFamily: Fonts.MulishRegular, fontSize: FontSize('xMedium'), color: '#333333' }}>Report a Problem</Text>
            </TouchableOpacity>
            <View style={{ width: wp(100), height: hp(0.6) }} />
            <View style={[styles.borderTopStyle, { alignItems: 'center', backgroundColor: '#fff', width: wp(100) }]}>
                <View style={{ flexDirection: 'row', alignItems: 'center', paddingTop: hp(1), paddingBottom: hp(3) }}>
                    <TouchableWithoutFeedback>
                        <Text style={{ fontSize: FontSize('xMini'), color: '#074caa', fontFamily: Fonts.MulishMedium }}>Terms & Conditions</Text>
                    </TouchableWithoutFeedback>
                    <View style={{ width: wp(10), alignItems: 'center', }}>
                        <View style={{ borderLeftColor: '#074caa', borderLeftWidth: wp(0.2), height: hp(2) }} />
                    </View>
                    <TouchableWithoutFeedback>
                        <Text style={{ fontSize: FontSize('xMini'), color: '#074caa', fontFamily: Fonts.MulishMedium }}>Privacy Policy</Text>
                    </TouchableWithoutFeedback>
                </View>
                <View>
                    <Button style={[styles.doneBtn, { backgroundColor: '#1156b4' }]}>
                        <Text style={{ color: '#ffffff', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishLight }}>Save Changes</Text>
                    </Button>
                </View>
            </View>
        </ScrollView>
    );
}

const SecondRoute = () => {
    const navigation = useNavigation();

    return (
        <ScrollView style={{ flex: 1, backgroundColor: '#fafafa' }} contentContainerStyle={{ alignItems: 'center', paddingBottom: hp(3) }}>
            <View style={{ height: hp(1.3) }} />
            <ImageBackground source={require('../../assets/view-profile-background.png')} style={{ width: wp(100), height: hp(26), alignItems: 'center', justifyContent: 'space-between', }}>
                <View style={{ height: hp(3) }} />
                <Image style={{ width: wp(30), height: hp(15), backgroundColor: '#fff', borderRadius: 60, borderColor: '#fff', borderWidth: 1 }} resizeMode="cover" source={require('../../assets/avatar-circle.png')} />
                <View style={{ height: hp(3) }} />
                <View style={{ backgroundColor: '#032252', width: wp(100), flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <Image style={{ width: wp(4), height: hp(2), tintColor: '#fff' }} resizeMode="contain" source={require('../../assets/icons/reach-out.png')} />
                    <Text style={{ fontSize: FontSize('small'), fontFamily: Fonts.MulishLight, color: '#fff', paddingVertical: hp(1.1), paddingLeft: wp(2) }}>Referred by: 108 people</Text>
                </View>
                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', position: 'absolute', right: 14, top: hp(2) }}>
                    <Image style={{ width: wp(5), height: hp(2.5), tintColor: '#fff' }} resizeMode="contain" source={require('../../assets/icons/video.png')} />
                    <Text style={{ fontSize: FontSize('small'), fontFamily: Fonts.MulishLight, color: '#fff', paddingVertical: hp(1.1), paddingLeft: wp(2) }}>Watch Trailer</Text>
                </TouchableOpacity>
            </ImageBackground>
            <View style={{ height: hp(0.8) }} />
            <View style={{ width: wp(100), alignItems: 'flex-start', }}>
                <View style={[styles.staticContainer, styles.borderBottomStyle, styles.borderTopStyle]}>
                    <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, color: '#333333', fontWeight: 'bold' }}>Name: </Text>
                    <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, color: '#333333' }}>Nojan Mahjour</Text>
                </View>
                <View style={[styles.staticContainer, styles.borderBottomStyle]}>
                    <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, color: '#333333', fontWeight: 'bold' }}>Email: </Text>
                    <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, color: '#104ca7' }}>nmahjour@gmail.com</Text>
                </View>
                <View style={{ height: hp(1.5) }} />
                <View style={[styles.staticContainer, styles.borderBottomStyle, styles.borderTopStyle]}>
                    <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, fontWeight: 'bold' }}>Occupation: </Text>
                    <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, }}>Doctor</Text>
                </View>
                <View style={[styles.staticContainer, styles.borderBottomStyle]}>
                    <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, color: '#333333', fontWeight: 'bold' }}>Company: </Text>
                    <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, color: '#333333' }}>Jour Medical</Text>
                </View>
                <View style={{ height: hp(1.5) }} />
                <View style={[styles.staticContainer, styles.borderBottomStyle, styles.borderTopStyle]}>
                    <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, color: '#333333', fontWeight: 'bold' }}>Address: </Text>
                    <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, color: '#333333' }}>12345 Street Ave, #345</Text>
                </View>
                <View style={[styles.staticContainer, styles.borderBottomStyle]}>
                    <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, color: '#333333', fontWeight: 'bold' }}>City: </Text>
                    <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, color: '#333333' }}>Woodland Hills</Text>
                </View>
                <View style={[styles.staticContainer, styles.borderBottomStyle]}>
                    <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, color: '#333333', fontWeight: 'bold' }}>Zipcode: </Text>
                    <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, color: '#333333' }}>97867</Text>
                </View>
                <View style={{ height: hp(1.5) }} />
                <View style={[styles.staticContainer, styles.borderBottomStyle, styles.borderTopStyle]}>
                    <Image style={{ width: wp(6), height: hp(3), tintColor: '#000' }} resizeMode="contain" source={require('../../assets/icons/reach-out.png')} />
                    <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, color: '#104ca7', paddingLeft: wp(3) }}>Refer Contact</Text>
                </View>
                <View style={{ height: hp(0.8) }} />
                <View style={[styles.staticContainer, styles.borderBottomStyle, styles.borderTopStyle]}>
                    <Image style={{ width: wp(6), height: hp(3) }} resizeMode="contain" source={require('../../assets/icons/web.png')} />
                    <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, color: '#104ca7', paddingLeft: wp(3) }}>www.jourmedical.com</Text>
                </View>
                <View style={{ height: hp(0.8) }} />
                <TouchableOpacity onPress={() => navigation.navigate('socialConnect')} style={[styles.openListStyle, styles.borderBottomStyle, styles.borderTopStyle]}>
                    <Text style={{ fontFamily: Fonts.MulishRegular, fontSize: FontSize('xMedium'), color: '#333333' }}>Social Media Connects: </Text>
                    <Image style={{ width: wp(4), height: hp(2) }} resizeMode="contain" source={require('../../assets/icons/arrow-right.png')} />
                </TouchableOpacity>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: wp(100), paddingHorizontal: wp(5), paddingTop: hp(2) }}>
                <Image style={{ width: wp(22), height: hp(11) }} resizeMode="contain" source={require('../../assets/icons/view-video.png')} />
                <Image style={{ width: wp(22), height: hp(11) }} resizeMode="contain" source={require('../../assets/icons/view-phone.png')} />
                <Image style={{ width: wp(22), height: hp(11) }} resizeMode="contain" source={require('../../assets/icons/edit-blue.png')} />
            </View>
        </ScrollView>
    )
};

const EditProfileScreen = ({ navigation }) => {

    const layout = useWindowDimensions();

    const [index, setIndex] = useState(0);
    const [routes] = useState([
        { key: 'first', title: 'Edit Profile' },
        { key: 'second', title: 'View Profile' },
    ]);

    const renderScene = SceneMap({
        first: FirstRoute,
        second: SecondRoute,
    });

    const renderTabBar = props => (
        <TabBar
            {...props}
            indicatorContainerStyle={{}}
            indicatorStyle={[{
                backgroundColor: '#074caa', height: 6, marginBottom: -6, width: wp(26)
            }, props.navigationState.index == 0 ? { marginLeft: wp(21) } : { marginLeft: wp(3) }]}
            style={{ backgroundColor: '#fff', elevation: 0, borderBottomWidth: hp(0.1), borderTopWidth: hp(0.1), borderBottomColor: '#e6e6e6', borderTopColor: '#e6e6e6', paddingHorizontal: wp(18) }}
            tabStyle={{ height: hp(5.5), borderWidth: 0, }}
            renderLabel={({ route, focused, color }) => (
                <Text style={{ color, fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishMedium, textTransform: 'capitalize', marginTop: -5, width: wp(26), textAlign: 'center', height: hp(4.5), textAlignVertical: 'center' }}>
                    {route.title}
                </Text>
            )}
            activeColor={'#333333'}
            inactiveColor={'#074caa'}
        />
    );

    return (
        <View style={{ flex: 1 }}>
            <View style={{ flexDirection: 'row', width: wp(100), justifyContent: 'space-between', alignItems: 'center', height: hp(8), backgroundColor: '#fff', }}>
                <TouchableOpacity
                    onPress={() => navigation.goBack()}
                    style={{ width: wp(12), alignItems: 'center', justifyContent: 'center' }}
                >
                    <Image style={{ width: wp(5), height: hp(2.5) }} resizeMode='contain' source={require('../../assets/icons/back-icon.png')} />
                </TouchableOpacity>
                <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishExtraBold, color: '#333333' }}>{index == 0 ? 'Edit Profile' : 'Nojan Mahjour'}</Text>
                <View style={{ width: wp(12) }} />
            </View>
            <TabView
                navigationState={{ index, routes }}
                style={{ justifyContent: 'center' }}
                sceneContainerStyle={{ flexDirection: 'row' }}
                renderTabBar={renderTabBar}
                renderScene={renderScene}
                onIndexChange={setIndex}
                initialLayout={{ width: layout.width }}
            />
        </View>
    );
}

export default EditProfileScreen;

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        paddingBottom: hp(5)
    },
    inputStyle: {
        fontFamily: Fonts.MulishRegular,
        fontSize: FontSize('xMedium'),
        paddingLeft: 0,
        color: '#333333',
        width: wp(80)
    },
    doneBtn: {
        width: wp(80),
        height: hp(6),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        marginBottom: hp(1),
        backgroundColor: '#074caa'
    },
    doneBtnText: {
        color: '#ffffff',
        fontSize: 18,
        fontFamily: Fonts.MulishLight
    },
    inputContainer: {
        width: wp(100),
        backgroundColor: '#fff',
        paddingHorizontal: wp(4)
    },
    openListStyle: {
        width: wp(100),
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: hp(1.8),
        paddingHorizontal: wp(4)
    },
    borderTopStyle: {
        borderTopColor: '#e6e6e6',
        borderTopWidth: hp(0.1)
    },
    borderBottomStyle: {
        borderBottomColor: '#e6e6e6',
        borderBottomWidth: hp(0.1)
    },
    profileImageContainer: {
        backgroundColor: '#fff',
        width: wp(100),
        alignItems: 'center',
        marginVertical: hp(1),
        paddingVertical: hp(3)
    },
    staticContainer: {
        flexDirection: 'row',
        paddingVertical: hp(1.5),
        alignItems: 'center',
        paddingLeft: wp(4),
        width: wp(100),
        backgroundColor: '#fff'
    }
})