import React, { useEffect, useState } from 'react';
import { View, Image, Text } from 'react-native';
import { StackActions } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { hp, wp } from '../../utils/utility';
import FontSize from '../../utils/FontSize';
import { Fonts } from '../../utils/Fonts';

const SplashScreen = ({ navigation }) => {

    useEffect(() => {
        async function getData() {
            let value = await AsyncStorage.getItem('@login_status');
            setTimeout(() => {
                value == '1'
                    ? navigation.dispatch(StackActions.replace('HomeDrawer'))
                    : navigation.dispatch(StackActions.replace('welcome'))
            }, 700);
        }

        getData();
        return function cleanup() {
            clearTimeout();
        };
    })

    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff' }}>
            <View style={{ flex: 2, justifyContent: 'flex-end' }}>
                <Image style={{ width: wp(50), height: hp(25) }} resizeMode="contain" source={require('../../assets/app-icon.png')} />
            </View>
            <View style={{ flex: 1, justifyContent: 'flex-end', paddingBottom: hp(6) }}>
                <Text style={{ color: '#0a54b3', fontSize: FontSize('x5Large'), fontFamily: Fonts.MulishRegular }}>In Reach</Text>
            </View>
        </View>
    );
}
export default SplashScreen;