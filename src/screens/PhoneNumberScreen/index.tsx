import React, { useState, useLayoutEffect } from 'react';
import { Text, StyleSheet, View, TouchableOpacity, } from 'react-native';
import CountryPicker, { CountryModalProvider } from 'react-native-country-picker-modal';
import { CountryCode, Country } from 'react-native-country-picker-modal/lib/types';
import TextInputMask from 'react-native-text-input-mask';
import { Fonts } from '../../utils/Fonts';

import { hp, wp } from '../../utils/utility';

const PhoneNumberScreen = ({ navigation }) => {

    const [countryCode, setCountryCode] = useState<CountryCode>('US')
    const [country, setCountry] = useState<Country>({ "callingCode": ["1"], "cca2": "US", "currency": ["USD"], "flag": "flag-us", "name": "United States", "region": "Americas", "subregion": "North America" })
    const [visible, setVisible] = useState<boolean>(false)
    const [phoneNumber, setPhoneNumber] = useState<string>('')
    const onSelect = (country: Country) => {
        setCountryCode(country.cca2)
        setCountry(country)
    }

    const changeScreen = () => {
        if (phoneNumber == '') {
            alert('Please Enter Phone Number');
        }
        else if (phoneNumber.length < 12) {
            alert('Please Enter Full Number');
        }
        else {
            navigation.navigate('AuthenticateCode', { phone: phoneNumber, callingCode: country.callingCode })
        }
    }

    useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: () => (
                <TouchableOpacity onPress={changeScreen}>
                    <Text style={{ fontSize: 18, color: '#176fdb', fontWeight: 'bold', fontFamily: Fonts.MulishBold }}>Next</Text>
                </TouchableOpacity>
            ),
        });
    }, [navigation, phoneNumber, country]);




    return (
        <View style={styles.container}>
            <Text style={{ fontSize: 28, fontFamily: Fonts.MulishMedium, color: '#333333' }}> Your Phone </Text>
            <Text style={{ fontSize: 16, paddingHorizontal: wp(16), paddingVertical: wp(2), textAlign: 'center', fontFamily: Fonts.MulishRegular, color: '#333333' }}> Please confirm your country code and enter phone number. </Text>

            <View style={[styles.section, { marginTop: hp(3) }]}>
                <CountryModalProvider>
                    <View style={styles.conuntryPickerContainer}>
                        <CountryPicker
                            theme={{ fontSize: 18, }} // fontSize for the text of country name
                            countryCode={countryCode}
                            withCountryNameButton={true}
                            withFlag={true}
                            withEmoji={true}
                            withFilter={true}
                            withCallingCode={true}
                            withAlphaFilter={true}
                            withModal={true}
                            withFlagButton={true}
                            preferredCountries={['US', 'GB']}
                            onClose={() => setVisible(false)}
                            onOpen={() => setVisible(true)}
                            {...{ onSelect, modalProps: { visible } }}
                        />
                    </View>
                </CountryModalProvider>
            </View>
            <View style={[styles.section, { borderBottomWidth: hp(0.1), borderBottomColor: '#e6e6e6', flexDirection: 'row' }]}>
                <Text style={styles.callingCode}>+{country.callingCode}</Text>
                <View style={{ borderLeftWidth: hp(0.1), borderLeftColor: '#e6e6e6' }}>
                    <TextInputMask
                        onChangeText={(formatted, extracted) => setPhoneNumber(formatted)}
                        value={phoneNumber}
                        keyboardType={'numeric'}
                        style={{ paddingLeft: wp(3), fontSize: 24, color: '#333333' }}
                        placeholder={"--- --- ----"}
                        placeholderTextColor="grey"
                        mask={"[000] [000] [0000]"}
                    />
                </View>
            </View>
        </View>
    );
}

export default PhoneNumberScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        paddingTop: hp(15)
    },
    conuntryPickerContainer: {
        justifyContent: 'center',
        paddingVertical: hp(1.5),
    },
    section: {
        paddingHorizontal: hp(2),
        borderTopWidth: hp(0.1),
        borderTopColor: '#e6e6e6',
        width: wp(100),
    },
    callingCode: {
        paddingLeft: wp(8),
        paddingRight: wp(5),
        fontSize: 24,
        paddingVertical: hp(1.5),
        fontFamily: Fonts.MulishRegular,
        color: '#333333'
    }
})
