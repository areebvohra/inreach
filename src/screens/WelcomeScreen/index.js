import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, Image, TouchableOpacity } from 'react-native';
import { Button, Container, Icon } from 'native-base';
import { CommonActions } from '@react-navigation/native';

import AppIntroSlider from '../../components/AppIntroSlider';
import { wp, hp } from '../../utils/utility';
import FontSize from '../../utils/FontSize';
import { Fonts } from '../../utils/Fonts';

const Slides = [
    {
        key: 'one',
        title: 'In Reach',
        text: ['Build your ', ' of professionals in '],
        bold: ['network', 'real time.'],
        image: require('../../assets/slides/slide1.png')
    },
    {
        key: 'two',
        title: 'Free',
        text: ['Seperate your personal and business Text Messaging \& Calls. View your contact list by occupations and access their entire professional profile all for FREE.', ''],
        bold: ['', ''],
        image: require('../../assets/slides/slide2.png')
    },
    {
        key: 'three',
        title: 'Profile',
        text: ['Create your profile and include business details you would like to share with others when you exchange contacts.', ''],
        bold: ['', ''],
        image: require('../../assets/slides/slide3.png')
    },
    {
        key: 'four',
        title: 'Invite',
        text: ['Invite your contacts to join In Reach to help you start building your network.', ''],
        bold: ['', ''],
        image: require('../../assets/slides/slide4.png')
    },
    {
        key: 'five',
        title: 'Network',
        text: ['Exchange contacts using In Reach and build a network of trusted professionals in real time.', ''],
        bold: ['', ''],
        image: require('../../assets/slides/slide5.png')
    },
    {
        key: 'six',
        title: 'Public',
        text: ['At trade shows / events you can set your profile to public and allow users to quickly save your contact. Then market to the people that took intrest in your business.', ''],
        bold: ['', ''],
        image: require('../../assets/slides/slide6.png')
    },
    {
        key: 'seven',
        title: 'Marketing',
        text: ['Send Offers & Promotions to selected contact categories. Ad\'s / Discounts / Real Estate Listings.', ''],
        bold: ['', ''],
        image: require('../../assets/slides/slide7.png')
    },
    {
        key: 'eight',
        title: 'Reach Out',
        text: ['Ask your network  for  professional recommendations. Each time a referral is made the recognition is added to the user\'s profile to help build their credibilty.', ''],
        bold: ['', ''],
        image: require('../../assets/slides/slide8.png')
    },
];

class WelcomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    _renderItem = ({ item }) => {
        return (
            <View style={styles.mainContent}>

                {/*  Main Image  */}
                <View style={[styles.mainRow, { paddingVertical: 0 }]}>
                    <Image style={{ width: wp(50), height: hp(25) }} resizeMode="contain" source={item.image} />
                </View>

                {/*  Main Title  */}
                <View style={[styles.mainRow, { paddingHorizontal: wp(6) }]}>
                    <Text style={styles.title}>{item.title}</Text>
                </View>

                {/*  Main Text  */}
                <View style={[styles.mainRow, { paddingHorizontal: wp(8) }]}>
                    <Text style={{ textAlign: 'center', color: '#333333' }}>
                        <Text style={styles.text}>{item.text[0]}</Text>
                        <Text style={[styles.text, { fontWeight: 'bold' }]}>{item.bold[0]}</Text>
                        <Text style={styles.text}>{item.text[1]}</Text>
                        <Text style={[styles.text, { fontWeight: 'bold' }]}>{item.bold[1]}</Text>
                    </Text>
                </View>

            </View>
        );
    };

    // _renderNextButton = () => {
    //     return (
    //         <View style={{ flexDirection: 'row', alignItems: 'center' }}>
    //             <Text style={styles.buttonText}>Next</Text>
    //             <Icon type='FontAwesome' name='angle-right' style={[styles.iconColor, { fontSize: FontSize('large'), paddingVertical: 15 }]} />
    //         </View>
    //     );
    // };

    // _renderPrevButton = () => {
    //     return (
    //         <View style={{ flexDirection: 'row', alignItems: 'center' }}>
    //             <Icon type='FontAwesome' name='angle-left' style={[styles.iconColor, { fontSize: FontSize('large'), paddingVertical: 15 }]} />
    //             <Text style={styles.buttonText}>Previous</Text>
    //         </View>
    //     );
    // };

    // _renderDoneButton = () => {
    //     return (
    //         <View style={{ flexDirection: 'row', alignItems: 'center' }}>
    //             <Text style={styles.buttonText}>Finish</Text>
    //             <Icon type='FontAwesome' name='angle-right' style={[styles.iconColor, { fontSize: FontSize('large'), paddingVertical: 15 }]} />
    //         </View>
    //     );
    // };

    // _onDoneButtonHandler = async () => {
    //     console.log(this.state.route)
    //     try {
    //         if (this.state.route == null) {

    //         } else {

    //         }
    //     } catch (error) {
    //         console.log(error)
    //     }
    // }

    _keyExtractor = (item) => item.key;

    render() {
        return (
            <Container style={{ flex: 1, }}>
                <View style={{ flex: 1, paddingTop: hp(6) }}>
                    <AppIntroSlider
                        data={Slides}
                        // showPrevButton={true}
                        keyExtractor={this._keyExtractor}
                        renderItem={this._renderItem}
                        // renderNextButton={this._renderNextButton}
                        // renderPrevButton={this._renderPrevButton}
                        // renderDoneButton={this._renderDoneButton}
                        // onDone={this._onDoneButtonHandler}
                        activeDotStyle={styles.activeDot}
                        dotStyle={styles.dot}
                    />
                </View>
                <View style={{ alignItems: 'center', height: hp(24), paddingHorizontal: wp(20) }}>
                    <Button
                        onPress={() => this.props.navigation.navigate('CreateProfileStack')}
                        block
                        style={[styles.BottomBtn, { backgroundColor: '#0A54B3' }]}
                    >
                        <Text style={{ fontSize: FontSize('large'), color: '#fff', fontFamily: Fonts.MulishMedium }}>Sign Up</Text>
                    </Button>
                    <View style={{ height: hp(2) }} />
                    <Button
                        onPress={() => this.props.navigation.navigate('AuthStack')}
                        block
                        style={[styles.BottomBtn, { elevation: 0 }]}>
                        <Text style={{ fontSize: FontSize('large'), color: '#0854b4', fontFamily: Fonts.MulishMedium }}>Login</Text>
                    </Button>
                </View>
            </Container>
        );
    }
}

export default WelcomeScreen;

const styles = StyleSheet.create({
    mainContent: {
        flex: 1,
        backgroundColor: 'transparent',
        width: '100%',
    },
    mainRow: {
        paddingVertical: hp(1),
        width: wp(100),
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    title: {
        textAlign: 'center',
        fontSize: FontSize('x4Large'),
        fontFamily: Fonts.MulishMedium,
        color: '#323232',
    },
    text: {
        textAlign: 'center',
        fontSize: 18,
        fontFamily: Fonts.MulishLight,
        lineHeight: 30,
    },
    buttonText: {
        backgroundColor: 'transparent',
        color: '#686868',
        fontSize: FontSize("small"),
        paddingVertical: 15,
        paddingHorizontal: 10,
        fontFamily: Fonts.MulishLight,
    },
    activeDot: {
        width: 12, height: 12,
        borderRadius: 10,
        backgroundColor: '#323232',
    },
    dot: {
        width: 12, height: 12,
        borderRadius: 10,
        backgroundColor: '#d0d0d0',
    },
    iconColor: {
        color: '#686868',
    },
    BottomBtn: {
        height: hp(7),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        backgroundColor: 'transparent',
    }
})