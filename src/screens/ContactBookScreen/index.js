import React, { useLayoutEffect, useState } from 'react';
import { Image, StyleSheet, Text, View, TouchableOpacity, TextInput, useWindowDimensions } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import AlphabetList from 'react-native-flatlist-alphabet';
import AntDesign from 'react-native-vector-icons/AntDesign'

import FontSize from '../../utils/FontSize';
import { Fonts } from '../../utils/Fonts';
import { hp, wp } from '../../utils/utility';

const renderSectionHeader = () => null

const FirstRoute = () => {
    const [contactList, setContactList] = useState([
        { key: '00', userImage: require('../../assets/avatar.png'), value: 'Aames Brian', type: 'Realtor', added: '2/12/2021' },
        { key: '01', userImage: require('../../assets/avatar.png'), value: 'Aames Brian', type: 'Realtor', added: '2/12/2021' },
        { key: '02', userImage: require('../../assets/avatar.png'), value: 'Aames Brian', type: 'Realtor', added: '2/12/2021' },
        { key: '03', userImage: require('../../assets/avatar.png'), value: 'Aames Brian', type: 'Realtor', added: '2/12/2021' },
        { key: '04', userImage: require('../../assets/avatar.png'), value: 'Aames Brian', type: 'Realtor', added: '2/12/2021' },
        { key: '05', userImage: require('../../assets/avatar.png'), value: 'Bames Brian', type: 'Realtor', added: '2/12/2021' },
        { key: '06', userImage: require('../../assets/avatar.png'), value: 'Bames Brian', type: 'Realtor', added: '2/12/2021' },
        { key: '07', userImage: require('../../assets/avatar.png'), value: 'Bames Brian', type: 'Realtor', added: '2/12/2021' },
        { key: '08', userImage: require('../../assets/avatar.png'), value: 'Names Brian', type: 'Realtor', added: '2/12/2021' },
        { key: '09', userImage: require('../../assets/avatar.png'), value: 'Mames Brian', type: 'Realtor', added: '2/12/2021' },
        { key: '10', userImage: require('../../assets/avatar.png'), value: 'Cames Brian', type: 'Realtor', added: '2/12/2021' },
        { key: '11', userImage: require('../../assets/avatar.png'), value: 'Dames Brian', type: 'Realtor', added: '2/12/2021' },
        { key: '12', userImage: require('../../assets/avatar.png'), value: 'Eames Brian', type: 'Realtor', added: '2/12/2021' },
        { key: '13', userImage: require('../../assets/avatar.png'), value: 'Fames Brian', type: 'Realtor', added: '2/12/2021' },
        { key: '14', userImage: require('../../assets/avatar.png'), value: 'Games Brian', type: 'Realtor', added: '2/12/2021' },
        { key: '15', userImage: require('../../assets/avatar.png'), value: 'Hames Brian', type: 'Realtor', added: '2/12/2021' },
        { key: '16', userImage: require('../../assets/avatar.png'), value: 'Iames Srian', type: 'Realtor', added: '2/12/2021' },
        { key: '17', userImage: require('../../assets/avatar.png'), value: 'James Orian', type: 'Realtor', added: '2/12/2021' },
        { key: '18', userImage: require('../../assets/avatar.png'), value: 'Kames Orian', type: 'Realtor', added: '2/12/2021' },
        { key: '19', userImage: require('../../assets/avatar.png'), value: 'Lames Orian', type: 'Realtor', added: '2/12/2021' },
        { key: '20', userImage: require('../../assets/avatar.png'), value: 'Mames Orian', type: 'Realtor', added: '2/12/2021' },
        { key: '21', userImage: require('../../assets/avatar.png'), value: 'Names Orian', type: 'Realtor', added: '2/12/2021' },
        { key: '22', userImage: require('../../assets/avatar.png'), value: 'Oames Orian', type: 'Realtor', added: '2/12/2021' },
        { key: '23', userImage: require('../../assets/avatar.png'), value: 'Pames Orian', type: 'Realtor', added: '2/12/2021' },
        { key: '24', userImage: require('../../assets/avatar.png'), value: 'Qames Orian', type: 'Realtor', added: '2/12/2021' },
        { key: '25', userImage: require('../../assets/avatar.png'), value: 'Rames Orian', type: 'Realtor', added: '2/12/2021' },
        { key: '26', userImage: require('../../assets/avatar.png'), value: 'Sames Orian', type: 'Realtor', added: '2/12/2021' },
        { key: '27', userImage: require('../../assets/avatar.png'), value: 'Tames Orian', type: 'Realtor', added: '2/12/2021' },
        { key: '28', userImage: require('../../assets/avatar.png'), value: 'Uames Orian', type: 'Realtor', added: '2/12/2021' },
        { key: '29', userImage: require('../../assets/avatar.png'), value: 'Vames Orian', type: 'Realtor', added: '2/12/2021' },
        { key: '30', userImage: require('../../assets/avatar.png'), value: 'Wames Orian', type: 'Realtor', added: '2/12/2021' },
        { key: '31', userImage: require('../../assets/avatar.png'), value: 'Xames Orian', type: 'Realtor', added: '2/12/2021' },
        { key: '32', userImage: require('../../assets/avatar.png'), value: 'Yames Orian', type: 'Realtor', added: '2/12/2021' },
        { key: '33', userImage: require('../../assets/avatar.png'), value: 'Zames Orian', type: 'Realtor', added: '2/12/2021' }
    ])

    const renderListItem = (item) => {
        return (
            <TouchableOpacity style={styles.itemContainer}>
                <Image style={styles.imageSize} resizeMode="contain" source={item.userImage} />
                <View>
                    <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishBold, color: '#333333' }}>{item.value}</Text>
                    <Text style={{ fontSize: FontSize('small'), fontFamily: Fonts.MulishLight, color: '#333333' }}>{item.type}</Text>
                </View>
                <View style={{ alignItems: 'center' }}>
                    <Text style={{ fontSize: FontSize('small'), fontFamily: Fonts.MulishLight, color: '#333333' }}>Added</Text>
                    <Text style={{ fontSize: FontSize('small'), fontFamily: Fonts.MulishLight, color: '#333333' }}>{item.added}</Text>
                </View>
            </TouchableOpacity>
        );
    };

    return (
        <View style={{ paddingBottom: hp(4), paddingTop: hp(2), backgroundColor: '#ffffff', }}>
            <AlphabetList
                style={{ width: '100%' }}
                data={contactList}
                renderItem={renderListItem}
                renderSectionHeader={renderSectionHeader}
                letterItemStyle={{ height: hp(2.6) }}
                indexLetterColor={'#074caa'}
            />
        </View>
    )
}

const SecondRoute = () => {
    const [contactList, setContactList] = useState([
        { key: '00', value: 'Accountants', contacts: '10' },
        { key: '01', value: 'Banker', contacts: '10' },
        { key: '02', value: 'Barber', contacts: '10' },
        { key: '03', value: 'Bartender', contacts: '10' },
        { key: '04', value: 'Caterer', contacts: '10' },
        { key: '05', value: 'Consultant', contacts: '10' },
        { key: '06', value: 'Dentist', contacts: '10' },
        { key: '07', value: 'Dermotologist', contacts: '10' },
        { key: '08', value: 'Doctor', contacts: '10' },
    ])

    const renderListItem = (item) => {
        return (
            <TouchableOpacity style={{ height: hp(8), flexDirection: 'row', width: wp(100), alignItems: 'center', justifyContent: 'space-between', paddingLeft: wp(8), paddingRight: wp(12), borderBottomWidth: hp(0.1), borderBottomColor: '#e6e6e6' }}>
                <Text style={{ fontSize: 18, fontFamily: Fonts.MulishLight, color: '#333333' }}>{item.value}</Text>
                <Text style={{ fontSize: FontSize('small'), fontFamily: Fonts.MulishLight, color: '#333333' }}>{item.contacts + ' Contacts'}</Text>
            </TouchableOpacity>
        );
    };

    return (
        <View style={{ paddingBottom: hp(2), backgroundColor: '#ffffff', }}>
            <AlphabetList
                style={{ width: '100%' }}
                data={contactList}
                renderItem={renderListItem}
                renderSectionHeader={renderSectionHeader}
                letterItemStyle={{ height: hp(2.6) }}
                indexLetterColor={'#074caa'}
            />
        </View>
    )
}

const ThirdRoute = () => {
    const [contactList, setContactList] = useState([
        { key: '00', value: 'Coworkers', contacts: '10' },
        { key: '01', value: 'Agents', contacts: '10' },
        { key: '02', value: 'Client List', contacts: '10' },
        { key: '03', value: 'COntractors', contacts: '10' },
    ])

    const renderListItem = (item) => {
        return (
            <TouchableOpacity style={{ height: hp(8), flexDirection: 'row', width: wp(100), alignItems: 'center', justifyContent: 'space-between', paddingLeft: wp(8), paddingRight: wp(12), borderBottomWidth: hp(0.1), borderBottomColor: '#e6e6e6' }}>
                <Text style={{ fontSize: 18, fontFamily: Fonts.MulishLight, color: '#333333' }}>{item.value}</Text>
                <Text style={{ fontSize: FontSize('small'), fontFamily: Fonts.MulishLight, color: '#333333' }}>{item.contacts + ' Contacts'}</Text>
            </TouchableOpacity>
        );
    };

    return (
        <View style={{ paddingBottom: hp(2), backgroundColor: '#ffffff', }}>
            <AlphabetList
                style={{ width: '100%' }}
                data={contactList}
                renderItem={renderListItem}
                renderSectionHeader={renderSectionHeader}
                letterItemStyle={{ height: hp(2.6) }}
                indexLetterColor={'#074caa'}
            />
        </View>
    )
}

const ContactBookScreen = ({ navigation }) => {
    const [SearchName, setSearchName] = useState(undefined)

    useLayoutEffect(() => {
        navigation.setOptions({
            header: () => (
                <View>
                    <View style={{ flexDirection: 'row', width: wp(100), justifyContent: 'space-between', alignItems: 'center', height: hp(8), backgroundColor: '#fff' }}>
                        <TouchableOpacity onPress={() => navigation.goBack()} style={{ width: wp(12), alignItems: 'center', justifyContent: 'center' }}>
                            <Image style={{ width: wp(5), height: hp(2.5) }} resizeMode='contain' source={require('../../assets/icons/back-icon.png')} />
                        </TouchableOpacity>
                        <Text style={{ color: '#333333', fontSize: FontSize('medium'), fontFamily: Fonts.MulishBold }}>Contact Book</Text>
                        <View style={{ width: wp(12) }} />
                    </View>
                    <View style={{ paddingHorizontal: wp(10), paddingBottom: hp(1.5), width: '100%', backgroundColor: '#fff' }}>
                        <View style={{ backgroundColor: '#fff', borderRadius: 5, elevation: 15, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: wp(2) }}>
                            <TextInput
                                value={SearchName}
                                onChangeText={setSearchName}
                                placeholder="Search By Name"
                                placeholderTextColor="#333333"
                                style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, width: wp(68), color: '#333333' }}
                            />
                            <AntDesign name="search1" size={25} />
                        </View>
                    </View>
                </View>
            )
        });
    }, [navigation, SearchName]);

    const layout = useWindowDimensions();

    const [index, setIndex] = useState(0);
    const [routes] = useState([
        { key: 'first', title: 'Favorities' },
        { key: 'second', title: 'Occupations' },
        { key: 'third', title: 'Personal' },
    ]);

    const renderScene = SceneMap({
        first: FirstRoute,
        second: SecondRoute,
        third: ThirdRoute,
    });

    const renderTabBar = props => (
        <TabBar
            {...props}
            indicatorContainerStyle={{ marginLeft: wp(6) }}
            indicatorStyle={{ backgroundColor: '#074caa', height: 6, marginBottom: -6, width: wp(28), }}
            style={{ backgroundColor: '#fff', elevation: 0, alignContent: 'center', borderBottomWidth: hp(0.1), borderBottomColor: '#e6e6e6' }}
            tabStyle={{ width: wp(30), height: hp(6.5), }}
            renderLabel={({ route, focused, color }) => (
                <Text style={{ color, fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, textTransform: 'capitalize', marginTop: -1, marginLeft: wp(10), width: wp(26), textAlign: 'center' }}>
                    {route.title}
                </Text>
            )}
            activeColor={'#333333'}
            inactiveColor={'#074caa'}
        />
    );

    return (
        <TabView
            navigationState={{ index, routes }}
            style={{ justifyContent: 'center' }}
            sceneContainerStyle={{ flexDirection: 'row' }}
            renderTabBar={renderTabBar}
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={{ width: layout.width }}
        />
    );
}

export default ContactBookScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fafafa',
        justifyContent: 'space-between',
    },
    imageSize: {
        width: wp(14),
        height: hp(7),
        borderRadius: 50,
        resizeMode: 'cover',
        marginLeft: hp(2),
        marginRight: wp(1.5)
    },
    itemContainer: {
        height: hp(10),
        flexDirection: 'row',
        width: wp(100),
        alignItems: 'flex-end',
        justifyContent: 'space-between',
        paddingBottom: hp(2),
        paddingLeft: wp(3),
        paddingRight: wp(10),
        borderBottomWidth: hp(0.1), borderBottomColor: '#e6e6e6',
    },
    sectionHeaderContainer: {
        backgroundColor: '#f7f7f7',
        width: wp(92),
        paddingVertical: hp(0.5),
        paddingLeft: wp(5),
    },
    sectionHeaderLabel: {
        color: 'grey',

    },
})