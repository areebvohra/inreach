import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Fonts } from '../../utils/Fonts';
import FontSize from '../../utils/FontSize';
import { hp, wp } from '../../utils/utility';

const GetStartedScreen = ({ navigation }) => {

    return (
        <View style={styles.container}>
            <View style={{ alignItems: 'center', width: '100%' }}>
                <View style={{ alignItems: 'flex-end', width: '100%', paddingHorizontal: wp(10) }}>
                    <TouchableOpacity>
                        <Text style={{ fontFamily: Fonts.MulishRegular, fontSize: FontSize('large'), color: '#0a54b3' }}>Skip</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <Image style={{ width: wp(70), height: hp(35) }} resizeMode="contain" source={require('../../assets/get-started.png')} />
                </View>
                <View style={{ paddingHorizontal: wp(10) }}>
                    <Text style={{ fontSize: FontSize('x5Large'), textAlign: 'center', fontFamily: Fonts.MulishBold, color: '#333333' }}>You're All Set!</Text>
                    <Text style={{ fontSize: FontSize('x5Large'), textAlign: 'center', fontFamily: Fonts.MulishExtraBold, color: '#333333' }}>Get a head start...</Text>
                    <Text style={{ fontSize: FontSize('large'), textAlign: 'center', lineHeight: 30, fontFamily: Fonts.MulishLight, paddingTop: hp(4), color: '#333333' }}>Invite your friends and colleagues to join your network and get <Text style={{ fontWeight: 'bold' }}>1 referral recognition</Text> added to your profile for each person you send an invite to.</Text>
                </View>
            </View>
            <TouchableOpacity onPress={() => navigation.navigate('InviteContact')} style={styles.doneBtn}>
                <Text style={styles.doneBtnText}>Get Started</Text>
            </TouchableOpacity>
        </View>
    );
}

export default GetStartedScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        paddingTop: hp(3),
        backgroundColor: '#fff',
        justifyContent: 'space-between'
    },
    doneBtn: {
        width: wp(80),
        height: hp(6),
        backgroundColor: '#004bb3',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        marginBottom: hp(3)
    },
    doneBtnText: { color: '#ffffff', fontSize: 18, fontFamily: Fonts.MulishLight },
})