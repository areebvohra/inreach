import OfferPromotionScreen from "./OfferPromotionScreen";
import AboutOfferPromotion from "./AboutOfferPromotion";
import PostOfferPromotion1 from "./PostOfferPromotion1";
import PostOfferPromotion2 from "./PostOfferPromotion2";
import PostOfferPromotion3 from "./PostOfferPromotion3";

export { OfferPromotionScreen, AboutOfferPromotion, PostOfferPromotion1, PostOfferPromotion2, PostOfferPromotion3 };