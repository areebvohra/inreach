import React, { useEffect, useRef, useState } from 'react';
import { Text, View, Animated } from 'react-native';
import { Container, Button, Picker } from "native-base";

import OfferPromotionStyles from './OfferPromotionStyles';
import FontSize from '../../utils/FontSize';
import { Fonts } from '../../utils/Fonts';
import { hp, wp } from '../../utils/utility';

const PostOfferPromotion2 = ({ navigation }) => {
    const [offerPromotion, setOfferPromotion] = useState(undefined)
    const progressWidth = useRef(new Animated.Value(wp(33.33))).current;

    useEffect(() => {
        longWidth();
    }, [progressWidth]);

    const longWidth = () => {
        Animated.timing(progressWidth, {
            toValue: wp(66.66),
            duration: 600,
            useNativeDriver: false,
        }).start();
    };

    return (
        <Container>
            <View style={OfferPromotionStyles.container}>
                <View style={{ position: 'absolute', justifyContent: 'flex-start', width: wp(100), backgroundColor: 'lightgrey' }}>
                    <Animated.View style={{ height: hp(0.3), width: progressWidth, backgroundColor: '#004bb3' }} />
                </View>
                <View />
                <View style={OfferPromotionStyles.inputContiner}>
                    <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishBold, color: '#333333' }}>Write a brief description about this Ad...</Text>
                    <Picker
                        // note={false}
                        mode="dropdown"
                        placeholder="Select a promotion type"
                        placeholderStyle={{ color: "grey" }}
                        placeholderIconColor="grey"
                        style={{ width: undefined, paddingLeft: 0, marginLeft: 0 }}
                        selectedValue={offerPromotion}
                        onValueChange={(value) => setOfferPromotion(value)}
                    >
                        <Picker.Item label="promotion 1" value="key0" />
                        <Picker.Item label="promotion 2" value="key1" />
                    </Picker>
                </View>
                <View>
                    <Button onPress={() => navigation.navigate('postOffer3')} style={[OfferPromotionStyles.doneBtn, { backgroundColor: '#1156b4' }]}>
                        <Text style={{ color: '#ffffff', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishLight }}>Continue</Text>
                    </Button>
                </View>
            </View>
        </Container>
    );
}

export default PostOfferPromotion2;