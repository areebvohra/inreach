import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { Button } from 'native-base'

import OfferPromotionStyles from './OfferPromotionStyles';
import FontSize from '../../utils/FontSize';
import { Fonts } from '../../utils/Fonts';
import { hp, wp } from '../../utils/utility';

const AboutOfferPromotion = ({ navigation }) => (
    <View style={OfferPromotionStyles.container}>
        <View style={{ position: 'absolute', justifyContent: 'flex-start', width: wp(100), backgroundColor: 'lightgrey', height: hp(0.3), }} />
        <View style={{ alignItems: 'center', paddingHorizontal: wp(8), paddingTop: hp(4) }}>
            <Image style={{ width: wp(26), height: hp(13) }} resizeMode="contain" source={require('../../assets/icons/offer.png')} />
            <Text style={{ fontSize: 16, fontFamily: Fonts.MulishExtraBold, paddingTop: hp(3), color: '#333333' }}>What are Offers & Promotions...</Text>
            <Text style={{ fontSize: 16, fontFamily: Fonts.MulishRegular, lineHeight: 25, textAlign: 'left', paddingTop: hp(3), color: '#333333' }}>
                Offers & Promotions are a great way to let your network of contacts know about company Sales, Discounts, Realtor Open House Listings and Advertisments for your business.{'\n\n'}If you create a custom contact list (e.g. My Clents) you can choose to send your offers to your custom list of contacts.
            </Text>
        </View>
        <View>
            <Button onPress={() => navigation.navigate('postOffer1')} style={[OfferPromotionStyles.doneBtn, { backgroundColor: '#1156b4' }]}>
                <Text style={{ color: '#ffffff', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishLight }}>Get Started</Text>
            </Button>
        </View>
    </View>
);

export default AboutOfferPromotion;