import React, { useLayoutEffect, useState } from 'react';
import { Text, View, TouchableWithoutFeedback, Image, StyleSheet, TextInput, TouchableOpacity, Modal, Pressable, ScrollView } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { Button } from 'native-base'

import FontSize from '../../utils/FontSize';
import { Fonts } from '../../utils/Fonts';
import { hp, wp } from '../../utils/utility';
import OfferPromotionStyles from './OfferPromotionStyles';

const OfferPromotionScreen = ({ navigation }) => {
    const [searchOfferPromo, setOfferPromo] = useState(undefined)
    const [modalVisible, setModalVisible] = useState(false);
    const [filter, setFilter] = useState(0);
    const filterList = ['All Offers & Promotions', 'Real State Listings', 'Discount Offers', 'Advertisments']

    useLayoutEffect(() => {
        navigation.setOptions({
            header: () => (
                <View>
                    <View style={{ flexDirection: 'row', width: wp(100), justifyContent: 'space-between', alignItems: 'center', height: hp(8), backgroundColor: '#fff' }}>
                        <TouchableOpacity onPress={() => navigation.goBack()} style={{ width: wp(12), alignItems: 'center', justifyContent: 'center' }}>
                            <Image style={{ width: wp(5), height: hp(2.5) }} resizeMode='contain' source={require('../../assets/icons/back-icon.png')} />
                        </TouchableOpacity>
                        <Text style={{ color: '#333333', fontSize: FontSize('medium'), fontFamily: Fonts.MulishBold }}>Offers & Promotions</Text>
                        <View style={{ width: wp(12) }} />
                    </View>
                    <View style={{ paddingHorizontal: wp(10), paddingBottom: hp(1.5), width: '100%', backgroundColor: '#fff' }}>
                        <View style={{ backgroundColor: '#fff', borderRadius: 5, elevation: 15, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: wp(2) }}>
                            <TextInput
                                value={searchOfferPromo}
                                onChangeText={setOfferPromo}
                                placeholder="Search Offers & Promotions"
                                placeholderTextColor="#333333"
                                style={{ fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishRegular, width: wp(68), color: '#333333' }}
                            />
                            <AntDesign name="search1" size={25} />
                        </View>
                    </View>
                </View>
            )
        });
    }, [navigation, searchOfferPromo]);

    return (
        <View style={styles.container}>
            <View style={{ width: '100%', paddingVertical: hp(1), paddingLeft: wp(4), paddingRight: wp(2), backgroundColor: '#f2f2f2', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <Text style={{ color: '#333333', fontFamily: Fonts.MulishExtraBold, fontSize: 13 }}>{filterList[filter]}</Text>
                <TouchableWithoutFeedback onPress={() => setModalVisible(!modalVisible)}>
                    <Image style={{ width: hp(4), height: hp(2) }} resizeMode="contain" source={require('../../assets/icons/carot-down.png')} />
                </TouchableWithoutFeedback>
            </View>
            <ScrollView style={{ flex: 1 }} contentContainerStyle={{ paddingBottom: wp(24) }}>

                {/* Advertisment */}
                <View style={{ paddingBottom: hp(2.5) }}>
                    <Image style={{ width: wp(100), height: hp(24) }} resizeMode="contain" source={require('../../assets/advertisment.png')} />
                    <View style={{ paddingHorizontal: wp(2) }}>
                        <View style={{ flexDirection: 'row', alignItems: 'flex-end', top: hp(-2.5) }}>
                            <View style={{ borderWidth: 3, borderColor: '#fff', borderRadius: 50, backgroundColor: '#fff' }}>
                                <Image style={{ width: wp(20), height: hp(10) }} resizeMode="contain" source={require('../../assets/avatar-circle.png')} />
                            </View>
                            <View style={{ width: wp(76), paddingLeft: wp(2), paddingBottom: hp(0.5) }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: wp(70) }}>
                                    <Text style={{ color: '#333333', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishExtraBold }}>John Smith</Text>
                                    <Text style={{ color: '#717171', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishMedium }}>Today 2:00PM</Text>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: wp(70) }}>
                                    <Text style={{ color: '#333333', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishSemiBold }}>Beautician</Text>
                                    <TouchableOpacity>
                                        <Text style={{ color: '#717171', fontSize: 13, color: '#768ebb', fontFamily: Fonts.MulishMedium }}>Report this post</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        <View style={{ paddingHorizontal: wp(2) }}>
                            <Text style={{ color: '#333333', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishExtraBold }}>Advertisment:</Text>
                            <Text style={{ color: '#333333', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishMedium, paddingVertical: hp(1) }}>New Med spa open in the Los Angeles area, visit our website for more details</Text>
                        </View>
                        <View style={{ height: hp(2) }} />
                        <View style={{ alignSelf: 'flex-end', paddingRight: wp(4) }}>
                            <Button style={[OfferPromotionStyles.doneBtn, { borderColor: '#1156b4', borderWidth: 1, backgroundColor: '#fff', width: wp(40), height: hp(4.5), }]}>
                                <Text style={{ color: '#1156b4', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishLight }}>Learn More</Text>
                            </Button>
                            <Button style={[OfferPromotionStyles.doneBtn, { backgroundColor: '#1156b4', width: wp(40), height: hp(4.5), }]}>
                                <Text style={{ color: '#ffffff', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishLight }}>Message</Text>
                            </Button>
                        </View>
                    </View>
                    <View style={styles.referredStyle}>
                        <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, color: '#fff', paddingLeft: wp(1) }}>Advertisment</Text>
                    </View>
                </View>

                {/* Real Estate Listing */}
                <View style={{ paddingBottom: hp(2.5) }}>
                    <Image style={{ width: wp(100), height: hp(24) }} resizeMode="contain" source={require('../../assets/advertisment.png')} />
                    <View style={{ paddingHorizontal: wp(2) }}>
                        <View style={{ flexDirection: 'row', alignItems: 'flex-end', top: hp(-2.5) }}>
                            <View style={{ borderWidth: 3, borderColor: '#fff', borderRadius: 50, backgroundColor: '#fff' }}>
                                <Image style={{ width: wp(20), height: hp(10) }} resizeMode="contain" source={require('../../assets/avatar-circle.png')} />
                            </View>
                            <View style={{ width: wp(76), paddingLeft: wp(2), paddingBottom: hp(0.5) }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: wp(70) }}>
                                    <Text style={{ color: '#333333', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishExtraBold }}>John Smith</Text>
                                    <Text style={{ color: '#717171', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishMedium }}>Today 2:00PM</Text>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: wp(70) }}>
                                    <Text style={{ color: '#333333', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishSemiBold }}>Beautician</Text>
                                    <TouchableOpacity>
                                        <Text style={{ color: '#717171', fontSize: 13, color: '#768ebb', fontFamily: Fonts.MulishMedium }}>Report this post</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        <View style={{ paddingHorizontal: wp(2) }}>
                            <Text style={{ color: '#333333', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishExtraBold }}>Property Address:</Text>
                            <Text style={{ color: '#333333', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishMedium, paddingVertical: hp(1) }}>12345 Street Ave Los Angeles, CA 90012</Text>
                        </View>
                        <View style={{ height: hp(1.5) }} />
                        <View style={{ paddingHorizontal: wp(2) }}>
                            <Text style={{ color: '#333333', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishExtraBold }}>Listing Price:</Text>
                            <Text style={{ color: '#06447d', fontSize: FontSize('x2Large'), fontFamily: Fonts.MulishBold, paddingVertical: hp(1) }}>$1,237,999.00</Text>
                        </View>
                        <View style={{ height: hp(1.5) }} />
                        <View style={{ paddingHorizontal: wp(2) }}>
                            <Text style={{ color: '#333333', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishExtraBold }}>Description:</Text>
                            <Text style={{ color: '#333333', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishMedium, paddingVertical: hp(1) }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</Text>
                        </View>
                        <View style={{ alignSelf: 'flex-end', paddingRight: wp(4), position: 'absolute', bottom: hp(9) }}>
                            <Button style={[OfferPromotionStyles.doneBtn, { borderColor: '#1156b4', borderWidth: 1, backgroundColor: '#fff', width: wp(40), height: hp(4.5), }]}>
                                <Text style={{ color: '#1156b4', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishLight }}>Learn More</Text>
                            </Button>
                            <Button style={[OfferPromotionStyles.doneBtn, { backgroundColor: '#1156b4', width: wp(40), height: hp(4.5), }]}>
                                <Text style={{ color: '#ffffff', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishLight }}>Message</Text>
                            </Button>
                        </View>
                    </View>
                    <View style={styles.referredStyle}>
                        <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, color: '#fff', paddingLeft: wp(1) }}>Real Estate Listing</Text>
                    </View>
                </View>

                {/* Discount */}
                <View style={{ paddingBottom: hp(2.5) }}>
                    <Image style={{ width: wp(100), height: hp(24) }} resizeMode="contain" source={require('../../assets/advertisment.png')} />
                    <View style={{ paddingHorizontal: wp(2) }}>
                        <View style={{ flexDirection: 'row', alignItems: 'flex-end', top: hp(-2.5) }}>
                            <View style={{ borderWidth: 3, borderColor: '#fff', borderRadius: 50, backgroundColor: '#fff' }}>
                                <Image style={{ width: wp(20), height: hp(10) }} resizeMode="contain" source={require('../../assets/avatar-circle.png')} />
                            </View>
                            <View style={{ width: wp(76), paddingLeft: wp(2), paddingBottom: hp(0.5) }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: wp(70) }}>
                                    <Text style={{ color: '#333333', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishExtraBold }}>John Smith</Text>
                                    <Text style={{ color: '#717171', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishMedium }}>Today 2:00PM</Text>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: wp(70) }}>
                                    <Text style={{ color: '#333333', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishSemiBold }}>Beautician</Text>
                                    <TouchableOpacity>
                                        <Text style={{ color: '#717171', fontSize: 13, color: '#768ebb', fontFamily: Fonts.MulishMedium }}>Report this post</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        <View style={{ paddingHorizontal: wp(2) }}>
                            <Text style={{ color: '#333333', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishExtraBold }}>Promotion Offer:</Text>
                            <Text style={{ color: '#06447d', fontSize: FontSize('x2Large'), fontFamily: Fonts.MulishBold, paddingVertical: hp(1) }}>50% OFF</Text>
                        </View>
                        <View style={{ height: hp(1.5) }} />
                        <View style={{ paddingHorizontal: wp(2) }}>
                            <Text style={{ color: '#333333', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishExtraBold }}>Description:</Text>
                            <Text style={{ color: '#333333', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishMedium, paddingVertical: hp(1) }}>Call us today and use discont code “in Reach” to get 505 off your next visit.</Text>
                        </View>
                        <View style={{ alignSelf: 'flex-end', paddingRight: wp(4), position: 'absolute', bottom: hp(9) }}>
                            <Button style={[OfferPromotionStyles.doneBtn, { borderColor: '#1156b4', borderWidth: 1, backgroundColor: '#fff', width: wp(40), height: hp(4.5), }]}>
                                <Text style={{ color: '#1156b4', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishLight }}>Learn More</Text>
                            </Button>
                            <Button style={[OfferPromotionStyles.doneBtn, { backgroundColor: '#1156b4', width: wp(40), height: hp(4.5), }]}>
                                <Text style={{ color: '#ffffff', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishLight }}>Message</Text>
                            </Button>
                        </View>
                    </View>
                    <View style={styles.referredStyle}>
                        <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, color: '#fff', paddingLeft: wp(1) }}>Discount Promotions</Text>
                    </View>
                </View>

            </ScrollView>
            <View style={{ position: 'absolute', bottom: hp(2), right: wp(5) }}>
                <TouchableWithoutFeedback onPress={() => navigation.navigate('aboutOfferPromotion')}>
                    <Image style={{ width: wp(24), height: hp(12) }} resizeMode="contain" source={require('../../assets/icons/post-reachout.png')} />
                </TouchableWithoutFeedback>
            </View>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => setModalVisible(!modalVisible)}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text style={styles.modalText}>See only</Text>
                        <Pressable style={[styles.button]} onPress={() => { setModalVisible(!modalVisible); setFilter(1) }}>
                            <Text style={styles.textStyle}>Real State Listings</Text>
                        </Pressable>
                        <Pressable style={[styles.button]} onPress={() => { setModalVisible(!modalVisible); setFilter(2) }}>
                            <Text style={styles.textStyle}>Discount Offers</Text>
                        </Pressable>
                        <Pressable style={[styles.button]} onPress={() => { setModalVisible(!modalVisible); setFilter(3) }}>
                            <Text style={styles.textStyle}>Advertisments</Text>
                        </Pressable>
                        <Pressable style={[styles.button]} onPress={() => { setModalVisible(!modalVisible); setFilter(0) }}>
                            <Text style={styles.textStyle}>View All</Text>
                        </Pressable>
                    </View>
                </View>
            </Modal>
        </View >
    );
}

export default OfferPromotionScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fafafa'
    },
    contentContainer: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'grey'
    },
    centeredView: {
        flex: 1,
        justifyContent: "flex-end",
        alignItems: "center",
        backgroundColor: 'rgba(0,0,0,0.5)',
    },
    modalView: {
        backgroundColor: "white",
        width: wp(100),
        height: hp(40),
        paddingVertical: hp(4),
        justifyContent: 'space-between',
        alignItems: "center",
    },
    button: { padding: 10, },
    buttonClose: {
        backgroundColor: "#2196F3",
    },
    modalText: {
        color: "#333333",
        fontFamily: Fonts.MulishBold,
        fontSize: FontSize('large'),
    },
    textStyle: {
        color: "#333333",
        fontFamily: Fonts.MulishRegular,
        fontSize: 18,
        textAlign: "center"
    },
    referredStyle: {
        backgroundColor: '#000',
        flexDirection: 'row',
        alignItems: 'center',
        // width: wp(50),
        paddingHorizontal: wp(5),
        justifyContent: 'center',
        paddingVertical: hp(0.5),
        opacity: 0.8,
        position: 'absolute',
        right: 0,
        top: hp(1)
    }
})