import { StyleSheet } from "react-native";
import { Fonts } from "../../utils/Fonts";
import FontSize from "../../utils/FontSize";
import { hp, wp } from "../../utils/utility";

const OfferPromotionStyles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff',
        justifyContent: 'space-between',
        paddingBottom: hp(2)
    },
    inputContiner: {
        borderBottomColor: '#e6e6e6',
        borderBottomWidth: hp(0.1),
        width: wp(90)
    },
    doneBtn: {
        width: wp(80),
        height: hp(6),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        marginBottom: hp(1),
        backgroundColor: '#074caa'
    },
    doneBtnText: {
        color: '#ffffff',
        fontSize: 18,
        fontFamily: Fonts.MulishLight
    },
    inputStyle: {
        fontSize: FontSize('medium'),
        paddingLeft: 0,
        color: '#333333',
        fontFamily: Fonts.MulishLight
    },
})

export default OfferPromotionStyles;