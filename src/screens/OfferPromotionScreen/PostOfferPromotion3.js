import React, { useEffect, useRef, useState } from 'react';
import { Text, View, Animated, TextInput, Image } from 'react-native';
import { Container, Button, Picker } from "native-base";

import OfferPromotionStyles from './OfferPromotionStyles';
import FontSize from '../../utils/FontSize';
import { Fonts } from '../../utils/Fonts';
import { hp, wp } from '../../utils/utility';
import { TouchableOpacity } from 'react-native';

const PostOfferPromotion3 = ({ navigation }) => {
    const [offerPromotion, setOfferPromotion] = useState(undefined)
    const progressWidth = useRef(new Animated.Value(66.66)).current;

    useEffect(() => {
        longWidth();
    }, [progressWidth]);

    const longWidth = () => {
        Animated.timing(progressWidth, {
            toValue: wp(100),
            duration: 600,
            useNativeDriver: false,
        }).start();
    };

    return (
        <Container>
            <View style={[OfferPromotionStyles.container, { paddingVertical: hp(5) }]}>
                <View style={{ position: 'absolute', justifyContent: 'flex-start', width: wp(100), backgroundColor: 'lightgrey' }}>
                    <Animated.View style={{ height: hp(0.3), width: progressWidth, backgroundColor: '#004bb3' }} />
                </View>
                {/* <View /> */}
                <View style={[OfferPromotionStyles.inputContiner, { borderBottomWidth: 0, alignItems: 'center', }]}>
                    <Text style={{ fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishBold, paddingBottom: hp(4), color: '#333333' }}>Upload a cover photo for your advertisment...</Text>
                    <TouchableOpacity>
                        <Image style={{ width: wp(30), height: hp(15) }} resizeMode="contain" source={require('../../assets/icons/upload-ad.png')} />
                    </TouchableOpacity>
                </View>
                <View style={OfferPromotionStyles.inputContiner}>
                    <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishBold, color: '#333333' }}>Add a link to view this AD...</Text>
                    <TextInput
                        placeholder="Type your URL link here"
                        placeholderTextColor="grey"
                        style={OfferPromotionStyles.inputStyle}
                    />
                </View>
                <View style={[OfferPromotionStyles.inputContiner, { borderBottomWidth: 0, }]}>
                    <Text style={{ fontSize: FontSize('small'), fontFamily: Fonts.MulishBold, color: '#838383' }}>Your Link will be used to send users toy your page when they click Learn More on your post</Text>
                </View>
                <View style={OfferPromotionStyles.inputContiner}>
                    <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishBold, color: '#333333' }}>Select up to 3 Catagories to share this post with...</Text>
                    <Picker
                        note={false}
                        mode="dropdown"
                        placeholder="Select a promotion type"
                        placeholderStyle={{ color: "grey" }}
                        placeholderIconColor="grey"
                        style={{ width: undefined, paddingLeft: 0, marginLeft: 0, color: '#333333' }}
                        selectedValue={offerPromotion}
                        onValueChange={(value) => setOfferPromotion(value)}
                    >
                        <Picker.Item label="promotion 1" value="key0" />
                        <Picker.Item label="promotion 2" value="key1" />
                    </Picker>
                </View>
                <View>
                    <Button style={[OfferPromotionStyles.doneBtn, { backgroundColor: '#1156b4' }]}>
                        <Text style={{ color: '#ffffff', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishLight }}>Post</Text>
                    </Button>
                </View>
            </View>
        </Container>
    );
}

export default PostOfferPromotion3;