import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const CallLogScreen = ({ navigation }) => {

    return (
        <View style={styles.container}>
            <Text>CallLogScreen</Text>
        </View>
    );
}

export default CallLogScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff'
    }
})