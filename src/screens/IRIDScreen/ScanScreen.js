import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const ScanScreen = ({ navigation }) => {

    return (
        <View style={styles.container}>
            <Text>ScanScreen</Text>
        </View>
    );
}

export default ScanScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff'
    }
})