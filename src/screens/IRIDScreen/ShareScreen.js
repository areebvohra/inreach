import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';

import { hp, wp } from '../../utils/utility';
import FontSize from '../../utils/FontSize';
import { Fonts } from '../../utils/Fonts';

const ShareScreen = ({ navigation }) => {

    return (
        <View style={styles.container}>
            <View style={{ alignItems: 'center', paddingHorizontal: wp(8), paddingTop: hp(4), paddingBottom: hp(3) }}>
                <Image style={{ width: wp(25), height: hp(12.5) }} resizeMode="contain" source={require('../../assets/icons/qrcode.png')} />
                <Text style={{ fontSize: FontSize('small'), fontFamily: Fonts.MulishExtraBold, paddingTop: hp(2), color: '#333333' }}>Share your iRiD</Text>
                <Text style={{ fontSize: FontSize('small'), fontFamily: Fonts.MulishRegular, lineHeight: 25, textAlign: 'center', paddingTop: hp(3), color: '#333333' }}>Users can scan your iRiD with their phones camera and automatically add and save your contact on the in Reach app.</Text>
            </View>
            <QrSocialShare
                onPress={() => { }}
                image={require('../../assets/icons/facebook-small.png')}
                text="Share of Facebook"
                borderTopWidth={hp(0.1)}
            />
            <QrSocialShare
                onPress={() => { }}
                image={require('../../assets/icons/instagram-small.png')}
                text="Share of Instagram"
            />
            <QrSocialShare
                onPress={() => { }}
                image={require('../../assets/icons/linkedin-small.png')}
                text="Share of LinkedIn"
            />
            <QrSocialShare
                onPress={() => { }}
                image={require('../../assets/icons/twitter-small.png')}
                text="Share of Twitter"
            />
            <QrSocialShare
                onPress={() => { }}
                image={require('../../assets/icons/text-small.png')}
                text="Text"
            />
            <QrSocialShare
                onPress={() => { }}
                image={require('../../assets/icons/email-small.png')}
                text="Email"
            />
            <QrSocialShare
                onPress={() => { }}
                image={require('../../assets/icons/download-small.png')}
                text="Download"
            />
        </View>
    );
}

export default ShareScreen;

const QrSocialShare = ({ image, text, onPress, borderBottomWidth = hp(0.1), borderTopWidth }) => (
    <TouchableOpacity onPress={onPress} style={{ width: wp(100), borderTopWidth: borderTopWidth ? borderTopWidth : 0, borderBottomWidth: borderBottomWidth, borderBottomColor: '#e6e6e6', borderTopColor: '#e6e6e6', alignItems: 'center', paddingVertical: hp(1), flexDirection: 'row', paddingHorizontal: wp(20) }}>
        <View style={{ width: wp(14) }}>
            <Image style={{ width: wp(8), height: hp(4) }} resizeMode="contain" source={image} />
        </View>
        <View style={{ width: wp(40) }}>
            <Text style={{ fontSize: FontSize('small'), fontFamily: Fonts.MulishExtraBold, textAlign: 'center', color: '#333333' }}>{text}</Text>
        </View>
    </TouchableOpacity>
)


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff'
    }
})