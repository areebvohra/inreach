import React from 'react';
import { Image, StyleSheet, Text, View, TouchableWithoutFeedback, TouchableOpacity, TextInput, FlatList } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign'

import FontSize from '../../utils/FontSize';
import { Fonts } from '../../utils/Fonts';
import { hp, wp } from '../../utils/utility';

const ReachOutRequests = ({ navigation }) => {

    const renderItem = ({ item, index }) => (
        <View key={index} style={{ width: wp(100), paddingVertical: hp(2), backgroundColor: '#fff', paddingHorizontal: wp(3) }}>
            <TouchableOpacity style={{ position: 'absolute', right: wp(4), top: hp(2) }}>
                <Image style={{ width: wp(5), height: hp(2.5) }} resizeMode="contain" source={require('../../assets/icons/close.png')} />
            </TouchableOpacity>
            <View style={{ flexDirection: 'row' }}>
                <View style={{ width: wp(26), alignItems: 'center', paddingTop: hp(1) }}>
                    <Image style={{ width: wp(20), height: hp(10) }} resizeMode="contain" source={require('../../assets/avatar-reachout.png')} />
                    <Text style={{ fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishBold, paddingTop: hp(0.5), color: '#333333' }}>Lisa Roberts</Text>
                </View>
                <View style={{ width: wp(60), paddingVertical: hp(4) }}>
                    <Text style={{ textAlign: 'center', fontFamily: Fonts.MulishMedium, fontSize: FontSize('small'), color: '#333333' }}>Hi, I'm looking for a recommended</Text>
                    <Text style={{ textAlign: 'center', fontFamily: Fonts.MulishMedium, fontSize: FontSize('large'), fontWeight: 'bold', color: '#104ca7', paddingVertical: hp(1.5) }}>Dentist</Text>
                    <Text style={{ textAlign: 'center', fontFamily: Fonts.MulishMedium, fontSize: FontSize('small'), color: '#333333' }}>In the city of <Text style={{ fontFamily: Fonts.MulishExtraBold }}>Los Angeles</Text></Text>
                </View>
            </View>
            <View>
                <Text style={{ textAlign: 'center', fontFamily: Fonts.MulishMedium, fontSize: FontSize('small'), color: '#0057a8' }}>You have 3 possible matches. Send Referrals</Text>
            </View>
        </View>
    )

    return (
        <View style={styles.container}>
            <View style={{ flexDirection: 'row', width: wp(100), justifyContent: 'space-between', alignItems: 'center', height: hp(8), backgroundColor: '#fff' }}>
                <TouchableOpacity onPress={() => navigation.goBack()} style={{ width: wp(12), alignItems: 'center', justifyContent: 'center' }}>
                    <Image style={{ width: wp(5), height: hp(2.5) }} resizeMode='contain' source={require('../../assets/icons/back-icon.png')} />
                </TouchableOpacity>
                <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishExtraBold, color: '#333333' }}>Reach Out Requests</Text>
                <View style={{ width: wp(12) }} />
            </View>
            <View style={{ paddingHorizontal: wp(10), paddingBottom: hp(1.5), width: '100%', backgroundColor: '#fff' }}>
                <View style={{ backgroundColor: '#fff', borderRadius: 5, elevation: 15, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: wp(2) }}>
                    <TextInput
                        placeholder="Search Reach Outs"
                        placeholderTextColor="#000"
                        style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, width: wp(68), color: '#333333' }}
                    />
                    <AntDesign name="search1" size={25} />
                </View>
            </View>
            <FlatList
                data={[{}, {}, {}]}
                renderItem={renderItem}
                style={{ flex: 1, width: wp(100), backgroundColor: '#fafafa' }}
                contentContainerStyle={{ paddingVertical: hp(2) }}
                ItemSeparatorComponent={() => (<View style={{ height: hp(1.2) }} />)}
                keyExtractor={(item, index) => index.toString()}
            />

            <View style={{ position: 'absolute', bottom: hp(2), right: wp(5) }}>
                <TouchableWithoutFeedback onPress={() => navigation.navigate('aboutReachOut')}>
                    <Image style={{ width: wp(24), height: hp(12) }} resizeMode="contain" source={require('../../assets/icons/post-reachout.png')} />
                </TouchableWithoutFeedback>
            </View>
        </View>
    );
}

export default ReachOutRequests;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fafafa',
        justifyContent: 'space-between',
        // paddingBottom: hp(2)
    },
})