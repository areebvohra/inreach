import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { Button } from 'native-base'

import FontSize from '../../utils/FontSize';
import { Fonts } from '../../utils/Fonts';
import { hp, wp } from '../../utils/utility';

const AboutReachOut = ({ navigation }) => (
    <View style={styles.container}>
        <View style={{ alignItems: 'center', paddingHorizontal: wp(8), paddingTop: hp(2) }}>
            <Image style={{ width: wp(26), height: hp(13) }} resizeMode="contain" source={require('../../assets/icons/reach-out-2.png')} />
            <Text style={{ fontSize: 16, fontFamily: Fonts.MulishExtraBold, paddingTop: hp(3), color: '#333333' }}>What are Reach Out Requests...</Text>
            <Text style={{ fontSize: 16, fontFamily: Fonts.MulishRegular, lineHeight: 25, color: '#333333', textAlign: 'left', paddingTop: hp(3) }}>
                A Reachout Request is a great way to get referrals from people you know & trust for services you are looking for.{'\n\n'}Only your contacts that have the service professional you are looking for will see your post and can send you referrals from their trusted network of contacts..{'\n\n'}Each time a person is referred to another user, they earn referral recognition which gets displayed on his/her profile. This helps users buid trust and credibilty within the in Reach Community.
            </Text>
        </View>
        <View>
            <Button onPress={() => navigation.navigate('postReachOut1')} style={[styles.doneBtn, { backgroundColor: '#1156b4' }]}>
                <Text style={{ color: '#ffffff', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishLight }}>Get Started</Text>
            </Button>
        </View>
    </View>
);

export default AboutReachOut;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff',
        justifyContent: 'space-between',
        paddingBottom: hp(2)
    },
    doneBtn: {
        width: wp(80),
        height: hp(6),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        marginBottom: hp(1),
        backgroundColor: '#074caa'
    },
    doneBtnText: {
        color: '#ffffff',
        fontSize: 18,
        fontFamily: Fonts.MulishLight
    },
})