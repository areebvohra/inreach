import React, { useEffect, useRef, useState } from 'react';
import { Text, View, StyleSheet, Animated } from 'react-native';
import { Container, Button, Picker } from "native-base";

import FontSize from '../../utils/FontSize';
import { Fonts } from '../../utils/Fonts';
import { hp, wp } from '../../utils/utility';

const PostReachOut2 = ({ }) => {
    const [stateofCountry, setStateofCountry] = useState(null)
    const [city, setCity] = useState(null)
    const progressWidth = useRef(new Animated.Value(wp(50))).current;

    useEffect(() => {
        longWidth();
    }, [progressWidth]);

    const longWidth = () => {
        Animated.timing(progressWidth, {
            toValue: wp(100),
            duration: 600,
            useNativeDriver: false,
        }).start();
    };

    return (
        <Container>
            <View style={styles.container}>
                <View style={{ position: 'absolute', justifyContent: 'flex-start', width: wp(100), backgroundColor: 'lightgrey' }}>
                    <Animated.View style={{ height: hp(0.3), width: progressWidth, backgroundColor: '#004bb3' }} />
                </View>
                <View>
                    <View style={{ height: hp(20) }} />
                    <View style={styles.inputContiner}>
                        <Text style={{ fontSize: FontSize('small'), fontFamily: Fonts.MulishBold, color: '#333333' }}>What state are you looking from this professional...</Text>
                        <Picker
                            note={false}
                            mode="dropdown"
                            placeholder="Select your nearby state"
                            placeholderStyle={{ color: "grey" }}
                            placeholderIconColor="grey"
                            style={{ width: undefined, paddingLeft: 0, marginLeft: 0, color: '#333333' }}
                            selectedValue={stateofCountry}
                            onValueChange={(value) => setStateofCountry(value)}
                        >
                            <Picker.Item label="State 1" value="key0" />
                            <Picker.Item label="State 2" value="key1" />
                        </Picker>
                    </View>
                    <View style={{ height: hp(6) }} />
                    <View style={styles.inputContiner}>
                        <Text style={{ fontSize: FontSize('small'), fontFamily: Fonts.MulishBold, color: '#333333' }}>Narrow down your request by selecting a country...</Text>
                        <Picker
                            note={false}
                            mode="dropdown"
                            placeholder="Select a City"
                            placeholderStyle={{ color: "grey" }}
                            placeholderIconColor="grey"
                            style={{ width: undefined, paddingLeft: 0, marginLeft: 0 }}
                            selectedValue={city}
                            onValueChange={(value) => setCity(value)}
                        >
                            <Picker.Item label="City 1" value="key0" />
                            <Picker.Item label="City 2" value="key1" />
                        </Picker>
                    </View>
                </View>
                <View>
                    <Button style={[styles.doneBtn, { backgroundColor: '#1156b4' }]}>
                        <Text style={{ color: '#ffffff', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishLight }}>Post</Text>
                    </Button>
                </View>
            </View>
        </Container>
    );
}
export default PostReachOut2;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff',
        justifyContent: 'space-between',
        paddingBottom: hp(2)
    },
    inputContiner: {
        borderBottomColor: '#e6e6e6',
        borderBottomWidth: hp(0.1),
        width: wp(90)
    },
    doneBtn: {
        width: wp(80),
        height: hp(6),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        marginBottom: hp(1),
        backgroundColor: '#074caa'
    },
    doneBtnText: {
        color: '#ffffff',
        fontSize: 18,
        fontFamily: Fonts.MulishLight
    },
})