import React, { useState } from 'react';
import { Image, StyleSheet, Text, View, Switch, TouchableOpacity, ScrollView } from 'react-native';
import ToggleSwitch from 'toggle-switch-react-native'
import { Button } from 'native-base';

import { hp, wp } from '../../utils/utility';
import FontSize from '../../utils/FontSize';
import { Fonts } from '../../utils/Fonts';

const PrivacySettingsScreen = ({ navigation }) => {
    const [privacy, setPrivacy] = useState(false);
    const [data, setData] = useState(false);
    const [connectionRequest, setConnectionRequest] = useState(false);
    const [reachOutRequest, setReachOutRequest] = useState(false);
    const [offerPromotionRequest, setOfferPromotionRequest] = useState(false);

    return (
        <ScrollView style={{ flex: 1 }} contentContainerStyle={styles.container}>
            <View style={{ alignItems: 'center' }}>
                <View style={{ alignItems: 'center', paddingHorizontal: wp(10), paddingTop: hp(4), paddingBottom: hp(3), backgroundColor: '#fff', width: wp(100) }}>
                    {
                        privacy
                            ? <Image style={{ width: wp(24), height: hp(12) }} resizeMode="contain" source={require('../../assets/icons/unlock.png')} />
                            : <Image style={{ width: wp(24), height: hp(12) }} resizeMode="contain" source={require('../../assets/icons/lock.png')} />
                    }
                    <Text style={{ fontSize: 13, fontFamily: Fonts.MulishExtraBold, paddingTop: hp(2), color: '#333333' }}>Your Profile is set to {privacy ? 'Public' : 'Private'}</Text>
                    <Text style={{ fontSize: 13, fontFamily: Fonts.MulishRegular, lineHeight: 30, textAlign: 'center', paddingTop: hp(1), color: '#333333' }}>
                        {
                            privacy
                                ? 'Public profiles are great if you would like to allow users to quickly save your contact information. It’s ideal for tradeshows, open houses and public conventions.'
                                : 'Private Profiles require you to approve any connection requests. Users can still add your contacts through your iRiD without the need for you to approve it.'
                        }
                    </Text>
                </View>
                <View style={{ backgroundColor: '#fff', width: wp(100), borderTopColor: '#e6e6e6', borderBottomColor: '#e6e6e6', borderTopWidth: hp(0.1), borderBottomWidth: hp(0.1), alignItems: 'flex-end', paddingVertical: hp(1.8), flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: wp(15), alignItems: 'center', }}>
                    <Text style={{ fontSize: 13, fontFamily: Fonts.MulishExtraBold, color: '#333333' }}>Private</Text>
                    <ToggleSwitch
                        isOn={privacy}
                        onColor="#4cd964"
                        offColor="#767577"
                        size="medium"
                        thumbOnStyle={{ width: wp(5), height: hp(2.5), left: -5 }}
                        thumbOffStyle={{ width: wp(5), height: hp(2.5), left: -1 }}
                        trackOnStyle={{ transform: [{ scaleX: 1.4 }, { scaleY: 1.4 }], width: wp(10) }}
                        trackOffStyle={{ transform: [{ scaleX: 1.4 }, { scaleY: 1.4 }], width: wp(10) }}
                        onToggle={() => setPrivacy(previousState => !previousState)}
                    />
                    <Text style={{ fontSize: 13, fontFamily: Fonts.MulishExtraBold, color: '#333333' }}>Public</Text>
                </View>
                <View style={{ alignItems: 'center', paddingHorizontal: wp(8), paddingVertical: hp(1), width: wp(100) }}>
                    <Text style={{ fontSize: 13, fontFamily: Fonts.MulishExtraBold, paddingTop: hp(1), color: '#333333' }}>Data Settings</Text>
                    <Text style={{ fontSize: 13, fontFamily: Fonts.MulishRegular, lineHeight: 30, color: '#333333', textAlign: 'center', paddingTop: hp(1) }}>You can choose to save all your messages and data to your phone instead the In Reach Cloud. You won't be able to access your data from other devices if you choose to save to your phone.</Text>
                </View>
                <View style={{ backgroundColor: '#fff', width: wp(100), borderTopColor: '#e6e6e6', borderBottomColor: '#e6e6e6', borderTopWidth: hp(0.1), borderBottomWidth: hp(0.1), alignItems: 'flex-end', paddingVertical: hp(1.8), flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: wp(10), alignItems: 'center' }}>
                    <Text style={{ fontSize: 13, fontFamily: Fonts.MulishExtraBold, color: '#333333' }}>Save to Phone</Text>
                    <ToggleSwitch
                        isOn={data}
                        onColor="#4cd964"
                        offColor="#767577"
                        size="medium"
                        thumbOnStyle={{ width: wp(5), height: hp(2.5), left: -5 }}
                        thumbOffStyle={{ width: wp(5), height: hp(2.5), left: -1 }}
                        trackOnStyle={{ transform: [{ scaleX: 1.4 }, { scaleY: 1.4 }], width: wp(10) }}
                        trackOffStyle={{ transform: [{ scaleX: 1.4 }, { scaleY: 1.4 }], width: wp(10) }}
                        onToggle={() => setData(previousState => !previousState)}
                    />
                    <Text style={{ fontSize: 13, fontFamily: Fonts.MulishExtraBold, color: '#333333' }}>Save to Cloud</Text>
                </View>
                <View style={{ alignItems: 'center', paddingHorizontal: wp(8), paddingVertical: hp(1), width: wp(100) }}>
                    <Text style={{ fontSize: 13, fontFamily: Fonts.MulishExtraBold, paddingTop: hp(1), color: '#333333' }}>Notification Settings</Text>
                    <Text style={{ fontSize: 13, fontFamily: Fonts.MulishRegular, lineHeight: 30, color: '#333333', textAlign: 'center', paddingTop: hp(1) }}>Receive alert notifications for the following features.</Text>
                </View>
                <View style={{ backgroundColor: '#fff', width: wp(100), alignItems: 'flex-end', paddingVertical: hp(1.8), flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: wp(10), alignItems: 'center', borderTopColor: '#e6e6e6', borderTopWidth: hp(0.1) }}>
                    <Text style={{ fontSize: 13, fontFamily: Fonts.MulishExtraBold, color: '#333333' }}>Connection Requests</Text>
                    <ToggleSwitch
                        isOn={connectionRequest}
                        onColor="#4cd964"
                        offColor="#767577"
                        size="medium"
                        thumbOnStyle={{ width: wp(5), height: hp(2.5), left: -5 }}
                        thumbOffStyle={{ width: wp(5), height: hp(2.5), left: -1 }}
                        trackOnStyle={{ transform: [{ scaleX: 1.4 }, { scaleY: 1.4 }], width: wp(10) }}
                        trackOffStyle={{ transform: [{ scaleX: 1.4 }, { scaleY: 1.4 }], width: wp(10) }}
                        onToggle={() => setConnectionRequest(previousState => !previousState)}
                    />
                </View>
                <View style={{ width: wp(100), height: hp(0.8), borderTopColor: '#e6e6e6', borderBottomColor: '#e6e6e6', borderTopWidth: hp(0.1), borderBottomWidth: hp(0.1) }} />
                <View style={{ backgroundColor: '#fff', width: wp(100), alignItems: 'flex-end', paddingVertical: hp(1.8), flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: wp(10), alignItems: 'center' }}>
                    <Text style={{ fontSize: 13, fontFamily: Fonts.MulishExtraBold, color: '#333333' }}>Reach Out Requests</Text>
                    <ToggleSwitch
                        isOn={reachOutRequest}
                        onColor="#4cd964"
                        offColor="#767577"
                        size="medium"
                        thumbOnStyle={{ width: wp(5), height: hp(2.5), left: -5 }}
                        thumbOffStyle={{ width: wp(5), height: hp(2.5), left: -1 }}
                        trackOnStyle={{ transform: [{ scaleX: 1.4 }, { scaleY: 1.4 }], width: wp(10) }}
                        trackOffStyle={{ transform: [{ scaleX: 1.4 }, { scaleY: 1.4 }], width: wp(10) }}
                        onToggle={() => setReachOutRequest(previousState => !previousState)}
                    />
                </View>
                <View style={{ width: wp(100), height: hp(0.8), borderTopColor: '#e6e6e6', borderBottomColor: '#e6e6e6', borderTopWidth: hp(0.1), borderBottomWidth: hp(0.1) }} />
                <View style={{ backgroundColor: '#fff', width: wp(100), alignItems: 'flex-end', paddingVertical: hp(1.8), flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: wp(10), alignItems: 'center' }}>
                    <Text style={{ fontSize: 13, fontFamily: Fonts.MulishExtraBold, color: '#333333' }}>Offers & Promotions</Text>
                    <ToggleSwitch
                        isOn={offerPromotionRequest}
                        onColor="#4cd964"
                        offColor="#767577"
                        size="medium"
                        thumbOnStyle={{ width: wp(5), height: hp(2.5), left: -5 }}
                        thumbOffStyle={{ width: wp(5), height: hp(2.5), left: -1 }}
                        trackOnStyle={{ transform: [{ scaleX: 1.4 }, { scaleY: 1.4 }], width: wp(10) }}
                        trackOffStyle={{ transform: [{ scaleX: 1.4 }, { scaleY: 1.4 }], width: wp(10) }}
                        onToggle={() => setOfferPromotionRequest(previousState => !previousState)}
                    />
                </View>
                <View style={{ width: wp(100), height: hp(4), borderTopColor: '#e6e6e6', borderTopWidth: hp(0.1) }} />
            </View>
            <View>
                <Button style={[styles.doneBtn, { backgroundColor: '#1156b4' }]}>
                    <Text style={styles.doneBtnText}>Save Changes</Text>
                </Button>
            </View>
        </ScrollView>
    );
}

export default PrivacySettingsScreen;

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#fafafa',
        justifyContent: 'space-between',
        paddingBottom: hp(2)
    },
    doneBtn: {
        width: wp(80),
        height: hp(6),
        backgroundColor: '#004bb3',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        marginBottom: hp(3)
    },
    doneBtnText: { color: '#ffffff', fontSize: 18, fontFamily: Fonts.MulishLight },
})