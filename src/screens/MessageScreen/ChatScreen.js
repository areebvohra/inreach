import React, { useLayoutEffect, useState } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View, Modal, Pressable } from 'react-native';

import FontSize from '../../utils/FontSize';
import { Fonts } from '../../utils/Fonts';
import { hp, wp } from '../../utils/utility';

const ChatScreen = ({ route, navigation }) => {
    const [attachmentVisible, setAttachmentVisible] = useState(false);
    const [bookmarkVisible, setBookmarkVisible] = useState(false);
    const [attachment, setAttachment] = useState('');

    const { data } = route.params;
    const { messageName } = data;

    useLayoutEffect(() => {
        navigation.setOptions({
            headerTitle: messageName,
            headerRight: () => (
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: wp(16), marginRight: wp(5) }}>
                    <TouchableOpacity>
                        <Image style={{ width: wp(6), height: hp(3) }} resizeMode="contain" source={require('../../assets/icons/search.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => setBookmarkVisible(!bookmarkVisible)}>
                        <Image style={{ width: wp(6), height: hp(3) }} resizeMode="contain" source={require('../../assets/icons/bookmark.png')} />
                    </TouchableOpacity>
                </View>
            )
        });
    }, [navigation, bookmarkVisible]);

    return (
        <View style={styles.container}>
            <Text>ChatScreen</Text>

            <Modal
                animationType="slide"
                transparent={true}
                visible={attachmentVisible}
                onRequestClose={() => setAttachmentVisible(!attachmentVisible)}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Pressable style={[styles.button, { alignItems: 'center' }]} onPress={() => { setAttachmentVisible(!attachmentVisible) }}>
                            <Image style={{ width: wp(6), height: hp(3) }} resizeMode="contain" source={require('../../assets/icons/phone-2.png')} />
                            <Text style={styles.textStyle}>Call</Text>
                        </Pressable>
                        <Pressable style={[styles.button, { alignItems: 'center' }]} onPress={() => { setAttachmentVisible(!attachmentVisible) }}>
                            <Image style={{ width: wp(6), height: hp(3) }} resizeMode="contain" source={require('../../assets/icons/facetime.png')} />
                            <Text style={styles.textStyle}>Facetime</Text>
                        </Pressable>
                        <Pressable style={[styles.button, { alignItems: 'center' }]} onPress={() => { setAttachmentVisible(!attachmentVisible) }}>
                            <Image style={{ width: wp(6), height: hp(3) }} resizeMode="contain" source={require('../../assets/icons/media.png')} />
                            <Text style={styles.textStyle}>View Media</Text>
                        </Pressable>
                        <Pressable style={[styles.button, { alignItems: 'center' }]} onPress={() => { setAttachmentVisible(!attachmentVisible) }}>
                            <Image style={{ width: wp(6), height: hp(3) }} resizeMode="contain" source={require('../../assets/icons/view-contact.png')} />
                            <Text style={styles.textStyle}>View Contact</Text>
                        </Pressable>
                    </View>
                </View>
            </Modal>
            <Modal
                animationType="slide"
                transparent={true}
                visible={bookmarkVisible}
                onRequestClose={() => setBookmarkVisible(!bookmarkVisible)}
            >
                <View style={styles.centeredView}>
                    <View style={[styles.modalView, { justifyContent: 'flex-start', }]}>
                        <View style={[styles.button, { alignItems: 'center', justifyContent: 'center', height: hp(6) }]}>
                            <Image style={{ width: wp(6), height: hp(3) }} resizeMode="contain" source={require('../../assets/icons/bookmark.png')} />
                            <Text style={[styles.textStyle, { fontSize: FontSize('medium'), }]}>Bookmark Conversation</Text>
                        </View>
                        <View style={{ width: wp(100), height: hp(1), borderBottomWidth: hp(0.1), borderBottomColor: '#bdbdbd' }} />
                        <Pressable style={[styles.button, { alignItems: 'center', justifyContent: 'center', height: hp(6) }]} onPress={() => { setBookmarkVisible(!bookmarkVisible) }}>
                            <Image style={{ width: wp(6), height: hp(3) }} resizeMode="contain" source={require('../../assets/icons/reply-to.png')} />
                            <Text style={styles.textStyle}>Reply to</Text>
                        </Pressable>
                        <View style={{ width: wp(100), height: hp(1), borderBottomWidth: hp(0.1), borderBottomColor: '#bdbdbd' }} />
                        <Pressable style={[styles.button, { alignItems: 'center', justifyContent: 'center', height: hp(6) }]} onPress={() => { setBookmarkVisible(!bookmarkVisible) }}>
                            <Image style={{ width: wp(6), height: hp(3) }} resizeMode="contain" source={require('../../assets/icons/close-circle.png')} />
                            <Text style={[styles.textStyle, { color: '#de0808' }]}>Delete</Text>
                        </Pressable>
                    </View>
                </View>
            </Modal>
        </View>
    );
}

export default ChatScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    centeredView: {
        flex: 1,
        justifyContent: "flex-end",
        alignItems: "center",
        backgroundColor: 'rgba(0,0,0,0.5)',
    },
    modalView: {
        backgroundColor: "white",
        width: wp(100),
        height: hp(30),
        paddingVertical: hp(4),
        justifyContent: 'space-between',
        alignItems: "center",
    },
    button: { width: wp(70), flexDirection: 'row', },
    textStyle: {
        color: "#333333",
        fontFamily: Fonts.MulishRegular,
        fontSize: 18,
        textAlign: "left",
        marginLeft: wp(4)
    },
})