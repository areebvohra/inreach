import React, { useState } from 'react';
import { Text, View, Image, StyleSheet, TouchableOpacity, TouchableWithoutFeedback, FlatList, LayoutAnimation, Platform, UIManager, Modal, } from 'react-native';
import { Button } from 'native-base'
import Animated from 'react-native-reanimated';
import SwipeableItem, { UnderlayParams } from 'react-native-swipeable-item';
import { CodeField, Cursor, useBlurOnFulfill, useClearByFocusCell } from 'react-native-confirmation-code-field';

import FontSize from '../../utils/FontSize';
import { Fonts } from '../../utils/Fonts';
import { hp, wp } from '../../utils/utility';

if (Platform.OS === 'android') {
    UIManager.setLayoutAnimationEnabledExperimental &&
        UIManager.setLayoutAnimationEnabledExperimental(true);
}

type Item = {
    key: string;
    text: string;
    imagePath: string;
    messageName: string;
    lock: boolean;
    time: string;
    sent: boolean;
    received: boolean;
    read: boolean;
};

const MessageScreen = ({ navigation }) => {
    const [oldNew, setOldNew] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);
    const [value, setValue] = useState('');
    const ref = useBlurOnFulfill({ value, cellCount: 4 });
    const [props, getCellOnLayoutHandler] = useClearByFocusCell({ value, setValue });
    const [pinCode, setPinCode] = useState({ key: '0', lock: false })
    const [data, setData] = useState(
        [
            { key: "0", text: "Row 0", imagePath: '../../assets/avatar-circle.png', messageName: 'John Smith', time: 'Today 5:30PM', lock: true, sent: true, received: false, read: false },
            { key: "1", text: "Row 1", imagePath: '../../assets/avatar-circle.png', messageName: 'John Dmith', time: 'Today 5:30PM', lock: false, sent: true, received: true, read: false },
            { key: "2", text: "Row 2", imagePath: '../../assets/avatar-circle.png', messageName: 'John Gmith', time: 'Today 5:30PM', lock: false, sent: true, received: true, read: true },
            { key: "3", text: "Row 3", imagePath: '../../assets/avatar-circle.png', messageName: 'John Hmith', time: 'Today 5:30PM', lock: false, sent: true, received: false, read: false },
            { key: "4", text: "Row 4", imagePath: '../../assets/avatar-circle.png', messageName: 'John Wmith', time: 'Today 5:30PM', lock: false, sent: true, received: false, read: false },
            { key: "5", text: "Row 4", imagePath: '../../assets/avatar-circle.png', messageName: 'John Qmith', time: 'Today 5:30PM', lock: false, sent: true, received: false, read: false }
        ]
    )

    const itemRefs = new Map();

    const deleteItem = (item: Item) => {
        const updatedData = data.filter((d) => d !== item);
        // Animate list to close gap when item is deleted
        LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
        setData(updatedData)
    };

    const renderUnderlayLeft = ({ item, percentOpen }: UnderlayParams<Item>) => (
        // Fade in on open
        <Animated.View style={[styles.row, styles.underlayLeft, { opacity: percentOpen }]}>
            <TouchableOpacity style={{ alignItems: 'center' }}>
                <Image style={{ width: wp(6), height: hp(3), }} resizeMode="contain" source={require('../../assets/icons/favorities.png')} />
                <Text style={{ fontFamily: Fonts.MulishLight, fontSize: FontSize('xMini'), color: '#434343' }}>Remove Favorties</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => setModalVisible(true)} style={{ alignItems: 'center' }}>
                <Image style={{ width: wp(7), height: hp(3), }} resizeMode="contain" source={require('../../assets/icons/unlock-2.png')} />
                <Text style={{ fontFamily: Fonts.MulishLight, fontSize: FontSize('xMini'), color: '#434343' }}>{item.lock ? 'Unlock' : 'Lock'}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPressOut={() => deleteItem(item)} style={{ alignItems: 'center' }}>
                <Image style={{ width: wp(6), height: hp(3), }} resizeMode="contain" source={require('../../assets/icons/delete.png')} />
                <Text style={{ fontFamily: Fonts.MulishLight, fontSize: FontSize('xMini'), color: '#bd0606' }}>Delete</Text>
            </TouchableOpacity>
        </Animated.View>
    );

    const openChat = (item) => {
        navigation.navigate('chat', { data: item })
    }

    const renderItem = ({ item }) => {
        return (
            <SwipeableItem
                key={item.key}
                item={item}
                ref={(ref) => { if (ref && !itemRefs.get(item.key)) itemRefs.set(item.key, ref) }}
                onChange={({ open }) => {
                    if (open) {
                        // Close all other open items
                        [...itemRefs.entries()].forEach(([key, ref]) => {
                            if (key !== item.key && ref) ref.close();
                        });
                    }
                }}
                overSwipe={20}
                renderUnderlayLeft={renderUnderlayLeft}
                snapPointsLeft={[270]}
            >
                <TouchableOpacity onPress={() => openChat(item)} style={styles.row}>
                    <Image style={{ width: wp(18), height: hp(9) }} resizeMode="contain" source={require('../../assets/avatar-circle.png')} />
                    <View>
                        <Text style={{ fontSize: FontSize('large'), fontFamily: Fonts.MulishRegular, color: '#333333' }}>{item.messageName}</Text>
                        <Text style={{ fontSize: 13, fontFamily: Fonts.MulishRegular, color: '#333333' }}>Lorem ipsum dolor sit amet...</Text>
                    </View>
                    <View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                            <Text style={{ fontSize: FontSize('small'), fontFamily: Fonts.MulishRegular, color: '#848484' }}>Today 5:30PM </Text>
                            <Image style={{ width: wp(4), height: hp(2), tintColor: '#b1b1b1' }} resizeMode="contain" source={require('../../assets/icons/arrow-right.png')} />
                        </View>
                        <Text></Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end', position: 'absolute', bottom: hp(1), right: wp(5) }}>
                        {
                            item.received
                                ? <Image style={{ width: wp(4), height: hp(2), tintColor: item.read ? '#074caa' : '#817f7f' }} resizeMode="contain" source={require('../../assets/icons/tick.png')} />
                                : null
                        }
                        {
                            item.sent
                                ? <Image style={{ width: wp(4), height: hp(2), tintColor: item.read ? '#074caa' : '#817f7f' }} resizeMode="contain" source={require('../../assets/icons/tick.png')} />
                                : null
                        }
                    </View>
                </TouchableOpacity>
            </SwipeableItem>
        );
    };

    return (
        <View style={{ flex: 1, backgroundColor: '#fafafa' }}>
            <View style={{ width: '100%', paddingVertical: hp(2), paddingLeft: wp(4), paddingRight: wp(2), backgroundColor: '#f3f3f3', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <Text style={{ fontFamily: Fonts.MulishExtraBold, fontSize: 13, color: '#333333' }}>
                    {oldNew ? 'All Connections' : 'Favorites'}
                </Text>
                {
                    oldNew
                        ? <TouchableWithoutFeedback onPress={() => setOldNew(!oldNew)}>
                            <Image style={{ width: hp(4.5), height: hp(2.5) }} resizeMode="contain" source={require('../../assets/icons/carot-down.png')} />
                        </TouchableWithoutFeedback>
                        : <TouchableWithoutFeedback onPress={() => setOldNew(!oldNew)}>
                            <Image style={{ width: hp(4.5), height: hp(2.5) }} resizeMode="contain" source={require('../../assets/icons/carot-up.png')} />
                        </TouchableWithoutFeedback>
                }
            </View>
            <View>
                <FlatList
                    keyExtractor={(item) => item.key}
                    contentContainerStyle={{ paddingBottom: hp(20) }}
                    ItemSeparatorComponent={() => (<View style={{ width: wp(90), height: hp(0.1), marginHorizontal: wp(5), borderBottomColor: '#aaaaaa', borderBottomWidth: hp(0.1) }} />)}
                    data={data}
                    renderItem={renderItem}
                />
            </View>
            <View style={{ position: 'absolute', bottom: hp(2), right: wp(5) }}>
                <TouchableWithoutFeedback>
                    <Image style={{ width: wp(24), height: hp(12) }} resizeMode="contain" source={require('../../assets/icons/edit-blue.png')} />
                </TouchableWithoutFeedback>
            </View>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => setModalVisible(!modalVisible)}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <View style={{ alignItems: "center", }}>
                            <Text style={styles.modalText}>{pinCode.lock ? 'Unlock Conversation' : 'This Chat is Locked'}</Text>
                            <Text style={{ color: "#333333", fontSize: FontSize('small'), fontFamily: Fonts.MulishLight, paddingTop: hp(1) }}>Type in your 4 digit pin</Text>
                            <CodeField
                                ref={ref}
                                {...props}
                                value={value}
                                onChangeText={setValue}
                                cellCount={4}
                                rootStyle={styles.codeFieldRoot}
                                keyboardType="number-pad"
                                textContentType="oneTimeCode"
                                renderCell={({ index, symbol, isFocused }) => (
                                    <View
                                        // Make sure that you pass onLayout={getCellOnLayoutHandler(index)} prop to root component of "Cell"
                                        onLayout={getCellOnLayoutHandler(index)}
                                        key={index}
                                        style={[styles.cellRoot, isFocused && styles.focusCell]}>
                                        <Text style={styles.cellText}>
                                            {symbol || (isFocused ? <Cursor /> : null)}
                                        </Text>
                                    </View>
                                )}
                            />
                        </View>
                        <View style={{ paddingTop: hp(2) }}>
                            <Button style={[styles.btnStyle, { backgroundColor: '#1156b4', marginBottom: hp(0), width: wp(70), height: hp(7), }]}>
                                <Text style={{ color: '#ffffff', fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishLight }}>{pinCode.lock ? 'Unlock' : 'Submit'}</Text>
                            </Button>
                        </View>
                    </View>
                </View>
            </Modal>
        </View>
    )
}

export default MessageScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    itemContainer: {
        width: wp(44),
        alignItems: 'center',
        borderRadius: 5,
        borderWidth: 0,
        backgroundColor: '#fff',
        elevation: 8
    },
    itemImageStyle: {
        width: wp(44),
        height: hp(20),
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5
    },
    btnStyle: {
        width: wp(36),
        height: hp(4.5),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        marginBottom: hp(1)
    },
    badgeStyle: {
        width: wp(6),
        height: hp(3.1),
        backgroundColor: '#0c51af',
        borderRadius: 15
    },
    referredStyle: {
        backgroundColor: '#000',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: wp(1.5),
        paddingVertical: hp(0.8),
        borderRadius: 5, opacity: 0.8,
        position: 'absolute',
        left: 3,
        top: 4
    },
    searchTextInput: {
        fontSize: FontSize('xMedium'),
        fontFamily: Fonts.MulishRegular,
        width: wp(70),
        color: '#333333'
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: hp(14),
        backgroundColor: '#fff',
        padding: 15,
    },
    text: {
        fontWeight: 'bold',
        color: 'black',
        fontSize: 32,
    },
    underlayLeft: {
        flex: 1,
        backgroundColor: '#f0f0f0',
        alignSelf: 'flex-end',
        width: wp(65),
    },
    button: { padding: 10, },
    centeredView: {
        flex: 1,
        justifyContent: "flex-end",
        alignItems: "center",
        backgroundColor: 'rgba(0,0,0,0.5)',
    },
    modalView: {
        backgroundColor: "white",
        width: wp(100),
        height: hp(40),
        paddingVertical: hp(4),
        justifyContent: 'space-between',
        alignItems: "center",
    },
    modalText: {
        color: "#333333",
        fontFamily: Fonts.MulishBold,
        fontSize: FontSize('large'),
    },
    textStyle: {
        color: "#333333",
        fontFamily: Fonts.MulishRegular,
        fontSize: 18,
        textAlign: "center"
    },
    codeFieldRoot: {
        marginTop: hp(1),
        width: wp(70),
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    cellRoot: {
        width: wp(14),
        height: hp(8),
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: '#ccc',
        borderBottomWidth: 1,
    },
    cellText: {
        color: '#000',
        fontSize: FontSize('x4Large'),
        textAlign: 'center',
    },
    focusCell: {
        borderBottomColor: '#007AFF',
        borderBottomWidth: 2,
    },
})