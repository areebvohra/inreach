import React from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native';

import { Fonts } from '../../utils/Fonts';
import FontSize from '../../utils/FontSize';
import { hp, wp } from '../../utils/utility';

const SocialConnectScreen = ({ navigation }) => {

    return (
        <View style={styles.container}>
            <View style={{ alignItems: 'center', width: wp(80), paddingTop: hp(1) }}>
                <Text style={{ fontSize: FontSize('xMedium'), fontFamily: Fonts.MulishBold, textAlign: 'center', color: '#333333' }} >Optional</Text>
                <View style={styles.inputContiner}>
                    <TouchableOpacity style={styles.connectBtn}>
                        <Image style={styles.connectImage} resizeMode="contain" source={require('../../assets/facebook.png')} />
                        <Text style={styles.connectText}>Add your Facebook Profile</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.inputContiner}>
                    <TouchableOpacity style={styles.connectBtn}>
                        <Image style={styles.connectImage} resizeMode="contain" source={require('../../assets/linkedin.png')} />
                        <Text style={styles.connectText}>Add your Linkedin Profile</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.inputContiner}>
                    <TouchableOpacity style={styles.connectBtn}>
                        <Image style={styles.connectImage} resizeMode="contain" source={require('../../assets/instagram.png')} />
                        <Text style={styles.connectText}>Add your Instagram Profile</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.inputContiner}>
                    <TouchableOpacity style={styles.connectBtn}>
                        <Image style={styles.connectImage} resizeMode="contain" source={require('../../assets/twitter.png')} />
                        <Text style={styles.connectText}>Add your Twitter Profile</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.inputContiner}>
                    <TouchableOpacity style={styles.connectBtn}>
                        <Image style={styles.connectImage} resizeMode="contain" source={require('../../assets/youtube.png')} />
                        <Text style={styles.connectText}>Add your Youtude Profile</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.inputContiner}>
                    <TouchableOpacity style={styles.connectBtn}>
                        <Image style={styles.connectImage} resizeMode="contain" source={require('../../assets/tiktok.png')} />
                        <Text style={styles.connectText}>Add your Tiktok Profile</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.inputContiner}>
                    <TouchableOpacity style={styles.connectBtn}>
                        <Image style={styles.connectImage} resizeMode="contain" source={require('../../assets/snapchat.png')} />
                        <Text style={styles.connectText}>Add your Snapshat</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.inputContiner}>
                    <TouchableOpacity style={styles.connectBtn}>
                        <Image style={styles.connectImage} resizeMode="contain" source={require('../../assets/vimeo.png')} />
                        <Text style={styles.connectText}>Youtube or Vimeo Trailer URL</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <TouchableOpacity onPress={() => navigation.navigate('AcceptTermsCondition')} style={styles.doneBtn}>
                <Text style={styles.doneBtnText}>Done</Text>
            </TouchableOpacity>
        </View>
    );
}

export default SocialConnectScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: wp(10),
        paddingTop: hp(2),
        backgroundColor: "#fff",
        alignItems: 'center',
        backgroundColor: "#fff",
        justifyContent: 'space-between',
    },
    inputContiner: {
        borderBottomColor: '#e6e6e6',
        borderBottomWidth: hp(0.1),
        width: wp(80)
    },
    inputStyle: {
        fontSize: FontSize('medium'),
        paddingLeft: 0,
        color: '#000',
        fontFamily: Fonts.MulishLight
    },
    doneBtn: {
        width: wp(80),
        height: hp(6),
        backgroundColor: '#004bb3',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        marginBottom: hp(3)
    },
    doneBtnText: { color: '#ffffff', fontSize: 18, fontFamily: Fonts.MulishLight },
    connectBtn: { flexDirection: 'row', alignItems: 'center', paddingVertical: hp(1.8), },
    connectImage: { width: wp(10), height: hp(5) },
    connectText: { fontFamily: Fonts.MulishMedium, fontSize: FontSize('small'), paddingLeft: wp(3), color: '#333333' },
})