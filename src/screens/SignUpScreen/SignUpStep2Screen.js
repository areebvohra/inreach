import React, { useEffect, useRef, useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, Image, Animated } from 'react-native';
import { Container, Picker } from "native-base";

import { hp, wp } from '../../utils/utility';
import signupStyles from './signupStyle';
import FontSize from '../../utils/FontSize';
import { Fonts } from '../../utils/Fonts';

const SignUpStep2Screen = ({ navigation }) => {
    const [occupation, setOccupation] = useState(undefined)
    const progressWidth = useRef(new Animated.Value(wp(0))).current;

    useEffect(() => {
        longWidth();
    }, [progressWidth]);

    const longWidth = () => {
        Animated.timing(progressWidth, {
            toValue: wp(50),
            duration: 600,
            useNativeDriver: false,
        }).start();
    };

    return (
        <Container>
            <View style={signupStyles.container}>
                <View style={{ position: 'absolute', top: 10, justifyContent: 'flex-start', width: wp(100), backgroundColor: 'lightgrey' }}>
                    <Animated.View style={{ height: hp(0.3), width: progressWidth, backgroundColor: '#004bb3' }} />
                </View>
                <View style={{ alignItems: 'center', width: wp(80) }}>
                    <View style={{ height: hp(10) }} />
                    <View style={signupStyles.inputContiner}>
                        <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishBold, color: '#333333' }}>Occupation</Text>
                        <Picker
                            note={false}
                            mode="dropdown"
                            placeholder="Select your occupation"
                            placeholderStyle={{ color: "grey" }}
                            placeholderIconColor="grey"
                            style={{ width: undefined, paddingLeft: 0, marginLeft: 0 }}
                            selectedValue={occupation}
                            onValueChange={(value) => setOccupation(value)}
                        >
                            <Picker.Item label="occupation 1" value="key0" />
                            <Picker.Item label="occupation 2" value="key1" />
                        </Picker>
                    </View>
                    <View style={{ height: hp(8) }} />
                    <View style={signupStyles.inputContiner}>
                        <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishBold, color: '#333333' }}>Business Name</Text>
                        <TextInput
                            placeholder="Type the name of your company here"
                            placeholderTextColor="grey"
                            style={signupStyles.inputStyle}
                        />
                    </View>
                    <View style={{ height: hp(8) }} />
                    <View style={signupStyles.inputContiner}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishBold, color: '#333333' }}>Business Website or Online Store </Text>
                            <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishExtraLight, color: '#333333' }}>(optional)</Text>
                        </View>
                        <TextInput
                            placeholder="Type your company website here"
                            placeholderTextColor="grey"
                            style={signupStyles.inputStyle}
                        />
                    </View>
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('signUpStep3')} style={signupStyles.doneBtn}>
                    <Text style={signupStyles.doneBtnText}>Continue</Text>
                </TouchableOpacity>
            </View>
        </Container>
    );
}

export default SignUpStep2Screen;