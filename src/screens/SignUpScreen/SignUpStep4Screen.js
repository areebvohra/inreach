import React, { useEffect, useRef } from 'react';
import { View, Text, TouchableOpacity, Image, Animated } from 'react-native';

import { Fonts } from '../../utils/Fonts';
import FontSize from '../../utils/FontSize';
import { hp, wp } from '../../utils/utility';
import signupStyles from './signupStyle';

const SignUpStep4Screen = ({ navigation }) => {
    const progressWidth = useRef(new Animated.Value(wp(75))).current;

    useEffect(() => {
        longWidth();
    }, [progressWidth]);

    const longWidth = () => {
        Animated.timing(progressWidth, {
            toValue: wp(100),
            duration: 600,
            useNativeDriver: false,
        }).start();
    };

    return (
        <View style={signupStyles.container}>
            <View style={{ position: 'absolute', top: 10, justifyContent: 'flex-start', width: wp(100), backgroundColor: 'lightgrey' }}>
                <Animated.View style={{ height: hp(0.3), width: progressWidth, backgroundColor: '#004bb3' }} />
            </View>
            <View style={{ alignItems: 'center', width: wp(80), paddingTop: hp(1) }}>
                <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishBold, color: '#333333', textAlign: 'center' }} >Optional</Text>
                <View style={signupStyles.inputContiner}>
                    <TouchableOpacity style={signupStyles.connectBtn}>
                        <Image style={signupStyles.connectImage} resizeMode="contain" source={require('../../assets/facebook.png')} />
                        <Text style={signupStyles.connectText}>Add your Facebook Profile</Text>
                    </TouchableOpacity>
                </View>
                <View style={signupStyles.inputContiner}>
                    <TouchableOpacity style={signupStyles.connectBtn}>
                        <Image style={signupStyles.connectImage} resizeMode="contain" source={require('../../assets/linkedin.png')} />
                        <Text style={signupStyles.connectText}>Add your Linkedin Profile</Text>
                    </TouchableOpacity>
                </View>
                <View style={signupStyles.inputContiner}>
                    <TouchableOpacity style={signupStyles.connectBtn}>
                        <Image style={signupStyles.connectImage} resizeMode="contain" source={require('../../assets/instagram.png')} />
                        <Text style={signupStyles.connectText}>Add your Instagram Profile</Text>
                    </TouchableOpacity>
                </View>
                <View style={signupStyles.inputContiner}>
                    <TouchableOpacity style={signupStyles.connectBtn}>
                        <Image style={signupStyles.connectImage} resizeMode="contain" source={require('../../assets/twitter.png')} />
                        <Text style={signupStyles.connectText}>Add your Twitter Profile</Text>
                    </TouchableOpacity>
                </View>
                <View style={signupStyles.inputContiner}>
                    <TouchableOpacity style={signupStyles.connectBtn}>
                        <Image style={signupStyles.connectImage} resizeMode="contain" source={require('../../assets/youtube.png')} />
                        <Text style={signupStyles.connectText}>Add your Youtude Profile</Text>
                    </TouchableOpacity>
                </View>
                <View style={signupStyles.inputContiner}>
                    <TouchableOpacity style={signupStyles.connectBtn}>
                        <Image style={signupStyles.connectImage} resizeMode="contain" source={require('../../assets/tiktok.png')} />
                        <Text style={signupStyles.connectText}>Add your Tiktok Profile</Text>
                    </TouchableOpacity>
                </View>
                <View style={signupStyles.inputContiner}>
                    <TouchableOpacity style={signupStyles.connectBtn}>
                        <Image style={signupStyles.connectImage} resizeMode="contain" source={require('../../assets/snapchat.png')} />
                        <Text style={signupStyles.connectText}>Add your Snapshat</Text>
                    </TouchableOpacity>
                </View>
                <View style={signupStyles.inputContiner}>
                    <TouchableOpacity style={signupStyles.connectBtn}>
                        <Image style={signupStyles.connectImage} resizeMode="contain" source={require('../../assets/vimeo.png')} />
                        <Text style={signupStyles.connectText}>Youtube or Vimeo Trailer URL</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <TouchableOpacity onPress={() => navigation.navigate('AcceptTermsCondition')} style={signupStyles.doneBtn}>
                <Text style={signupStyles.doneBtnText}>Complete Profile</Text>
            </TouchableOpacity>
        </View>
    );
}

export default SignUpStep4Screen;