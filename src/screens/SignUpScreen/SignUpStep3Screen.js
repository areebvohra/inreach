import React, { useEffect, useRef, useState } from 'react';
import { View, Text, TouchableOpacity, Image, TextInput, Animated } from 'react-native';
import { Container, Picker } from "native-base";

import { Fonts } from '../../utils/Fonts';
import FontSize from '../../utils/FontSize';
import { hp, wp } from '../../utils/utility';
import signupStyles from './signupStyle';

const SignUpStep3Screen = ({ navigation }) => {
    const [stateofCountry, setStateofCountry] = useState(undefined)
    const [country, setCountry] = useState(undefined)
    const progressWidth = useRef(new Animated.Value(wp(50))).current;

    useEffect(() => {
        longWidth();
    }, [progressWidth]);

    const longWidth = () => {
        Animated.timing(progressWidth, {
            toValue: wp(75),
            duration: 600,
            useNativeDriver: false,
        }).start();
    };

    return (
        <Container>
            <View style={signupStyles.container}>
                <View style={{ position: 'absolute', top: 10, justifyContent: 'flex-start', width: wp(100), backgroundColor: 'lightgrey' }}>
                    <Animated.View style={{ height: hp(0.3), width: progressWidth, backgroundColor: '#004bb3' }} />
                </View>
                <View style={{ alignItems: 'center', width: wp(80) }}>
                    <View style={{ height: hp(5) }} />
                    <View style={signupStyles.inputContiner}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishBold, color: '#333333' }}>Address </Text>
                            <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishExtraLight, color: '#333333' }}>(optional)</Text>
                        </View>
                        <TextInput
                            placeholder="Type your business address here"
                            placeholderTextColor="grey"
                            style={signupStyles.inputStyle}
                        />
                    </View>
                    <View style={{ height: hp(5) }} />
                    <View style={signupStyles.inputContiner}>
                        <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishBold, color: '#333333' }}>State</Text>
                        <Picker
                            note={false}
                            mode="dropdown"
                            placeholder="Select your state"
                            placeholderStyle={{ color: "grey" }}
                            placeholderIconColor="grey"
                            style={{ width: undefined, paddingLeft: 0, marginLeft: 0 }}
                            selectedValue={stateofCountry}
                            onValueChange={(value) => setStateofCountry(value)}
                        >
                            <Picker.Item label="Taxes" value="key0" />
                            <Picker.Item label="Verginia" value="key1" />
                        </Picker>
                    </View>
                    <View style={{ height: hp(5) }} />
                    <View style={signupStyles.inputContiner}>
                        <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishBold, color: '#333333' }}>Zipcode</Text>
                        <TextInput
                            placeholder="Type your zipcode here"
                            placeholderTextColor="grey"
                            style={signupStyles.inputStyle}
                        />
                    </View>
                    <View style={{ height: hp(5) }} />
                    <View style={signupStyles.inputContiner}>
                        <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishBold, color: '#333333' }}>State</Text>
                        <Picker
                            note={false}
                            mode="dropdown"
                            placeholder="Select your country"
                            placeholderStyle={{ color: "grey" }}
                            placeholderIconColor="grey"
                            style={{ width: undefined, paddingLeft: 0, marginLeft: 0 }}
                            selectedValue={country}
                            onValueChange={(value) => setCountry(value)}
                        >
                            <Picker.Item label="USA" value="key0" />
                            <Picker.Item label="UK" value="key1" />
                        </Picker>
                    </View>
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('signUpStep4')} style={signupStyles.doneBtn}>
                    <Text style={signupStyles.doneBtnText}>Continue</Text>
                </TouchableOpacity>
            </View>
        </Container>
    );
}


export default SignUpStep3Screen;