import React, { useState } from 'react';
import { View, StyleSheet, Text, TextInput, TouchableOpacity, Image, } from 'react-native';

import FontSize from '../../utils/FontSize';
import { Fonts } from '../../utils/Fonts';
import { hp, wp } from '../../utils/utility';
import signupStyles from './signupStyle';

const SignUpStep1Screen = ({ navigation }) => {

    return (
        <View style={signupStyles.container}>
            <View style={{ position: 'absolute', top: 10, justifyContent: 'flex-start', width: wp(100), backgroundColor: 'lightgrey' }}>
                <View style={{ height: hp(0.3), width: wp(25), backgroundColor: '#004bb3', }} />
            </View>
            <View style={{ alignItems: 'center', width: wp(80) }}>
                <View style={{ height: hp(5) }} />
                <Text style={{ fontSize: FontSize('large'), fontFamily: Fonts.MulishBold, color: '#333333' }}>Upload a profile Photo...</Text>
                <View style={{ height: hp(5) }} />
                <TouchableOpacity>
                    <Image style={{ width: wp(36), height: hp(18) }} resizeMode="contain" source={require('../../assets/upload-image.png')} />
                </TouchableOpacity>
                <View style={{ height: hp(5) }} />
                <View style={signupStyles.inputContiner}>
                    <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishBold, color: '#333333' }}>Your Full Name</Text>
                    <TextInput
                        placeholder="Type your full name here"
                        placeholderTextColor="grey"
                        style={signupStyles.inputStyle}
                    />
                </View>
                <View style={{ height: hp(5) }} />
                <View style={signupStyles.inputContiner}>
                    <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishBold, color: '#333333' }}>Personal or Business Email</Text>
                    <TextInput
                        placeholder="Type your email here"
                        placeholderTextColor="grey"
                        style={signupStyles.inputStyle}
                    />
                </View>
            </View>
            <TouchableOpacity onPress={() => navigation.navigate('signUpStep2')} style={signupStyles.doneBtn}>
                <Text style={signupStyles.doneBtnText}>Continue</Text>
            </TouchableOpacity>
        </View>
    );
};

export default SignUpStep1Screen;