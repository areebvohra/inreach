import { StyleSheet } from "react-native";
import { Fonts } from "../../utils/Fonts";
import FontSize from "../../utils/FontSize";
import { hp, wp } from "../../utils/utility";

const signupStyles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: wp(6),
        paddingTop: hp(2),
        backgroundColor: "#fff",
        alignItems: 'center',
        backgroundColor: "#fff",
        justifyContent: 'space-between',
    },
    inputContiner: {
        borderBottomColor: '#e6e6e6',
        borderBottomWidth: hp(0.1),
        width: wp(90)
    },
    inputStyle: {
        fontSize: FontSize('medium'),
        paddingLeft: 0,
        color: '#333333',
        fontFamily: Fonts.MulishLight
    },
    doneBtn: {
        width: wp(80),
        height: hp(6),
        backgroundColor: '#004bb3',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        marginBottom: hp(3)
    },
    doneBtnText: { color: '#ffffff', fontSize: 18, fontFamily: Fonts.MulishLight },
    connectBtn: { flexDirection: 'row', alignItems: 'center', paddingVertical: hp(1.8), },
    connectImage: { width: wp(10), height: hp(5) },
    connectText: { fontFamily: Fonts.MulishMedium, fontSize: FontSize('small'), paddingLeft: wp(3), color: '#333333' },
})

export default signupStyles;