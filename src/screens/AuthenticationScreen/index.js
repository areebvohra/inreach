import React, { useLayoutEffect } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { CommonActions } from '@react-navigation/native';

import { Fonts } from '../../utils/Fonts';
import { hp, wp } from '../../utils/utility';

const AuthenticationScreen = ({ route, navigation }) => {
    const { phone, callingCode } = route.params;

    useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: () => (
                <TouchableOpacity onPress={login}>
                    <Text style={{ fontSize: 18, color: '#176fdb', fontWeight: 'bold', fontFamily: Fonts.MulishRegular }}>Next</Text>
                </TouchableOpacity>
            ),
        });
    }, [navigation]);

    const login = async () => {
        try {
            await AsyncStorage.setItem('@login_status', '1')
            navigation.dispatch(
                CommonActions.reset({
                    index: 1,
                    routes: [{ name: 'HomeDrawer' }],
                })
            );
        } catch (e) {
            console.log(e);
        }
    }

    return (
        <View style={styles.container}>
            <Text style={{ fontSize: 28, fontFamily: Fonts.MulishMedium, color: '#333333' }}> +{callingCode + " " + phone.split(' ').join('-')} </Text>
            <Text style={{ fontSize: 16, fontFamily: Fonts.MulishMedium, color: '#333333', paddingHorizontal: wp(12), paddingVertical: wp(2), textAlign: 'center' }}>We have sent you an SMS with a code.</Text>
            <View style={{ paddingVertical: hp(15) }}>
                <TextInput
                    style={{ width: wp(80), textAlign: 'center', fontSize: 25, color: '#333333', fontFamily: Fonts.MulishRegular }}
                    placeholder="Code"
                    placeholderTextColor="grey"
                    underlineColorAndroid="grey"
                />
            </View>
            <TouchableOpacity>
                <Text style={{ color: '#176fdb', fontSize: 18, fontFamily: Fonts.MulishRegular }}>Haven't received the code?</Text>
            </TouchableOpacity>
        </View>
    )
};

export default AuthenticationScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        paddingTop: hp(8)
    },
})
