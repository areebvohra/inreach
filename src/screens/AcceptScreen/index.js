import React, { useState } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import { Container, Button } from 'native-base';
import { Fonts } from '../../utils/Fonts';
import FontSize from '../../utils/FontSize';
import { hp, wp } from '../../utils/utility';

const AcceptScreen = ({ navigation }) => {
    const [toggleCheckBox, setToggleCheckBox] = useState(false)

    return (
        <Container>
            <View style={styles.container}>
                <View style={{ height: hp(70), alignItems: 'center', paddingTop: hp(6) }}>
                    <Image style={{ width: wp(70), height: hp(35) }} resizeMode="contain" source={require('../../assets/accept.png')} />
                    <View style={{ alignItems: 'center', paddingHorizontal: wp(6) }}>
                        <Text style={{ color: '#333333', fontSize: FontSize('x5Large'), textAlign: 'center', fontFamily: Fonts.MulishSemiBold, paddingVertical: hp(2) }}>Terms & Conditions</Text>
                        <Text style={{ color: '#333333', fontSize: FontSize('medium'), textAlign: 'center', fontFamily: Fonts.MulishMedium, lineHeight: 25 }}>We have revised our <Text style={{ color: '#0a54b3' }}>Terms & Conditions</Text> and <Text style={{ color: '#0a54b3' }}>Privacy Policy.</Text></Text>
                    </View>
                </View>
                <View style={{ height: hp(30), alignItems: 'center', paddingHorizontal: wp(14), }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: hp(3) }}>
                        <CheckBox
                            disabled={false}
                            value={toggleCheckBox}
                            boxType="square"
                            style={{ borderWidth: 1, transform: [{ scaleX: 1.5 }, { scaleY: 1.5 }], marginRight: wp(2) }}
                            onValueChange={(newValue) => setToggleCheckBox(newValue)}
                        />
                        <Text style={{ fontSize: FontSize('small'), fontFamily: Fonts.MulishMedium, color: '#333333', lineHeight: 20 }}>By choosing "Accept" I agree to the updated Terms of Use and Privacy Policy</Text>
                    </View>
                    <Button block onPress={() => { toggleCheckBox ? navigation.navigate('GetStarted') : alert('Please accept Terms & Condition to proceed') }} style={styles.doneBtn}>
                        <Text style={styles.doneBtnText}>Accept</Text>
                    </Button>
                </View>
            </View>
        </Container>
    );
}
export default AcceptScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    doneBtn: {
        height: hp(6.5),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        backgroundColor: '#0A54B3'
    },
    doneBtnText: { fontSize: FontSize('large'), color: '#fff', textAlign: 'center', fontFamily: Fonts.MulishMedium },
})