import React, { useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, TextInput } from 'react-native';
import AlphabetList from 'react-native-flatlist-alphabet';
const moment = require('moment');
import AntDesign from 'react-native-vector-icons/AntDesign'

import { hp, wp } from '../../utils/utility';
import RadioButton from '../../components/RadioButton'
import FontSize from '../../utils/FontSize';
import { Fonts } from '../../utils/Fonts';

const InviteContactScreen = () => {
    const [contactList, setContactList] = useState([
        { key: '00', userImage: require('../../assets/avatar.png'), value: 'Aames Brian', lastSeen: '2021-05-02T09:10:51+05:00', selected: false },
        { key: '01', userImage: require('../../assets/avatar.png'), value: 'Aames Brian', lastSeen: '2021-05-02T03:10:51+05:00', selected: false },
        { key: '02', userImage: require('../../assets/avatar.png'), value: 'Aames Brian', lastSeen: '2021-05-02T07:10:51+05:00', selected: false },
        { key: '03', userImage: require('../../assets/avatar.png'), value: 'Aames Brian', lastSeen: '2021-05-01T08:10:51+05:00', selected: false },
        { key: '04', userImage: require('../../assets/avatar.png'), value: 'Aames Brian', lastSeen: '2021-03-01T08:10:51+05:00', selected: false },
        { key: '05', userImage: require('../../assets/avatar.png'), value: 'Bames Brian', lastSeen: '2021-01-01T08:10:51+05:00', selected: false },
        { key: '06', userImage: require('../../assets/avatar.png'), value: 'Bames Brian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '07', userImage: require('../../assets/avatar.png'), value: 'Bames Brian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '08', userImage: require('../../assets/avatar.png'), value: 'Names Brian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '09', userImage: require('../../assets/avatar.png'), value: 'Mames Brian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '10', userImage: require('../../assets/avatar.png'), value: 'Cames Brian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '11', userImage: require('../../assets/avatar.png'), value: 'Dames Brian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '12', userImage: require('../../assets/avatar.png'), value: 'Eames Brian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '13', userImage: require('../../assets/avatar.png'), value: 'Fames Brian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '14', userImage: require('../../assets/avatar.png'), value: 'Games Brian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '15', userImage: require('../../assets/avatar.png'), value: 'Hames Brian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '16', userImage: require('../../assets/avatar.png'), value: 'Iames Srian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '17', userImage: require('../../assets/avatar.png'), value: 'James Orian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '18', userImage: require('../../assets/avatar.png'), value: 'Kames Orian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '19', userImage: require('../../assets/avatar.png'), value: 'Lames Orian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '20', userImage: require('../../assets/avatar.png'), value: 'Mames Orian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '21', userImage: require('../../assets/avatar.png'), value: 'Names Orian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '22', userImage: require('../../assets/avatar.png'), value: 'Oames Orian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '23', userImage: require('../../assets/avatar.png'), value: 'Pames Orian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '24', userImage: require('../../assets/avatar.png'), value: 'Qames Orian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '25', userImage: require('../../assets/avatar.png'), value: 'Rames Orian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '26', userImage: require('../../assets/avatar.png'), value: 'Sames Orian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '27', userImage: require('../../assets/avatar.png'), value: 'Tames Orian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '28', userImage: require('../../assets/avatar.png'), value: 'Uames Orian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '29', userImage: require('../../assets/avatar.png'), value: 'Vames Orian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '30', userImage: require('../../assets/avatar.png'), value: 'Wames Orian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '31', userImage: require('../../assets/avatar.png'), value: 'Xames Orian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '32', userImage: require('../../assets/avatar.png'), value: 'Yames Orian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false },
        { key: '33', userImage: require('../../assets/avatar.png'), value: 'Zames Orian', lastSeen: '2021-05-02T08:10:51+05:00', selected: false }
    ])

    const selectContact = (key) => {
        // let index = parseInt(key)
        // let a = [...contactList];
        // a[index]['selected'] = !a[index]['selected'];
        // setContactList(a);

        var index = contactList.findIndex(x => x.key === key);
        let a = contactList[index]
        a['selected'] = !a['selected'];
        setContactList([...contactList.slice(0, index), a, ...contactList.slice(index + 1)]);
    }

    const lastSeenFotmatter = (value) => {
        let final = null;
        let fromNow = moment(value).fromNow();

        if (fromNow.includes('hour') || fromNow.includes('minute'))
            final = moment(value).fromNow();
        else
            final = moment(value).calendar();

        return final;
    }

    const renderListItem = (item) => {
        return (
            <TouchableOpacity onPress={() => selectContact(item.key)} style={styles.itemContainer}>
                <RadioButton selected={item.selected} />
                <Image style={styles.imageSize} source={item.userImage} />
                <View style={{ borderBottomWidth: hp(0.1), borderBottomColor: '#e6e6e6', justifyContent: 'space-between', height: '100%', paddingVertical: hp(1), width: wp(65) }}>
                    <Text style={{ fontSize: FontSize('large'), fontFamily: Fonts.MulishBold, color: '#333333' }}>{item.value}</Text>
                    <Text style={{ fontSize: FontSize('small'), fontFamily: Fonts.MulishSemiBold, color: '#89898b' }}>{'last seen ' + lastSeenFotmatter(item.lastSeen)}</Text>
                </View>
            </TouchableOpacity>
        );
    };

    const renderSectionHeader = (section) => {
        return (
            <View style={styles.sectionHeaderContainer}>
                <Text style={styles.sectionHeaderLabel}>{section.title}</Text>
            </View>
        );
    };

    return (
        <View style={styles.container}>
            <View style={{ paddingHorizontal: wp(10), paddingVertical: hp(1), width: '100%', backgroundColor: '#fff' }}>
                <View style={{ backgroundColor: '#fff', borderRadius: 5, elevation: 5, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: wp(2) }}>
                    <TextInput
                        placeholder="Search Contacts"
                        placeholderTextColor="#000"
                        style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, width: wp(68) }}
                    />
                    <AntDesign name="search1" size={25} />
                </View>
            </View>

            <View style={{ paddingBottom: hp(4), backgroundColor: '#ffffff', }}>
                <AlphabetList
                    style={{ width: '100%', }}
                    data={contactList}
                    renderItem={renderListItem}
                    renderSectionHeader={renderSectionHeader}
                    letterItemStyle={{ height: hp(3), fontFamily: Fonts.MulishLight }}
                    indexLetterColor={'#074caa'}
                />
            </View>
        </View>
    );
}


export default InviteContactScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    imageSize: {
        width: wp(12),
        height: hp(6),
        borderRadius: 50,
        resizeMode: 'cover',
        marginLeft: hp(2),
        marginRight: wp(1.5)
    },
    itemContainer: {
        height: hp(8),
        flexDirection: 'row',
        width: wp(90),
        alignItems: 'center',
        // paddingVertical: hp(1),
        paddingHorizontal: wp(3)
    },
    sectionHeaderContainer: {
        backgroundColor: '#f7f7f7',
        width: wp(92),
        paddingVertical: hp(0.5),
        paddingLeft: wp(5),
    },
    sectionHeaderLabel: {
        color: 'grey',
    },
})