import React from 'react';
import { Text, TouchableOpacity, Image, View } from 'react-native';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import Entypo from 'react-native-vector-icons/Entypo'

import { hp, wp } from '../utils/utility';
import { Fonts } from '../utils/Fonts';
import FontSize from '../utils/FontSize';

import WelcomeScreen from '../screens/WelcomeScreen';
import { SignUpStep1Screen, SignUpStep2Screen, SignUpStep3Screen, SignUpStep4Screen } from '../screens/SignUpScreen'
import PhoneNumberScreen from '../screens/PhoneNumberScreen';
import AuthenticationScreen from '../screens/AuthenticationScreen';
import SplashScreen from '../screens/SplashScreen';
import GetStartedScreen from '../screens/GetStartedScreen';
import InviteContactScreen from '../screens/InviteContactScreen';
import AcceptScreen from '../screens/AcceptScreen';
import DrawerNavigator from './DrawerNavigator';

const Stack = createStackNavigator();

const StackNavigator = () => (
    <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName="splash">
        <Stack.Screen name="splash" component={SplashScreen} />
        <Stack.Screen name="welcome" component={WelcomeScreen} />
        <Stack.Screen name="AuthStack" component={AuthStack} />
        <Stack.Screen name="CreateProfileStack" component={CreateProfileStack} />
        <Stack.Screen name="HomeDrawer" component={DrawerNavigator} />
    </Stack.Navigator>
);

const AuthStack = () => (
    <Stack.Navigator
        initialRouteName="phone"
        screenOptions={({ route }) => ({
            gestureEnabled: true,
            gestureDirection: 'horizontal',
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
            headerTitle: null,
            headerLeft: ({ onPress }) => route.name == 'phone' ? null : (
                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={onPress}>
                    <Entypo name="chevron-left" size={30} color="#176fdb" />
                    <Text style={{ fontSize: 18, color: '#176fdb', fontWeight: 'bold', fontFamily: Fonts.MulishRegular }}>Back</Text>
                </TouchableOpacity>
            ),
            headerLeftContainerStyle: { paddingLeft: wp(4) },
            headerRightContainerStyle: { paddingRight: wp(4) },
            headerTransparent: true,
        })}
    >
        <Stack.Screen name="phone" component={PhoneNumberScreen} />
        <Stack.Screen name="AuthenticateCode" component={AuthenticationScreen} />
    </Stack.Navigator>
)

const CreateProfileStack = () => (
    <Stack.Navigator
        initialRouteName="signUpStep1"
        screenOptions={{
            headerTitle: 'Create Your Profile',
            headerLeft: ({ onPress }) => (
                <TouchableOpacity onPress={onPress} style={{ marginLeft: wp(6) }}>
                    <Image style={{ width: wp(5), height: hp(2.5), }} resizeMode='contain' source={require('../assets/icons/back-icon.png')} />
                </TouchableOpacity>
            ),
            headerTitleAlign: 'center',
            headerTitleStyle: { fontSize: FontSize('medium'), fontFamily: Fonts.MulishBold },
            headerStyle: { elevation: 0, height: hp(8), backgroundColor: '#fff' },
            gestureEnabled: true,
            gestureDirection: 'horizontal',
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
    >
        <Stack.Screen name="signUpStep1" component={SignUpStep1Screen} />
        <Stack.Screen name="signUpStep2" component={SignUpStep2Screen} />
        <Stack.Screen name="signUpStep3" component={SignUpStep3Screen} />
        <Stack.Screen name="signUpStep4" component={SignUpStep4Screen} />
        <Stack.Screen name="AcceptTermsCondition" options={{ headerShown: false }} component={AcceptScreen} />
        <Stack.Screen name="GetStarted" options={{ headerShown: false }} component={GetStartedScreen} />
        <Stack.Screen
            name="InviteContact"
            options={{
                headerLeft: ({ onPress }) => (
                    <TouchableOpacity onPress={onPress} style={{ marginLeft: wp(2), flexDirection: 'row', alignItems: 'center' }}>
                        <Image style={{ width: wp(5), height: hp(2.5), tintColor: '#3879df' }} resizeMode='contain' source={require('../assets/icons/back-icon.png')} />
                        <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, color: '#3879df' }}>Cancel</Text>
                    </TouchableOpacity>
                ),
                headerTitle: "Invite Contacts",
                headerTitleStyle: { fontSize: FontSize('medium'), fontFamily: Fonts.MulishBold },
                headerRight: () => (
                    <TouchableOpacity style={{ marginRight: wp(4), alignItems: 'center' }}>
                        <Text style={{ fontSize: FontSize('medium'), fontFamily: Fonts.MulishBold, color: '#a8a8a8' }}>Send</Text>
                    </TouchableOpacity>
                )
            }}
            component={InviteContactScreen}
        />
    </Stack.Navigator>
);

export default StackNavigator;
