import React from 'react';
import { Image, View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import Ionicons from 'react-native-vector-icons/Ionicons'

import DiscoverScreen from '../screens/DiscoverScreen';
import EditProfileScreen from '../screens/EditProfileScreen';
import PrivacySettingsScreen from '../screens/PrivacySettingsScreen';
import { ChatScreen, MessageScreen } from '../screens/MessageScreen';
import ContactBookScreen from '../screens/ContactBookScreen';
import CallLogScreen from '../screens/CallLogScreen';
import PendingScreen from '../screens/PendingScreen';
import { ScanScreen, ShareScreen } from '../screens/IRIDScreen'
import { AboutReachOut, PostReachOut1, PostReachOut2, ReachOutRequests } from '../screens/ReachOutRequestScreen';
import { OfferPromotionScreen, AboutOfferPromotion, PostOfferPromotion1, PostOfferPromotion2, PostOfferPromotion3 } from '../screens/OfferPromotionScreen';
import InviteContactScreen from '../screens/InviteContactScreen';
import { WSAboutScreen, WSContactScreen } from '../screens/WhoSavedScreen';
import SocialConnectScreen from '../screens/SocialConnectScreen';

import { hp, wp } from '../utils/utility';
import FontSize from '../utils/FontSize';
import { Fonts } from '../utils/Fonts';
import CustomLabel from '../components/CustomLabel';

const Drawer = createDrawerNavigator();

const DrawerNavigator = () => (
    <Drawer.Navigator
        initialRouteName="discover"
        // openByDefault
        drawerContentOptions={{
            activeBackgroundColor: '#fff',
            contentContainerStyle: { paddingTop: hp(3) },
            itemStyle: {
                marginHorizontal: 0, marginVertical: 0, borderRadius: 0,
                paddingLeft: wp(2),
            }
        }}
        screenOptions={({ navigation }) => ({
            headerShown: true,
            headerTitleAlign: 'center',
            headerTitleStyle: { fontSize: FontSize('medium'), fontFamily: Fonts.MulishBold },
            headerLeft: () => (
                <TouchableOpacity onPress={() => navigation.goBack()} style={{ width: wp(12), alignItems: 'center', justifyContent: 'center' }}>
                    <Image style={{ width: wp(5), height: hp(2.5) }} resizeMode='contain' source={require('../assets/icons/back-icon.png')} />
                </TouchableOpacity>
            ),
            headerStyle: { borderBottomWidth: hp(0.1), borderBottomColor: '#e6e6e6', height: hp(8), elevation: 0 }
        })}
    >
        <Drawer.Screen
            name="editProfile"
            component={EditStack}
            options={{
                drawerLabel: () => (
                    <View style={styles.labelContainer}>
                        <View style={{ alignItems: 'flex-end', flexDirection: 'row', justifyContent: 'space-between', }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Image style={styles.iconStyle} resizeMode="contain" source={require('../assets/icons/edit-profile.png')} />
                                <Text style={styles.labelStyle}>Edit Profile</Text>
                            </View>
                        </View>
                        <View>
                            <Image style={{ width: wp(10), height: hp(5), borderRadius: 30 }} resizeMode="contain" source={require('../assets/avatar-circle.png')} />
                        </View>
                    </View>

                ),
                headerTitle: "Edit Profile",
                headerShown: false
            }}
        />
        <Drawer.Screen
            name="privacySettings"
            component={PrivacySettingsScreen}
            options={{
                drawerLabel: () => (
                    <CustomLabel
                        text="Privacy Settings"
                        image={require('../assets/icons/settings.png')}
                        badgeValue={1}
                        borderBottom={true}
                    />
                ),
                headerTitle: "Privacy Settings"
            }}
        />
        <Drawer.Screen
            name="whoSaved"
            component={WhoSavedStack}
            options={{
                drawerLabel: () => (
                    <View style={{ backgroundColor: '#165cbb', height: hp(7), justifyContent: 'center', width: wp(80), left: wp(-4), paddingLeft: wp(4), marginTop: hp(-2), marginBottom: hp(-2) }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image style={{ tintColor: '#fff', width: wp(6), height: hp(3) }} resizeMode="contain" source={require('../assets/icons/saved.png')} />
                            <Text style={{ color: '#fff', fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, paddingLeft: wp(2) }}>Who Saved My Contact</Text>
                        </View>
                    </View>
                ),
                headerShown: false
            }}
        />
        <Drawer.Screen
            name="shareiRiD"
            component={ShareScreen}
            options={{
                drawerLabel: () => (
                    <CustomLabel
                        text="Share Your iRiD"
                        image={require('../assets/icons/scan.png')}
                        badgeValue={0}
                        display="none"
                    />
                ),
                headerTitle: "Share Your iRiD"
            }}
        />
        <Drawer.Screen
            name="scaniRiD"
            component={ScanScreen}
            options={{
                drawerLabel: () => (
                    <CustomLabel
                        text="Scan an iRiD"
                        image={require('../assets/icons/photo.png')}
                        badgeValue={0}
                        display="none"
                        borderBottom={true}
                    />
                )
            }}
        />
        <Drawer.Screen
            name="messages"
            component={HomeStack}
            options={{
                drawerLabel: () => (
                    <CustomLabel
                        text="Messages"
                        image={require('../assets/icons/messages.png')}
                        badgeValue={10}
                    />
                ),
                headerShown: false
            }}
        />
        <Drawer.Screen
            name="discover"
            component={DiscoverScreen}
            options={{
                drawerLabel: () => (
                    <CustomLabel
                        text="Discover"
                        image={require('../assets/icons/location.png')}
                        badgeValue={0}
                        display="none"
                    />
                ),
                headerShown: false
            }}
        />
        <Drawer.Screen
            name="contactBook"
            component={ContactBookScreen}
            options={({ navigation }) => ({
                drawerLabel: () => (
                    <View style={{ backgroundColor: '#f2f2f2', height: hp(7), justifyContent: 'center', width: wp(80), left: wp(-4), paddingLeft: wp(4), marginTop: hp(-2), marginBottom: hp(-2) }}>
                        <View style={{ alignItems: 'flex-end', flexDirection: 'row', justifyContent: 'space-between', }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Image style={{ tintColor: '#000', width: wp(6), height: hp(3) }} resizeMode="contain" source={require('../assets/icons/contact-book.png')} />
                                <Text style={{ color: '#333333', fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, paddingLeft: wp(2) }}>Contact Book</Text>
                            </View>
                        </View>
                    </View>
                )
            })}
        />
        <Drawer.Screen
            name="callLog"
            component={CallLogScreen}
            options={{
                drawerLabel: () => (
                    <CustomLabel
                        text="Call Log"
                        image={require('../assets/icons/phone.png')}
                        badgeValue={0}
                        display="none"
                        borderBottom={true}
                    />
                ),
                headerTitle: "Call Log"
            }}
        />
        <Drawer.Screen
            name="pendingConnection"
            component={PendingScreen}
            options={{
                drawerLabel: () => (
                    <CustomLabel
                        text="Pending Connections"
                        image={require('../assets/icons/pending.png')}
                        badgeValue={10}
                        borderBottom={true}
                    />
                ),
                headerTitle: 'Pending Connections'
            }}
        />
        <Drawer.Screen
            name="ReactOut"
            component={ReactOutStack}
            options={{
                drawerLabel: () => (
                    <CustomLabel
                        text="Reach Out Requests"
                        image={require('../assets/icons/reach-out.png')}
                        badgeValue={1}
                        borderBottom={true}
                    />
                ),
                headerShown: false
            }}
        />
        <Drawer.Screen
            name="offerPromotion"
            component={OfferPromotionStack}
            options={{
                drawerLabel: () => (
                    <CustomLabel
                        text="Offers & Promotions"
                        image={require('../assets/icons/offer-promotion.png')}
                        badgeValue={1}
                        borderBottom={true}
                    />
                ),
                headerShown: false
            }}
        />
        <Drawer.Screen
            name="inviteContact"
            component={InviteContactScreen}
            options={{
                drawerLabel: () => (
                    <CustomLabel
                        text="Invite Contacts"
                        badgeValue={0}
                        image={require('../assets/icons/invite.png')}
                        display="none"
                    />
                ),
                headerTitle: 'Invite Contacts'
            }}
        />
    </Drawer.Navigator>
);

const Stack = createStackNavigator();

const WhoSavedStack = () => (
    <Stack.Navigator initialRouteName="WSAbout" screenOptions={StackHeader}>
        <Stack.Screen name="WSAbout" options={{ headerTitle: "Privacy Settings" }} component={WSAboutScreen} />
        <Stack.Screen name="WSContact" options={{ headerTitle: "Who Saved My Contact" }} component={WSContactScreen} />
    </Stack.Navigator>
)

const EditStack = () => (
    <Stack.Navigator initialRouteName="editProfile" screenOptions={StackHeader}>
        <Stack.Screen name="editProfile" options={{ headerShown: false }} component={EditProfileScreen} />
        <Stack.Screen name="socialConnect" options={{ headerTitle: "Social Media Connects" }} component={SocialConnectScreen} />
    </Stack.Navigator>
)

const ReactOutStack = () => (
    <Stack.Navigator initialRouteName="reachOutRequests" screenOptions={StackHeader}>
        <Stack.Screen name="reachOutRequests" options={{ headerShown: false }} component={ReachOutRequests} />
        <Stack.Screen name="aboutReachOut" options={{ headerTitle: "About Reach Out" }} component={AboutReachOut} />
        <Stack.Screen name="postReachOut1" options={{ headerTitle: "Post Reach Out" }} component={PostReachOut1} />
        <Stack.Screen name="postReachOut2" options={{ headerTitle: "Post Reach Out" }} component={PostReachOut2} />
    </Stack.Navigator>
)

const OfferPromotionStack = () => (
    <Stack.Navigator initialRouteName="offerPromotion" screenOptions={StackHeader}>
        <Stack.Screen name="offerPromotion" options={{ headerTitle: "Offers & Promotions" }} component={OfferPromotionScreen} />
        <Stack.Screen name="aboutOfferPromotion" options={{ headerTitle: "Post Offers & Promotions" }} component={AboutOfferPromotion} />
        <Stack.Screen name="postOffer1" options={{ headerTitle: "Post Offers & Promotions" }} component={PostOfferPromotion1} />
        <Stack.Screen name="postOffer2" options={{ headerTitle: "Post Offers & Promotions" }} component={PostOfferPromotion2} />
        <Stack.Screen name="postOffer3" options={{ headerTitle: "Post Offers & Promotions" }} component={PostOfferPromotion3} />
    </Stack.Navigator>
)

const HomeStack = () => (
    <Stack.Navigator initialRouteName="messages" screenOptions={StackHeader}>
        <Stack.Screen name="messages" options={{ headerTitle: "Messages" }} component={MessageScreen} />
        <Stack.Screen name="chat" component={ChatScreen} />
    </Stack.Navigator>
)

const StackHeader = ({ navigation }) => ({
    gestureEnabled: true,
    gestureDirection: 'horizontal',
    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
    headerTitleAlign: 'center',
    headerTitleStyle: { fontSize: FontSize('medium'), fontFamily: Fonts.MulishBold },
    headerLeft: () => (
        <TouchableOpacity onPress={() => navigation.goBack()} style={{ width: wp(12), alignItems: 'center', justifyContent: 'center' }}>
            <Image style={{ width: wp(5), height: hp(2.5) }} resizeMode='contain' source={require('../assets/icons/back-icon.png')} />
        </TouchableOpacity>
    ),
    headerStyle: { borderBottomWidth: hp(0.1), borderBottomColor: '#e6e6e6', height: hp(8), elevation: 0 }
})

export default DrawerNavigator;

const styles = StyleSheet.create({
    iconStyle: { tintColor: '#000', width: wp(6), height: hp(3) },
    labelContainer: {
        backgroundColor: '#fff',
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: wp(62),
    },
    labelStyle: { color: '#333333', fontSize: FontSize('medium'), fontFamily: Fonts.MulishRegular, paddingLeft: wp(2) },
    badgeStyle: { width: wp(6), height: hp(3), backgroundColor: '#0c51af', borderRadius: 15 }
})
